.TH TC2HTML-TOC 1
.SH NAME
tc2html-toc \- table of contents processor for \fItc2html\fP
.SH SYNTAX
.B tc2html-toc
.I file
\&...
.SH DESCRIPTION
.I tc2html
is a postprocessor for
.I troffcvt
that produces Hypertext Markup Language (HTML).
.I tc2html
is capable of generating a table of contents (TOC).
However, since TOC entries
cannot all be known until the entire input document has been read,
.I tc2html
writes the TOC near the end of the HTML document.
.PP
To handle this problem,
.I tc2html-toc
examines
.I tc2html
output for a table of contents and moves
it to the correct location.
.I tc2html-toc
is invoked for your automatically if you use
.I troff2html
(which also invokes
.IR tc2html ).
If you run
.I tc2html
manually, you must also run
.I tc2html-toc
manually.
.PP
.I tc2html
and
.I tc2html-toc
cooperate by means of the following conventions.
If
.I tc2html
writes a TOC, it surrounds them with the following two HTML comments:
.sp .5v
.RS
.nf
<!-- TOC BEGIN -->
<!-- TOC END -->
.fi
.RE
.sp .5v
If the user wishes to explicitly
specify a location for the TOC, this may be done
by invoking the
.B \&.H*toc*title
request in the document.
This request writes a TOC title followed by this HTML comment:
.sp .5v
.RS
<!-- INSERT TOC HERE -->
.RE
.sp .5v
In addition,
macro package redefinitions used with
.I tc2html
can make a guess about where the TOC should be
located in the absence of an explicit location marker.
To do so, a redefinition can write the following advisory marker comment:
.sp .5v
.RS
<!-- INSERT TOC HERE, MAYBE -->
.RE
.sp .5v
For example, redefinitions for the
.B \-man
macros can write an advisory location for the TOC after the
.B \&.TH
macro invocation has been seen.
.PP
Given these conventions,
.I tc2html-toc
reads the input and examines it for the TOC and TOC location.
The input is written unchanged to the output if no TOC is found.
Otherwise, the locations listed below are used to reposition the TOC (with
locations listed first taking priority):
.IP \(bu
The position of the TOC location marker.
.IP \(bu
The position of the advisory TOC location marker.
.IP \(bu
The beginning of the <BODY> part of the document.
.\"
.SH "SEE ALSO"
.B troffcvt\c
(1),
.B tc2html\c
(1),
.B troff2html\c
(1)
.SH "WHO-TO-BLAME"
Paul DuBois,
.IR dubois@primate.wisc.edu\c
\&.
.SH DIAGNOSTICS
.I tc2html-toc
exits with an error if it discovers that the TOC location marker lies
within the TOC itself.
