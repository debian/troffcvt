.TH TBLCVT 1
.SH NAME
tblcvt \- \fItroffcvt\fP preprocessor to convert \fItbl\fP-format tables
.SH SYNTAX
.B tblcvt
[
.I file
] \&...
.SH DESCRIPTION
.PP
.I tblcvt
examines
.I troff
input for tables that would normally be processed by the
.I tbl
program, i.e., for lines bracketed by the
.B \&.TS
table start and
.B \&.TE
table end requests.
.I tblcvt
parses table specifications and data and converts it to a form that
.I troffcvt
can more easily handle.
This is useful for document processing commands that use
.IR troffcvt ,
since
.I troffcvt
has knowledge only of the
.I troff
input language, and knows nothing of the
.I tbl
input language.
.PP
.I tblcvt
is a direct replacement for
.IR tbl .
Suppose
you would normally process a document using
.I tbl
and
.I troff
(or
.IR groff,
etc.) like this:
.sp .5v
.RS
.ps -2
\f(CW% tbl file ... | troff ...\fP
.ps
.RE
.sp .5v
To format the document using
.I tblcvt
and
.IR troffcvt ,
use a command like this:
.sp .5v
.RS
.ps -2
\f(CW% tblcvt file ... | troffcvt ...\fP
.ps
.RE
.sp .5v
Actually, it's more likely that you'll be using
one of the
.I troffcvt
front ends such as
.IR troff2html
than that you'll be using
.I troffcvt
directly.
.I troffcvt
in the preceding command may be replaced by the name of the front end:
.sp .5v
.RS
.ps -2
\f(CW% tblcvt file ... | troff2html ...\fP
.ps
.RE
.sp .5v
If it seems that
.I troffcvt
or a front end are not reading the output from
.I tblcvt ,
specify
.B \-
after the option list to explicitly tell them to
read the standard input after processing their other options:
.sp .5v
.RS
.nf
.ps -2
\f(CW% tblcvt file ... | troffcvt ... -\fP
\f(CW% tblcvt file ... | troff2html ... -\fP
.ps
.fi
.RE
.sp .5v
.\"
.SH DIAGNOSTICS
.PP
Diagnostic messages are written in this format:
.sp .5v
.RS
.ps -2
\f(CWtblcvt:line \f(CIl\fP (table \f(CIt\fP, section \f(CIs\fP): \f(CImessage...\fR
.ps
.RE
.sp .5v
This indicates that a problem was detected at line number
.IR l ,
and that
.I tblcvt
considers the line to be part of table number
.I t
and section
.I s
within the table.
Section numbers start at one and are incremented for each
.B \&.T&
request that occurs within a table specification.
.\"
.SH "SEE ALSO"
.BR troffcvt (1)
.PP
\fItblcvt \- A troffcvt Preprocessor\fP
.\"
.SH WHO-TO-BLAME
Paul DuBois,
.IR dubois@primate.wisc.edu\c
\&.
.\"
.SH BUGS
.PP
For the most part,
.I tblcvt
assumes your tables make sense, i.e., are legal according to the
.I tbl
input language.
.I tblcvt
may complain about malformed table constructs (if it detects them), but
it may also get confused by them and generate garbage output.
.PP
.I tblcvt
generates
.I troff
output that uses long request names, so you can't use compatibility
mode
.B \-C "" (
option) with
.I troffcvt
or a
.I troffcvt
front end.
.PP
.I tblcvt
doesn't support vertically-spanned lines.
It prints a warning when it detects one, but the table output won't be
correct.
