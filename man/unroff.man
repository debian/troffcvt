.TH UNROFF 1
.SH NAME
unroff \- convert \fItroff\fR documents to plain text
.SH SYNTAX
.B unroff
[
.B \-E
] [
.B \-J
] [
.B \-8
] [
.BI \-m xx
] [
.I file
\&...
]
.SH DESCRIPTION
.I unroff
processes documents written to be formatted with
.I troff
(or
.IR nroff ,
or any of the other
.I *roff
variants) and converts them to plain text.
This is similar to what
.I deroff
does, but the result is sometimes better than
.I deroff
output.
.PP
Optional flags may be given to modify the operation of
.IR unroff ,
as follows:
.TP
.B \-E
Echo input tokens as they are read.
Useful mainly for debugging.
.TP
.B \-J
Join output lines that would be part of a single paragraph so they form
a single output line.
This is useful if you plan to move the resulting output into a document
formatter that treats paragraphs as a single line.
The default is to do line wrapping according to the line length specified
in the input document.
.TP
.B \-8
Write 8-bit ISO Latin-1 (ISO 8859-1) characters.
This option may be useful for producing output intended to be viewed on
devices capable of 8-bit character display.
Without this option, 8-bit Latin-1 characters will generall
appear in the output
as ``[[name]]'' where ``name'' is the
.I troffcvt
internal name for the character, e.g., ``[[Aacute]]''.
For some of these characters, an ASCII approximation will be used if
something reasonably close is available.
.TP
.BI \-m xx
Specify macro package, usually
.BR \-man ,
.BR \-me ,
.BR \-mm ,
or
.BR \-ms .
.SH DIAGNOSTICS
.PP
.B  "line length clipped to nnn chars."
A very long line length was requested, so long that it would likely
result in output line assembly buffer overflow.
The length is clipped to prevent this.
.PP
.B "output buffer capacity exceeded."
This means some line is so long that it couldn't be collected in the
output line assembly buffer.
Most likely this signals a bug in
.IR tc2text ,
since the length is supposed to be kept within reasonable bounds (see
previous paragraph).
.\"
.SH "SEE ALSO"
.BR troffcvt (1),
.BR tc2text (1)
.\"
.SH "WHO-TO-BLAME"
Paul DuBois,
.IR dubois@primate.wisc.edu\c
\&.
.\"
.SH BUGS
.PP
.I unroff
doesn't do so well with tables, particularly tables with multiple-line
cells.
Table centering isn't handled.
