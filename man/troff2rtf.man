.TH TROFF2RTF 1
.SH NAME
troff2rtf \- convert \fItroff\fR documents to Rich Text Format
.SH SYNTAX
.B troff2rtf
[
.BI \-m xx
] [
.B \-S
.I charset
] [
.I file
\&...
]
.SH DESCRIPTION
.I troff2rtf
processes documents written to be formatted with
.I troff
(or
.IR nroff ,
or any of the other
.I *roff
variants) and converts them to Rich Text Format.
RTF is a reasonably portable interchange standard; files in RTF format
can be read by a variety of applications, e.g., Microsoft Word,
WordPerfect (Macintosh version 2.0 and up), WriteNow.
.PP
The main use for
.I troff2rtf
is to make it easier to transport
.I troff
documents for use with microcomputers.
First, convert your document to RTF:
.sp .5v
.RS
.ps -2
\f(CW% troff2rtf [\f(CIoptions\fP] file > file.rtf\fR
.ps
.RE
.sp .5v
The available options are described below.
The one you'll most likely use is
.BI \-m xx
to specify a macro package like
.B \-me
or
.BR \-ms .
If the document contains tables, the conversion can be done like this
instead:
.sp .5v
.RS
.ps -2
\f(CW% tblcvt file | troff2rtf [\f(CIoptions\fP] > file.rtf\fR
.ps
.RE
.sp .5v
Then move the RTF file to your target machine and read it into your document
processor.
.PP
Optional flags may be given to modify the operation of
.IR troff2rtf ,
as follows:
.TP
.BI \-m xx
Specify macro package, usually
.BR \-man ,
.BR \-me ,
.BR \-mm ,
or
.BR \-ms .
.TP
.B \-S\ \fIcharset\fR
Specify the RTF character set.
.I charset
can be one of the following:
.B ansi
.B mac
.B pc
.BR pca .
The default is the Macintosh character set.
For documents that you intend to use under Windows,
.B \-S
.B ansi
is a better choice.
.SH "SEE ALSO"
.BR tblcvt (1),
.BR troffcvt (1)
.SH "WHO-TO-BLAME"
Paul DuBois,
.IR dubois@primate.wisc.edu\c
\&.
.SH BUGS
.PP
Table output generated when
.I troff2rtf
is used in concert with
.I tblcvt
has been known to crash Word outright; caution may be in order.
In addition, you may need to read the resulting RTF document
into a word processor and tweak column widths manually.
.PP
Word97 adds support for vertically merging table cells (using the
.B \eclvmgf
and
.B \eclvmrg
control words).
.I troff2rtf
supports vertical spans using these controls, but earlier versions of
Word don't yet understand them.
Consequently, what you'll see for
.IR n -cell
vertical spans is
.I n
individual cells, with all the text in the top cell and
.IR n \-1
empty cells below it.
