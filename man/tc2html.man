.TH TC2HTML 1
.SH NAME
tc2html \- convert \fItroffcvt\fR output to Hypertext Markup Language
.SH SYNTAX
.B tc2html
[
.I options
]
.I file
\&...
]
.SH DESCRIPTION
.I tc2html
is a postprocessor for
.I troffcvt
and converts
.I troffcvt
output to to Hypertext Markup Language (HTML).
It's usually invoked by running the
.I troffcvt
front end
.I troff2html .
.PP
.I tc2html
understands the following options:
.TP
.B \-D
Enable debugging output.
.TP
.B \-E
Cause
.I tc2html
to echo token information to
.I stderr
as tokens are read from
.IR troffcvt .
.TP
.BI \-T \0title
Specify a title for the document.
This can be used for documents that do not contain any recognizable
title.
If the document does contain a title, it is overridden.
.SH "SEE ALSO"
troffcvt(1), troff2html(1)
.SH "WHO-TO-BLAME"
Paul DuBois,
.IR dubois@primate.wisc.edu\c
\&.
.SH BUGS
.I tc2html
uses a set of special macros that tell
.I troffcvt
how to generate
.IR tc2html -specific
output that indicates HTML structural elements.
These macros do not work in compatibility mode.
