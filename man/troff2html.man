.TH TROFF2HTML 1
.SH NAME
troff2html \- convert \fItroff\fR documents to Hypertext Markup Language
.SH SYNTAX
.B troff2html
[
.I options
]
.I file
\&...
]
.SH DESCRIPTION
.I troff2html
converts files written in the
.I troff
input language
and converts them to Hypertext Markup Language (HTML).
It works by running the input files through
.I troffcvt
and then through
.I tc2html
and
.IR tc2html-toc .
.PP
.I troff2html
understands the usual
.I troffcvt
options.
It also understands the additional options listed below:
.TP
.B \-D
Enable debugging output.
.TP
.B \-E
Cause
.I tc2html
to echo token information to
.I stderr
as tokens are read from
.IR troffcvt .
.TP
.B \-n
No execution.
Just show the commands that
.I troff2html
would execute to process the document.
.TP
.B \-p
Don't run the postprocessors
.RI ( tc2html
and
.IR tc2html-toc ).
The output from
.I troff2html
will be in the
.I troffcvt
output language.
.TP
.BI \-T \0title
Specify a title for the document.
This can be used for documents that do not contain any recognizable
title.
If the document does contain a title, it is overridden.
.SH "SEE ALSO"
troffcvt(1), tc2html(1)
.SH "WHO-TO-BLAME"
Paul DuBois,
.IR dubois@primate.wisc.edu\c
\&.
.SH BUGS
.I troff2html
uses a set of special macros that tell
.I troffcvt
how to generate
.IR tc2html -specific
output that
.I tc2html
uses to recognize HTML structural elements.
These macros do not work in compatibility mode.
