.\"
.\" troff -ms % | lpr
.\"
.\" revision date - change whenever this file is edited
.ds RD 18 October 1993
.nr PO 1.2i	\" page offset 1.2 inches
.nr PD .7v	\" inter-paragraph distance
.\"
.EH 'Token Scanning Library'- % -''
.OH ''- % -'Token Scanning Library'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.\" I - italic font (taken from -ms and changed)
.de I
.nr PQ \\n(.f
.if t \&\\$3\\f2\\$1\\fP\&\\$2
.if n .if \\n(.$=1 \&\\$1
.if n .if \\n(.$>1 \&\\$1\c
.if n .if \\n(.$>1 \&\\$2
..
.de IS	\" interface routine description header start
.DS L
.ta .8i
.ft B
..
.de IE	\" interface routine description header end
.DE
.ft R
..
.TL
TS \*- A Simple Token Scanning Library
.sp .5v
.ps 10
Release 1.06
.AU
Paul DuBois
dubois@primate.wisc.edu
.AI
Wisconsin Regional Primate Research Center
Revision date:\0\0\*(RD
.LP
Applications often wish to pull strings apart into individual tokens.
This document describes TS, a library consisting of
an unsophisticated set of routines providing simple token scanning operations.
.LP
String tokenizing can often be done satisfactorily using
.I strtok()
or equivalent function from the C library.
When such routines are insufficient, the routines described here may be useful.
They offer, for example, quote and escape character parsing, and
configurability of underlying scanning properties, within the confines
of a fixed interface.
TS provides a simple built-in scanner, which
may be replaced by alternate routines as
desired.
Applications may switch back and forth between scanners
on the fly.
.NH
Installation
.LP
This release of TS is configured using
.I imake
and the WRPRC2 configuration files, so you
also need to obtain the WRPRC2 configuration distribution
if you want to build it the usual way.
(If you want to avoid
.I imake ,
the
.I Makefile
is simple enough that you should be able to tweak it by hand.)
.LP
There is one library to be built,
.I libtokenscan.a .
That library should be installed in a system library directory.
The header file
.I tokenscan.h
should be installed in a system header file directory.
.NH
Example
.LP
The canonical method of tokenizing a string with TS is as follows:
.LP
.DS
char	buf[size], *p;

/* \fI...initialize contents of buf here...\fP */
TSScanInit (buf);	/* initialize scanner */
while ((p = TSScan ()) != (char *) NULL)
{
	/* \fI...do something here with token pointed to by p here...\fP */
}
.DE
.LP
The scanner is initialized by passing the string to be scanned to
.I TSScanInit()
and
.I TSScan()
is called to get pointers to successive tokens.
.I TSScan()
returns NULL when there are no more.
.NH
Behavior of the Default Scanner
.LP
The default scanner is destructive in that it modifies the string scanned (it
writes nulls at the end of each token found), so make a copy of
the scanned string if you need to maintain an intact version.
.LP
The scanner is controlled by delimiter, quote, escape, and end-of-string (EOS)
characters.
The defaults for each of these are given below.
.LP
.TS
center tab(:);
l l .
delimiter:space tab
quote:" '
escape:\e
EOS:null linefeed carriage-return
.TE
.LP
In the simplest case, tokens are sequences of characters between
delimiters.
Since the default delimiters are the whitespace characters space and
tab, tokens are sequences of non-whitespace characters.
.LP
.DS
.ta 1.5i 2i
This is a line	\(->	<This> <is> <a> <line>
.DE
.LP
Quotes may be used to include whitespace within a token.
Quotes must match; hence one quote character may be used to quote
another kind of quote character, if there is more than one.
.LP
.DS
.ta 1.5i 2i
"This is" a line	\(->	<This is> <a> <line>
This" "is a line	\(->	<This is> <a> <line>
"'"\0'"'	\(->	<'> <">
"'"'"'	\(->	<'">
.DE
.LP
The escape character turns off any special meaning of the next
character, including another escape character.
.LP
.DS
.ta 1.5i 2i
What\e's up	\(->	<What's> <up>
\e\e is the escape	\(->	<\e> <is> <the> <escape>
.DE
.LP
The EOS characters tell the scanner when to quit scanning.
A null character always terminates the scan.
In the default case, linefeed and carriage return do as well.
.LP
You can replace the delimiter, quote, escape, or EOS character sets.
This changes the particular characters that trigger the above
behaviors, without changing the way the default scan algorithm works.
Or you can replace the scan routine to make the scanner behave in
entirely different ways.
.LP
By default, multiple consecutive delimiter characters are treated as a
single delimiter.
A flag may be set in the scanner structure to suppress delimiter concatenation,
so that every delimiter character is significant.
This is useful for tokenizing strings in which empty fields are allowed:
two consecutive delimiters are considered to have an empty
token between them, and delimiters appearing at the beginning or end of a
string signify an empty token at the beginning end of the string.
.LP
The difference in treatment of strings when delimiters are concatenated versus
when they are not is illustrated below.
Suppose the delimiter is colon (:) and the string to be tokenized is:
.DS
:a:b::c:
.DE
When delimiters are concatenated, the string contains three tokens:
.LP
.DS
.ta 1.5i 2i
:a:b::c:	\(-> <a> <b> <c>
.DE
.LP
When all delimiters are significant, string contains three empty tokens in
addition:
.LP
.DS
.ta 1.5i 2i
:a:b::c:	\(-> <> <a> <b> <> <c> <>
.DE
.NH
Programming Interface
.LP
Source files using TS routines should include
.I tokenscan.h
and executables should be linked with
.I "\-ltokenscan" .
.LP
A scanner is described by a data structure:
.LP
.DS
typedef struct TSScanner TSScanner;
struct TSScanner
{
	void	(*scanInit) ();
	char	*(*scanScan) ();
	char	*scanDelim;
	char	*scanQuote;
	char	*scanEscape;
	char	*scanEos;
	int	scanFlags;
}
.DE
.LP
Scanner structures may be obtained or installed with
.I TSGetScanner()
and
.I TSSetScanner() .
.LP
For each string to be scanned, the application passes a pointer to it to
.I TSScanInit() ,
which takes care of scan initialization.
If the application requires initialization to be performed in addition to
that done internally by TS, a pointer to a routine that does so should
be installed in the
.I scanInit
field of the scanner data structure.
It takes one argument, a pointer to the string to be scanned.
The default
.I scanInit
is NULL, which does nothing.
.LP
.I scanDelim ,
.I scanQuote ,
.I scanEscape ,
and
.I scanEos
are pointers to null-terminated strings consisting of
the set of characters to be considered delimiter, quote,
escape, and EOS characters, respectively.
The default values were described previously.
.LP
.I scanScan
points to the
routine that does the actual scanning.
It is called by
.I TSScan()
and should be declared to take no arguments and return a character
pointer to the next token in the current scan buffer.
Normally, this routine does the following:
call
.I TSGetScanPos()
to get the current scan position, scan the token, call
.I TSSetScanPos()
to update the scan position, then return a pointer to the beginning of
the token.
If there are no more tokens in the scan buffer, the routine should return
NULL, and should continue to do so until
.I TSScanInit()
is called again.
.LP
.I scanFlags
contains flags that modify the scanner's behavior.
For the default scanner, the default is zero.
If the
.I tsNoConcatDelims
flag is set, the scanner stops on every delimiter rather than
treating sequences of contiguous delimiters as a single delimiter.
.LP
The public routines in the TS library are described below.
.IS
void TSScanInit (p)
char	*p;
.IE
Initializes the scanning routines to make
the character string pointed to by
.I p
the current scan buffer.
.IS
char *TSScan ()
.IE
Returns a pointer to the next token in the current scan buffer, NULL if
there are no more.
The token is terminated by a null byte.
Scan behavior may be modified by substituting alternate scan routines.
.LP
Once
.I TSScan()
returns NULL, it continues to do so
until the scanner is reinitialized with another call to
.I TSScanInit() .
.IS
void TSGetScanner (p)
TSScanner	*p;
.IE
Gets the current scanner information (initialization and scan procedures;
delimiter, quote, escape, and EOS character sets; and scanner flags) into the
structure pointed to by
.I p .
.IS
void TSSetScanner (p)
TSScanner	*p;
.IE
Installs a scanner.
If
.I p
itself if NULL, all default scanner values are reinstalled.
Otherwise, any pointer field in
.I p
with a NULL value causes the corresponding value from the default scanner
to be reinstalled, and if
.I p\->scanFlags
is zero, the scanner flags are set to the default (also zero).
.IS
void TSGetScanPos (p)
char	**p;
.IE
Puts the current position within the current scan buffer into the
argument, which should be passed as the address of a character pointer.
This is useful when you want to scan only enough of the buffer to partially
classify it, then use the rest in some other way.
.IS
void TSSetScanPos (p)
char	*p;
.IE
Set the current scan position to
.I p .
.IS
int TSIsScanDelim (c)
char	c;
.IE
Returns non-zero if
.I c
is a member of the current delimiter character set, zero otherwise.
.IS
int TSIsScanQuote (c)
char	c;
.IE
Returns non-zero if
.I c
is a member of the current quote character set, zero otherwise.
.IS
int TSIsScanEscape (c)
char	c;
.IE
Returns non-zero if
.I c
is a member of the current escape character set, zero otherwise.
.IS
int TSIsScanEos (c)
char	c;
.IE
Returns non-zero if
.I c
is an end-of-string character, zero otherwise.
.IS
int TSTestScanFlags (flags)
int	flags
.IE
Returns non-zero if
all bits in
.I flags
are set for the current scanner, zero otherwise.
.NH 2
Overriding Scanning Routines
.LP
It is possible to switch back and forth between scan procedures on the
fly, even in the middle of scanning a string.
The general procedure is to use
.I TSGetScanner()
to get the current scanner information,
and
.I TSSetScanner()
to install a new one and reinstall the old one when done with
the new.
If you switch between more than two scanners, another method may be
necessary.
.LP
It is possible to modify the default scanner without replacing it.
For instance, you could change the default delimiter set but leave everything
else the same,
as follows:
.LP
.DS
TSScanner scanStruct;

TSGetScanner (&scanStruct);
scanStruct.scanDelim = " \et:;?,!";
TSSetScanner (&scanStruct);
.DE
.NH
Miscellaneous
.LP
A scanner can be nondestructive with respect to the line being scanned
by using a scan routine that copies characters out of the scanned
line into a second buffer and returning a pointer to the second buffer.
The second buffer must be large enough to hold the largest possible token,
of course.
If the second buffer is a fixed area, the host application must be careful
not to call
.I TSScan()
again until it is done with the current token, or else make a copy of it
first.
If the second buffer is dynamically allocated, the application must
be ready to do storage management of the tokens returned.
.LP
Some scanners might not need delimiter, quote, escape, or EOS
characters at all, particularly if token boundaries are context sensitive.
.NH
Distribution and Update Availability
.LP
The TS
distribution may be freely circulated and is available for
anonymous FTP access in the
.I /pub/TS
directory on host
.I ftp.primate.wisc.edu .
Updates appear there as they become available.
.LP
The WRPRC2
.I imake
configuration file distribution is available on
.I ftp.primate.wisc.edu
as well, in
.I /pub/imake-stuff.
.LP
If you do not have FTP access, send requests to
.I software@primate.wisc.edu .
Bug reports, questions, suggestions and comments may be sent to
this address as well.
