/*
 * tstest - tokenscan library test program
 *
 * Syntax: tstest [ -dstr ] [ -estr ] [ -qstr ] [ -s ] [ file ]
 *
 * 25 Sep 91	Paul DuBois	dubois@primate.wisc.edu
 *
 * 25 Sep 91 Version 1.00.  Created.
*/

# include	<stdio.h>

# include	"tokenscan.h"


#ifndef bufSiz
#define bufSiz 1024
#endif

#define maxPtr 100

static char *usage = "tstest [ -dstr ] [ -estr ] [ -qstr ] [ -s ] [ file ]";

int main (argc, argv)
int	argc;
char	**argv;
{
TSScanner	scanner;
char	buf[bufSiz];
char	*p;
char	*ptr[maxPtr];
int	nPtrs;
int	i;

	--argc;
	++argv;
	while (argc > 0 && argv[0][0] == '-')
	{
		if (strncmp (argv[0], "-d", 2) == 0)
		{
			TSGetScanner (&scanner);
			scanner.scanDelim = &argv[0][2];
			TSSetScanner (&scanner);
			--argc;
			++argv;
		}
		else if (strncmp (argv[0], "-e", 2) == 0)
		{
			TSGetScanner (&scanner);
			scanner.scanEscape = &argv[0][2];
			TSSetScanner (&scanner);
			--argc;
			++argv;
		}
		else if (strncmp (argv[0], "-q", 2) == 0)
		{
			TSGetScanner (&scanner);
			scanner.scanQuote = &argv[0][2];
			TSSetScanner (&scanner);
			--argc;
			++argv;
		}
		else if (strcmp (argv[0], "-s") == 0)
		{
			TSGetScanner (&scanner);
			scanner.scanFlags |= tsNoConcatDelims;
			TSSetScanner (&scanner);
			--argc;
			++argv;
		}
		else
		{
			fprintf (stderr, "unknown flag: %s\n", argv[0]);
			fprintf (stderr, "%s\n", usage);
			exit (1);
		}
	}
	if (argc == 0)
		fprintf (stderr, "Reading standard input:\n");
	else if (argc == 1)
	{
		if (freopen (argv[0], "r", stdin) == (FILE *) NULL)
		{
			fprintf (stderr, "cannot open: %s\n", argv[0]);
			exit (1);
		}
	}
	else
	{
		fprintf (stderr, "%s\n", usage);
		exit (1);
	}

	while (fgets (buf, (int) sizeof (buf), stdin) != (char *) NULL)
	{
		if (buf[0] == '#')	/* comment; just echo */
		{
			fputs (buf, stdout);
			continue;
		}
		printf ("Input:\t%s", buf);
		TSScanInit (buf);
		for (nPtrs = 0; nPtrs < maxPtr; nPtrs++)
		{
			if ((p = TSScan ()) == (char *) NULL)
				break;
			ptr[nPtrs] = p;
		}
		printf ("%d token%s:", nPtrs, nPtrs == 1 ? "" : "s");
		for (i = 0; i < nPtrs; i++)
		{
			putchar (' ');
			printf ("<%s>", ptr[i]);
		}
		putchar ('\n');
	}
	exit (0);
}
