Changes to the WRPRC2 configuration file distribution -- Release 2.10
23 May 1997

- INCOMPATIBLE CHANGE: mklinuxppc.{cf,p-cf} are now linux-pmac.{cf,p-cf}.
This follows the work of the Linux/PPC folks who now have an X11R6.3
port that works under both MkLinux and Linux/PPC.  Any projects that
have a private mklinuxppc.p-cf should rename them to linux-pmac.p-cf.
Also modified the Linux vendor block in Imake.cf accordingly.

- INCOMPATIBLE CHANGE: The TroffToPostScript() macro now has the
$(TROFF) and $(TROFFOPTS) variables embedded in the rule, rather than
those variables being passed in by the TroffMxxxToPostScript() rules.
The third argument therefore is now "flags" rather than "formatter".
Actually, this shouldn't cause much problem since the
TroffToPostScript() rule should be invoked mainly only through the
TroffMxxToPostScript() rules anyway.

- Updated OS{Major,Minor,Teeny}Version macros in linux-pmac.cf to
reflect that the current Linux/PPC server version is 2.0.28.

- Changed Perl5Path in linux-pmac.cf from /usr/local/bin/perl to
/usr/bin/perl.

- Modified tmac.wrprc since it was causing problems with some versions
of groff (e.g., under FreeBSD, MkLinux).

- Added MdocMacros macro and MDOCMACROS variable to Imake.params,
TroffMdocToPostScript() rule to Imake.rules for -mdoc support.

- Added macros and variables for the troffcvt programs.  Added rules
for converting troff documents to RTF and HTML using the troffcvt programs.

- Define PerlPath as /usr/bin/perl in FreeBSD.cf.

- Modified Mips.cf to allow usual setting of CcCmd to be overridden if
HasGcc2 is YES.

- Modified Imake.tmpl to set OSName, OS{Major,Minor,Teeny}Version of
DefaultOSName, DefaultOS{Major,Minor,Teeny}Version are set.  This
may occur if the X11R6.3 version of imake is used, which tries to set
those macros by running uname to find out system name and version
information.  There's no guarantee that version of imake will be the
one that's used, but if the information is available, might as well
use it.

- Top-level Imakefile now has a documents-ps target so that "make
documents-ps" generates PostScript versions of all the main documents.

- Imake.params defines ProjectWRPRC symbol so that Imakefiles can test
it to see whether or not they're being configured with the WRPRC
configuration files.
