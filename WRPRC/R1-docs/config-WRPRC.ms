.\" format with
.\" ${TROFF} ${MSMACROS} this-file | ${PRINTER}
.\"
.\" C is a non-proportional font; if different at your site, change C
.\" to something more appropriate.  Then use .Cw to reference the font.
.\" For in-line references, use \f\*(cW
.fp 4 C \" this should be a non-proportional font
.ds cW 4
.fp 5 CB \" this should be a non-proportional font
.ds cB 5
.\"
.\" revision date - change whenever this file is edited
.ds Rd 28 August 1993
.\" document revision number - change each time document is released
.ds Rn 1.05
.\"
.nr PO 1.25i	\" page offset 1.25 inches
.nr PD .5v	\" inter-paragraph distance
.\"
.EH 'Multiple-project \fIimake\fP'- % -''
.OH ''- % -'Multiple-project \fIimake\fP'
.OF 'Revision date:\0\0\*(Rd''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(Rd''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.\" Ft - font reference
.\" $1 = font to which to use for .ft switches
.\" $2 = font for inline \f switches
.\" $3 = thing to put in the given font
.\" $4 = goes immediately after $3
.\" $5 = goes immediately before $4
.\" if $3-$5 are empty, simply switch font w/o switching back
.de Ft
.nr PQ \\n(.f
.ie \(ts\\$3\\$4\\$5\(ts\(ts .ft \\$1
.el \&\\$5\\f\\$2\\$3\\f\\n(PQ\&\\$4
..
.\"
.\" Cw - constant width font
.de Cw
.Ft \\*(cW \\*(cW "\\$1" "\\$2" "\\$3"
..
.\"
.\" Cb - constant bold font
.de Cb
.Ft \\*(cB \\*(cB "\\$1" "\\$2" "\\$3"
..
.\"
.\" I - italic font
.de I
.Ft I I "\\$1" "\\$2" "\\$3"
..
.\"
.\" B - bold font
.de B
.Ft B B "\\$1" "\\$2" "\\$3"
..
.\" start of section of code
.de cS
.br
.ie "\\$1"0" .DS "L" "\\$2" "\\$3"
.el .DS "\\$1" "\\$2" "\\$3"
.ps -2
.vs -2
.Cw
.\" 8n doesn't work.  How come?
.\".ta 8n +8n +8n +8n +8n +8n +8n +8n +8n
.nr Cw 8*\\w`n`
.ta \\n(Cwu +\\n(Cwu +\\n(Cwu +\\n(Cwu +\\n(Cwu +\\n(Cwu +\\n(Cwu +\\n(Cwu +\\n(Cwu
.rr Cw
..
.\" end of section of code
.de cE
.ft R
.vs +2
.ps +2
.br
.DE
..
.TL
Using imake for Multiple Projects
.AU
Paul DuBois
dubois@primate.wisc.edu
.AI
Wisconsin Regional Primate Research Center
Document revision:\0\0\*(Rn
Revision date:\0\0\*(Rd
.NH
Introduction
.LP
.I imake
is a tool for generating Makefiles and is used to allow software projects
to be configured easily on a wide variety of target machines.
It is distributed with projects such as the X Window System and the Kerberos
authentication system.
The tool
.I makedepend
is typically used in conjunction with
.I imake
to generate
.I Makefile
dependencies.
Another tool is used to bootstrap a
.I Makefile
from an
.I Imakefile .
This has traditionally been
.I xmkmf ;
a variant called
.I imkmf
is discussed here, too.
.LP
This document describes the use of
.I imake
to configure projects at the Wisconsin Regional Primate Research
Center.
The WRPRC configuration architecture allows multiple sets of configuration
files to peacefully coexist on a single machine so that any of them
can be applied to the configuration of an arbitrary number of
distinct projects.
It allows redundancy among project configurations
to be exploited while providing for project-specific information
which does not affect other projects.
Information on obtaining these tools and configuration files
is provided at the end of this document.
.LP
The method described here was patterned after the architecture of
the configuration files used in the X Window System (version 11, release 4).
This decision was made because that architecture was significantly more
developed than that of either X11R3 or Kerberos (then at protocol revision 4).
Since that time Kerberos V5 has been released (in beta) and its configuration
too has been patterned after X11R4.
X11R5 was released later yet; its architecture is essentially similar to
that of X11R4.
.LP
A more-than-passing acquaintance with
.I imake
is assumed on the reader's part.
Other documentation you might want to read
includes ``Configuration Management in the X Window System'' by Jim
Fulton (in
.I mit/doc/config/usenixws/paper.ms
from the X11 distribution)
and the configuration and
.I README
files (in the
.I mit/config
directory).
You should also be willing to look at the WRPRC configuration files firsthand.
.NH
Statement of Problem
.LP
.I imake
is useful because it helps you keep system dependencies out of your
source code and put them into a set of separate configuration files.
The files are used to select, on a platform-specific basis, the proper
configuration information for a particular system and to build
Makefiles accordingly.
Since
.I make
is assumed to be the main tool used to control a project's build process,
reconfiguring the
Makefiles
for a different system amounts to running
.I imake
again, rather than having to edit all the Makefiles by hand.
.LP
Having discovered the flexibility
.I imake
provides as a
configuration tool (once I figured out how to use it!),
I began to use it on all my projects.
Since these were all developed at the same site, the projects naturally
had quite a bit of configuration information in common.
At the same time, different projects had their own
individual requirements.
.LP
The X11R4 project allows configuration files to be found either
(a) in the
.I config
directory under the X top-level directory or (b) in a well-known
system-wide location (e.g.,
.I /usr/lib/X11/config ).
For my own projects I would have liked to
adopt approach (b) and install a
single set of system-wide configuration files and use them for all my
projects.
That wasn't suitable because the requirements for my projects
tended to differ somewhat.
Instead, I
adopted approach (a) on a per-project basis:
under each project's top-level directory I created a
.I config
directory and placed a full set of configuration files
in it, modified as necessary for the project.
.LP
This was the brute force approach, of course, but it worked .\^.\^. sort of.
It was most useful when moving a project to a
.I different
machine, since rebuilding the Makefiles to configure them
on a per-machine basis was much easier.
(That after all was the original purpose of
.I imake ).
The approach was less useful for sharing configuration information among
projects on the
.I same
machine, because of the multiple sets of files:
.IP \(bu
There was a great deal of redundancy between these sets
since projects were built and installed in similar ways.
That was dissatisfactory because it
seemed rather wasteful and silly not to take advantage of the overlap.
.IP \(bu
The many copies of files contributed to a certain amount of
maintenance difficulty.
For instance, if I decided to change the way a rule worked, and the rule
was used in several projects, that meant changing the rules file
for each of those projects.
.LP
The last result was somewhat paradoxical as
.I imake
is intended to reduce maintenance chores, not contribute to them.
Evidently a boundary condition on
.I imake 's
capabilities was being reached, which led to
two questions:
how can the number of configuration
files to be maintained be minimized, and how can
configuration files be shared among projects without sacrificing the ability
to specify project-specific information as necessary?
.NH
Solution to Problem
.LP
.I imake
possesses the ability to look through multiple directories when looking
for configuration files (via the
.I \-I
command line option), so to allow it to be used to configure multiple
projects with the same set of files the simplest solution to the problem
seemed to be:
.IP \(bu
Create a set of standard configuration files and store them in
a well-known public location for any project's use.
.IP \(bu
Allow projects to define an alternate directory in
which private project-specific configuration files can be placed.
.IP \(bu
Tell
.I imake
to look through the private project-specific directory first when searching
for configuration
files, then through the shared public directory for any files not found.
.LP
In this document
.Cw PubConfigDir
and
.Cw PrivConfigDir
denote the public and private configuration directories,
respectively.
.Cw PrivConfigDir
is the
.I config
directory under the project's top-level directory, i.e.,
.Cw $(TOP)\fI/config\fP .
.NH
Implementation of Solution
.LP
This section describes the X11R4 configuration file architecture and how
it was modified for multiple-project use.
.NH 2
Imake.tmpl Architecture
.LP
The architecture of
.I Imake.tmpl ,
the
.I imake
template
used in X11R4 is, roughly:
.LP
.cS
\fRvendor block section\fP
#include <\fIplatform\fP.cf>		\fR(platform-specific definitions)\fP
#include <site.def>		\fR(site-specific definitions)\fP
\fRsystem description, build definitions\fP
#include <Project.tmpl>		\fR(project-specific definitions)\fP
#include <Imake.rules>		\fR(\fP\fIimake\fP\fR rules)\fP
#include "./Imakefile"		\fR(\fP\fIImakefile\fP\fR for current directory)\fP
\fRdefault \fIMakefile\fP target entries\fP
.cE
.LP
The vendor block section determines the machine type and the
platform-specific file
.I platform.cf
to be included.
Information in
.I platform.cf
controls the selection of configuration information from the other included
files.
.LP
The X11R4 architecture can be modified to accommodate multiple projects.
To do this, extra
.Cw #include
files are introduced into
.I Imake.tmpl .
The resulting modified architecture is shown below, with the new files
in boldface:
.LP
.cS
\fRvendor block section\fP
.sp .2v
\f(CB#include <\fP\fIplatform\fP\f(CB.pcf>\fP
#include <\fIplatform\fP.cf>
.sp .2v
\f(CB#include <site.pdef>\fP
#include <site.def>
.sp .2v
\fRsystem description, build definitions\fP
.sp .2v
\f(CB#include <ProjectGroup.ptmpl>\fP
#include <ProjectGroup.tmpl>
.sp .2v
\f(CB#include <Imake.prules>\fP
#include <Imake.rules>
.sp .2v
#include "./Imakefile"
\fRdefault \fIMakefile\fP target entries\fP
.cE
.LP
This architecture differs from that used in X11R4 in the following respects:
.IP \(bu
The vendor block section now additionally determines the name of the
project-specific platform file
.I platform.pcf ,
not just
.I platform.cf .
For instance, the original and modified Sun vendor blocks look like this:
.LP
.cS
.ta 2.5i
\fBX11R4 vendor block		Modified vendor block\fP
.sp .2v
#ifdef sun	#ifdef sun
#define MacroIncludeFile <sun.cf>	#define MacroIncludeFile <sun.cf>
#define MacroFile sun.cf	#define MacroFile sun.cf
#undef sun	\f(CB#define ProjectMacroIncludeFile <sun.pcf>\fP
#define SunArchitecture	\f(CB#define ProjectMacroFile sun.pcf\fP
#endif /* sun */	#undef sun
	#define SunArchitecture
	#endif /* sun */
.cE
.IP \(bu
.I Project.tmpl
is renamed to
.I ProjectGroup.tmpl
to reflect that it applies to a group of projects, not a single project.
.IP \(bu
The template includes a number of additional files.
This modification is based on the notion of override files.
Before any of the platform, site, project-group or rules
files are included, another related file is
included which may contain project-specific information.
Default versions of these project-specific files
.I *.pcf , (
.I site.pdef ,
.I ProjectGroup.ptmpl
and
.I Imake.prules )
are present in
.Cw PubConfigDir
(to prevent
.I cpp
from issuing file-inclusion errors) but are all empty.
When a given project has special configuration requirements, it
supplies its own versions of any of these files in its private
configuration directory,
.Cw PrivConfigDir .
Telling
.I imake
to search that directory before
.Cw PubConfigDir
causes the project-specific
versions to be used, overriding the empty dummy versions.
.LP
The disadvantages of this architecture are obvious:
.IP \(bu
.I Imake.tmpl
is more complex.
.IP \(bu
.Cw PubConfigDir
must be populated with the additional
.I *.pcf ,
.I site.pdef ,
.I ProjectGroup.ptmpl
and
.I Imake.prules
files, although this is easy since they are all empty:
.LP
.cS
% touch site.pdef
% touch ProjectGroup.ptmpl
% touch Imake.prules
% ...
.cE
.LP
The advantages are:
.IP \(bu
Configuration information is centralized.
Multiple projects may use files in
.Cw PubConfigDir
cooperatively.
.IP \(bu
Flexibility is maintained.
Changes can be made to the configuration of one project without
affecting other projects, by specifying those changes in the project's
private files.
This contributes to configuration file stability, since the public files
don't change just because a single project changes.
.IP \(bu
On the other hand,
when changes (e.g., improvements, bug fixes)
.I do
need to be made to the public files, the changes are
immediately available to any project using them via a normal
configuration step, i.e.:
.LP
.cS
% make Makefile
% make Makefiles
.cE
.IP
(Of course, it is prudent to exercise due caution \*- it becomes possible
to break a lot of projects if an erroneous change is made.)
.IP \(bu
The number of configuration files referenced by
.I Imake.tmpl
is greater, but when multiple projects are maintained, the overall number of
configuration files involved is smaller and less space is used since
only one set of public files in needed.
.IP \(bu
The public configuration files need not be distributed with every project
that uses them.
They can be made into a separate distribution; only the project-specific
configuration files need be distributed with projects that use the public
files.
.LP
The philosophy underlying the architecture is that the public files
.I Imake.tmpl ,
.I *.cf ,
.I site.def ,
.I ProjectGroup.tmpl
and
.I Imake.rules
describe a common ``baseline'' or standard configuration, and
individual projects need to specify configuration information using
private
.I *.pcf ,
.I site.pdef ,
.I ProjectGroup.ptmpl
and
.I Imake.prules
files only to the extent that they
.I differ
from the standard.
Thus, private override files minimize the need for individual
projects to modify the baseline files directly.
.LP
Some of the ramifications of this architectural change
include:
.IP \(bu
At the one extreme, when projects have
no special project-specific configuration requirements (no
.I config
directory, or an empty one),
only the files in
.Cw PubConfigDir
are used to create Makefiles.
Since the default project-specific files are empty, the new
architecture of
.I Imake.tmpl
is functionally equivalent to the old.
At the other extreme, projects can supply
.I all
the configuration files if they wish so that only files in
.Cw PrivConfigDir
are used.
.IP \(bu
The public
.I site.def
file might be used by the system administrator to set the value of
a macro such as
.Cw BinDir
to some public directory in which general access
user programs are installed.
Invocations of installation rules would then normally refer to the
corresponding
.I make
variable
.Cw BINDIR .
If a project needs its programs installed somewhere else, this could be done
by defining
.Cw BinDir
in the project-specific site file
.I site.pdef
to override the value in
.I site.def .
.IP \(bu
Override files may also be used to extend the standard configuration
by defining symbols not appearing at all in the public files.
For instance, if some of a project's programs were to be installed
in the public
.Cw BinDir ,
but others were to be installed in a private directory, a
symbol such as
.Cw ProjBinDir
could be defined for the latter purpose in
.I ProjectGroup.ptmpl .
.NH 2
Other Architecture-related Changes
.LP
The architecture change described above required modification of
those things which cause
.I imake
to be executed, such as
the configuration file definitions used to construct
.I imake
commands, and the procedure for bootstrapping a
.I Makefile
from an
.I Imakefile .
.NH 3
IMAKE_CMD
.LP
.I imake
is invoked when ``make Makefile'' is performed, through the use of the
.I Makefile
variable
.Cw IMAKE_CMD .
The definition of this variable in the configuration file
.I ProjectGroup.tmpl
was changed.
.LP
.B Old
(from X11R4):
.cS
#ifdef UseInstalled
	IRULESRC = $(CONFIGDIR)
	IMAKE_CMD = $(IMAKE) -DUseInstalled \e
		-I$(IRULESRC) \e
		$(IMAKE_DEFINES)
#else
	IRULESRC = $(CONFIGSRC)
	IMAKE_CMD = $(NEWTOP)$(IMAKE) \e
		-I$(NEWTOP)$(IRULESRC) \e
		$(IMAKE_DEFINES)
#endif
.cE
.\" allow page break here if necessary...
.LP
.B New :
.cS
#ifdef UseInstalled
	IMAKE_CMD = $(IMAKE) -DUseInstalled \e
		-I$(NEWTOP)$(PROJCONFIGDIR) -I$(GROUPCONFIGIR) \e
		$(IMAKE_DEFINES)
#else
	IMAKE_CMD = $(NEWTOP)$(IMAKE) \e
		-I$(NEWTOP)$(PROJCONFIGDIR) -I$(GROUPCONFIGIR) \e
		$(IMAKE_DEFINES)
#endif
.cE
.LP
The old rule determines,
based on whether
.Cw UseInstalled
is defined or not,
whether to use a version of
.I imake
already installed in some public directory or
build it in the source tree.
That symbol also determines where to look for configuration files and
defines the value of
.Cw IRULESRC
accordingly.
If
.Cw UseInstalled
is undefined, searches look in
.Cw CONFIGSRC ,
the project's
.I config
directory, otherwise in
.Cw CONFIGDIR ,
the system-wide configuration directory.
.LP
This behavior is unsuitable for the purposes described here, since there
are two directories which should be searched, if they exist.
The new rule makes
.Cw UseInstalled
relevant only for determining where
to find (and possibly build)
.I imake ,
not where to look for configuration files.
The file are always searched for in both places, which renders
.Cw IRULESRC
obsolete.
.LP
.Cw UseInstalled
is typically relevant only for configuring
the WRPRC configuration distribution itself, which must build
.I imake
and cannot assume it's already installed somewhere.
Other projects most likely will be configured on the assumption that
.I imake
is publicly installed.
.NH 3
Bootstrapping Makefiles \*- xmkmf and imkmf
.LP
.I xmkmf
is used to bootstrap a
.I Makefile
from an
.I Imakefile ,
but will not
accommodate the two-directory-search scheme outlined above.
To handle this,
.I imkmf
was developed, essentially as a copy of
.I xmkmf
modified to search
.Cw PrivConfigDir
and
.Cw PubConfigDir ,
not just
.Cw PubConfigDir .
.LP
The name
.I imkmf
was chosen to better reflect the
.I imake -related
function of the program;
the name
.I xmkmf
reflects its X-specific orientation.
.NH 2
Test of Implementation
.LP
After making the changes described above, a flight test was arranged.
Nine projects developed under six different logins were converted to use
the public files.
Projects supplied override files where necessary.
These projects were all initially developed using configuration files
that bore a
fairly strong relationship to the public set, so it was expected
that conversion would not be unbearably odious.
The degree of relationship broke down as follows:
.IP \(bu
Two projects used configuration files that were identical (or nearly
so) to the public files.
Each of these projects, unlike the other seven, comprised only a
single source directory.
As expected, conversion was completely trivial, once the top-level
.I Makefile
was rebuilt with
.I imkmf .
.IP \(bu
Four projects required additional project-specific specification
beyond the public configuration
(special include file directories, link libraries,
installation directories, etc.).
These usually required only
.I site.pdef
and
.I ProjectGroup.ptmpl
files in their local
.I config
directories.
One project required a
.I platform.pcf
file.
Project owner and group names (used for install operations) were moved
into
.I site.pdef
files from
.I site.def
files, which were then deleted from the sets of private configuration files.
.IP \(bu
Three projects diverged from the public files fairly significantly,
because although their configuration files were initially set up from
files similar to the public ones, less effort had been exerted to keep
them in sync as project development progressed.
As cladists would say, they developed autapomorphies.\**
.FS
Although most cladists might decline to attribute these to a designer.
.FE
Besides the changes made to the projects described in the previous
paragraph, there were additional special commands for compilation, special
.I Imakefile
rules, etc.
Nevertheless, conversion proceeded smoothly and without incident.
.LP
One thing I had done in my original
.I Project.tmpl
files was to define certain
.I Makefile
variables directly (e.g.,
.Cw INCLUDESRC=$(TOP)/h ),
rather
with the standard X11R4 technique of using a previously
.Cw #define 'd
.I cpp
symbol (e.g.,
.Cw INCLUDESRC=IncludeSrc ,
where
.Cw IncludeSrc
is
.Cw #define 'd
within an
.Cw #ifndef/#endif
block).
This was a short-cut that works in a non-shared environment but
needed to be fixed to work in a shared environment.
A default value of
.Cw IncludeSrc
was placed in
.I ProjectGroup.tmpl ,
and individual projects redefine that symbol as necessary in
.I ProjectGroup.ptmpl .
.LP
It was also necessary to place a number of
.I cpp
symbol definitions in
.I ProjectGroup.tmpl
within
.Cw #ifndef/#endif
blocks, since they became liable to being
overridden in
.I ProjectGroup.ptmpl .
.LP
Overall, conversion was reasonably simple and straightforward.
I judged the ``conversion odiousness coefficient'' acceptably low.
.NH
Accommodating Multiple Groups of Projects
.LP
The architecture described so far allows a group of similarly-configured
projects to share configuration files and selectively override or extend
them as necessary.
This provides a solution to the original problem (how to avoid
maintaining multiple full sets of configuration files while maintaining
flexibility), but leads to another.
Suppose that instead of a group of projects, there exist several
groups of projects, such that projects within a group are similar, but
differences between groups are quite large.
This can happen, for instance, if projects are installed that originated
at several different sites.
It is likely that developers at different sites will, at the least, use
different sets of rules and test for different sets of system characteristics.
.LP
The difficulty here is that there are multiple sets of public configuration
files, and they cannot all be installed in the same well-known
location.
This problem can be overcome quite simply.
Instead of using
.Cw PubConfigDir
as the single well-known location in which
the public files are installed, define that directory as
.Cw PubConfigPath
and use it as the root of a set of
subdirectories, each of which corresponds to a set of configuration files.
Each set of files is installed in the appropriate directory, and
.Cw PubConfigDir
for that set is
defined in
.I ProjectGroup.tmpl
as that directory (e.g.,
.Cw PubConfigDir
for group
.I abc
is
.Cw PubConfigPath\fI/abc\fP ).
.LP
To allow
.I imkmf
to be used for bootstrapping,
it was modified slightly again to allow selection of any
of the configuration group subdirectories with a
.I \-C
option.
For flexibility, the argument following
.I \-C
may be absolute or relative.
If absolute, it is used unchanged.
If relative,
.Cw PubConfigPath
is prepended.
.LP
.I imkmf
as described has some significant differences from the
.I xmkmf
program distributed with X11R4.
However,
.I imkmf
can be used in a way compatible with
.I xmkmf
and thus used to configure X projects.
If the X configuration files are installed in
.I /usr/lib/X11/config
and the value of
.Cw PubConfigPath
is
.I /usr/lib/config ,
the X11R4 configuration files can be linked into the directory structure
used by
.I imkmf :
.LP
.cS
% cd /usr/lib/config
% ln -s /usr/lib/X11/config X11
.cE
.LP
For sites without symbolic links,
.I /usr/lib/config/X11
can be created as a regular directory into which the files from
.I /usr/lib/X11/config
are copied:
.LP
.cS
% cd /usr/lib/config
% mkdir X11
% cp /usr/lib/X11/config/* X11
.cE
.LP
Ugly, but workable.
(Also known as ``crude, but effective.'')
.LP
With these conventions,
projects within groups can share configuration files (as before),
project groups can coexist peacefully, and
WRPRC
.I imkmf
is compatible with the behavior of X11R4
.I xmkmf .
.LP
The nine projects previously converted were tested against these
additional modifications.
They required only the following to
properly reset the configuration information in the Makefiles:
.LP
.cS
% imkmf -C WRPRC
% make Makefiles
.cE
.LP
.I "\-C WRPRC" "" (
selects the standard Primate Center configuration files.)
.NH
Developing New Projects
.NH 2
Describing the Configuration
.LP
If a new project has any special configuration requirements, create a
.I config
directory and specify those requirements in private configuration files.
Also, create a
.I README
file explaining what they are.
.NH 2
Distributing Your Project
.LP
The mechanism described in this document solves
the problem of reusing
configuration files while allowing for extension and override within
individual projects.
However, when a project is packaged for distribution, it's very little
use to include only those configuration files which are unique to the
project if the receiving site doesn't have the public files.
It seems simplest to
provide the public files as a separate distribution, along with the
sources to the configuration tools.
This is perhaps inconvenient for the recipient, as it is another
distribution that needs to be retrieved.
On the other hand, it only needs to be done once, and then it's not
necessary to distribute all the configuration files with each project that
uses them.
.NH
Distribution and Update Availability
.LP
The WRPRC configuration distribution may be freely circulated and
is available for anonymous FTP access in the file
.I ~ftp/pub/imake-stuff/config-WRPRC.tar.Z
on host
.I ftp.primate.wisc.edu .
Updates appear there as they become available.
.LP
The WRPRC distribution
illustrates the concepts discussed in this document.
It also includes (modified) parts of the X11R4
.I mit/util
source tree (where
.I makedepend
is found) as well as
.I msub ,
a locally-developed utility for yanking the values of
.I make
variables out of Makefiles and using them to process templates to
produce target files such as
shell or
.I perl
scripts, help files that reflect local convention, etc.
.LP
The same directory contains the X11R4 configuration files, in
.I config-X11R4.tar.Z .
It might be instructive to compare them against the WRPRC files.
There is also a paper describing the X11R4 configuration architecture in some
detail.
.LP
If you do not have FTP access, send requests to
.I software@primate.wisc.edu .
Bug reports, questions, suggestions, and comments may be sent
to this address as well.
