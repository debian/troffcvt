.\"
.\" these macros are used in addition to the -ms macros.
.so tmac.wrprc
.\"
.\" ${TROFF} ${MSMACROS} thisfile | ${PRINTER}
.\"
.\" revision date - change whenever this file is edited
.ds RD 18 June 1996
.\"
.EH 'WRPRC2 \fIImakefile\fP Writing'- % -''
.OH ''- % -'WRPRC2 \fIImakefile\fP Writing'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.TL
Writing Project-Specific Configuration Files
.sp .3v
(WRPRC2 imake Configuration Files)
.AU
Paul DuBois
dubois@primate.wisc.edu
.AI
Wisconsin Regional Primate Research Center
Revision date:\0\0\*(RD
.LP
This document
describes how to provide project-specific configuration information
to extend or override the standard configuration provided by the WRPRC2
configuration files.
.LP
A separate document serves as a reference describing the contents of the
shared configuration files.
.\"
.Ah "Using Project-Specific Configuration Files"
.\"
.LP
The WRPRC2 configuration files provide ``standard'' configuration rules
and parameters by means of a set of files shared among all projects that
use them.
Sometimes the standard information provided by these
files is insufficient to configure a project.
The WRPRC2 architecture allows you to provide project-specific information.
This is done by allowing projects to create a
directory under the project root in which another set of configuration
files may be placed as necessary.
This section describes some of the ways in which you can use project-specific
configuration files to extend or override the base configuration capabilities
provided by the shared files.
.LP
To use project-specific configuration files, create a directory named
.I config
under the project root.
You can then create one or more of the following files in
.I config :
.Ps
Imake.p-params			\fRSystem and project parameters\fP
site.p-def				\fRSite parameters\fP
vendor.p-cf				\fRVendor parameters\fP
Imake.p-rules			\fRRule macros\fP
Imake.p-cf				\fRVendor blocks\fP
.Pe
If you add a parameter to
.I Imake.p-params ,
you may need to override it for particular systems in the
.I *.p-cf
files.
.\"
.Bh "BuildProgram() as a Basis for Other Rules"
.\"
.LP
If you find that you tend to invoke
.Cw BuildProgram()
in a stereotypical way for several targets within a project, you can
use the project's
.I Imake.p-rules
file to define a new rule that invokes
.Cw BuildProgram()
for you, perhaps supplying some of the arguments for you.
.Cw BuildSimpleProgram()
is in fact implemented as an invocation of
.Cw BuildProgram()
and some other rules.
Take a look at its definition to see how you can use
.Cw BuildProgram()
as a basis for other rules.

.LP
Do you want installed binaries stripped?
If not, override
.Cw InstStrip :
.Ps
#ifndef InstStrip
#define InstStrip /**/
#endif
.Pe
Do you want to specify a particular owner and/or group for installed
files?
If so, define
.Cw InstOwner
and/or
.Cw InstGroup :
.Ps
#ifndef InstOwner
#define InstOwner -o \f(CIname\fP
#endif
#ifndef InstGroup
#define InstGroup -g \f(CIname\fP
#endif
.Pe
.\"
.Ah "Interaction Problems To Watch Out For"
.\"
.LP
You can't set macros in
.I vendor.p-cf
based on
.Cw OSMajorVersion ,
.Cw OSMinorVersion ,
or
.Cw OSTeenyVersion ,
because they're not set until after
.I vendor.cf
is processed.
If you
.I must
make decisions based on the version numbers in
.I vendor.p-cf ,
you must set the version numbers in that file before you test them.
Your project instructions should note that people who use the project
should verify that the version numbers are set correctly on their own
machine.
.LP
Specifying a parameter in the
.Cw After\%VendorCF
part of
.I site.def
is ineffective if vendor files also set the parameter.
In such cases, it's better to modify the vendor file, or perhaps to
set the parameter in the
.Cw Before\%VendorCF
part of
.I site.def .
