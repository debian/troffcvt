/*
 * Vendor blocks for platform determination
 *
 * To add support for another platform:
 *
 *   1.  Put a new #ifdef block below that defines VendorIncludeFile,
 *       VendorFile, ProjectVendorIncludeFile and ProjectVendorFile for your
 *       new platform and then #undefs the machine-specific preprocessor
 *       symbols (to avoid problems with file names).
 *
 *   2.  Create .cf and .p-cf files with the names given by VendorFile and
 *       ProjectVendorFile.  The .cf file should contain any platform-specific
 *       parameter values to override the defaults in the *.tmpl files.  The
 *       .p-cf file should be empty.
 *
 *   3.  You may also need to rebuild imake to know about the new symbol.
 *
 * WARNING: if you copy a vendor block from X11's Imake.cf, note that it
 * it probably uses MacroFile and MacroInclude file to define the vendor.cf
 * file, which is DIFFERENT than what the WRPRC config files use.
 *
 * If you add support for a new system, please send the changes to
 * dubois@primate.wisc.edu.
 */


#ifdef Mips
/*
 * MIPS RISC/os cpp predefines nothing, so it has no unique symbol:
 * build imake with BOOTSTRAPCFLAGS="-DMips -DBSD43" or with
 * BOOTSTRAPCFLAGS="-DMips -DSYSV"
 */
#define VendorIncludeFile <Mips.cf>
#define VendorFile Mips.cf
#define ProjectVendorIncludeFile <Mips.p-cf>
#define ProjectVendorFile Mips.p-cf
#undef Mips
#undef mips
#ifdef SYSV
#define MipsSysvArchitecture	/* for SYSV OS environment */
#else
#define MipsBsdArchitecture	/* for BSD OS environment */
#endif
#define MipsArchitecture	/* for chipset type */
#endif /* Mips */

#ifdef ultrix
#define VendorIncludeFile <ultrix.cf>
#define VendorFile ultrix.cf
#define ProjectVendorIncludeFile <ultrix.p-cf>
#define ProjectVendorFile ultrix.p-cf
#ifdef vax
#undef vax
#define VaxArchitecture
#endif
#ifdef mips
#undef mips
#define MipsArchitecture
#endif
#undef ultrix
#define UltrixArchitecture
#endif

#if defined(vax) && !defined(UltrixArchitecture)
#define VendorIncludeFile <bsd.cf>
#define VendorFile bsd.cf
#define ProjectVendorIncludeFile <bsd.p-cf>
#define ProjectVendorFile bsd.p-cf
#undef vax
#define VaxArchitecture
#endif

#ifdef sun
#define VendorIncludeFile <sun.cf>
#define VendorFile sun.cf
#define ProjectVendorIncludeFile <sun.p-cf>
#define ProjectVendorFile sun.p-cf
# ifdef SVR4
/*#  undef SVR4*/
#  define SVR4Architecture
# endif
#ifdef sparc
#undef sparc
#define SparcArchitecture
#endif
#ifdef mc68000
#undef mc68000
#define Sun3Architecture
#endif
#ifdef i386
#undef i386
#define i386Architecture
#endif
#undef sun
#define SunArchitecture
#endif /* sun */

#ifdef hpux
#define VendorIncludeFile <hp.cf>
#define VendorFile hp.cf
#define ProjectVendorIncludeFile <hp.p-cf>
#define ProjectVendorFile hp.p-cf
#undef hpux
#define HPArchitecture
#endif /* hpux */

#ifdef __FreeBSD__
#define VendorIncludeFile <FreeBSD.cf>
#define VendorFile FreeBSD.cf
#define ProjectVendorIncludeFile <FreeBSD.p-cf>
#define ProjectVendorFile FreeBSD.p-cf
# undef __FreeBSD__
# define FreeBSDArchitecture
# ifdef __i386__
#  define i386BsdArchitecture
#  define i386Architecture
#  undef i386
# endif
#endif /* __FreeBSD__ */

#ifdef __NetBSD__
# define VendorIncludeFile <NetBSD.cf>
# define VendorFile NetBSD.cf
# define ProjectVendorIncludeFile <NetBSD.p-cf>
# define ProjectVendorFile NetBSD.p-cf
# undef __NetBSD__
# define NetBSDArchitecture
# ifdef __i386__
#  define i386BsdArchitecture
#  define i386Architecture
#  undef i386
# endif
# if defined(__sparc__) || defined(sparc)
#  define SparcArchitecture
#  undef sparc
# endif
# if defined(m68k)
#  undef mc68000
#  define Mc68000Architecture
# endif
#endif /* NetBSD */

#ifdef att
#define VendorIncludeFile <att.cf>
#define VendorFile att.cf
#define ProjectVendorIncludeFile <att.p-cf>
#define ProjectVendorFile att.p-cf
#undef att
#define ATTArchitecture
#endif /* att */

#ifdef apollo
#define VendorIncludeFile <apollo.cf>
#define VendorFile apollo.cf
#define ProjectVendorIncludeFile <apollo.p-cf>
#define ProjectVendorFile apollo.p-cf
#undef apollo
#define ApolloArchitecture
#endif /* apollo */

#ifdef sony
#define VendorIncludeFile <sony.cf>
#define VendorFile sony.cf
#define ProjectVendorIncludeFile <sony.p-cf>
#define ProjectVendorFile sony.p-cf
#undef sony
#define SonyArchitecture
#endif /* sony */

#ifdef M4310
#define VendorIncludeFile <pegasus.cf>
#define VendorFile pegasus.cf
#define ProjectVendorIncludeFile <pegasus.p-cf>
#define ProjectVendorFile pegasus.p-cf
#undef M4310
#define PegasusArchitecture
#endif /* M4310 */

#ifdef M4330
#define VendorIncludeFile <m4330.cf>
#define VendorFile m4330.cf
#define ProjectVendorIncludeFile <m4330.p-cf>
#define ProjectVendorFile m4330.p-cf
#undef  M4330
#define M4330Architecture
#endif /* M4330 */

#ifdef macII
/* A/UX cpp has no unique symbol:  build imake with BOOTSTRAPCFLAGS=-DmacII */
#define VendorIncludeFile <macII.cf>
#define VendorFile macII.cf
#define ProjectVendorIncludeFile <macII.p-cf>
#define ProjectVendorFile macII.p-cf
#undef  macII
#define MacIIArchitecture
#endif /* macII */

#ifdef CRAY
#define VendorIncludeFile <cray.cf>
#define VendorFile cray.cf
#define ProjectVendorIncludeFile <cray.p-cf>
#define ProjectVendorFile cray.p-cf
#undef cray
#undef CRAY
#define CrayArchitecture
#endif /* CRAY */

#ifdef sgi
#define VendorIncludeFile <sgi.cf>
#define VendorFile sgi.cf
#define ProjectVendorIncludeFile <sgi.p-cf>
#define ProjectVendorFile sgi.p-cf
#undef sgi
#define SGIArchitecture
#undef mips
#define MipsArchitecture
#endif

#ifdef stellar
#define VendorIncludeFile <stellar.cf>
#define VendorFile stellar.cf
#define ProjectVendorIncludeFile <stellar.p-cf>
#define ProjectVendorFile stellar.p-cf
#undef stellar
#define StellarArchitecture
#endif

#ifdef __OSF1__
#define VendorIncludeFile <osf1.cf>
#define VendorFile osf1.cf
#define ProjectVendorIncludeFile <osf1.p-cf>
#define ProjectVendorFile osf1.p-cf
#define OSF1Architecture
#undef __OSF1__
#ifdef __mips__
#undef __mips__
#define MipsArchitecture
#endif
#endif

/*
 * A convenience for people running on rt's since they define ibm032, and for
 * people running AIX (note that AOS will no longer be supported by IBM).
 * _IBMR2 added for RISC6000
 */
#if defined(_IBMR2) && !defined(ibm)
#define ibm
#endif

#if defined(ibm032) && !defined(ibm)
#define ibm
#endif

#if defined(aix) && !defined(ibm)
#define ibm
#endif

#if defined(ibm)
#define VendorIncludeFile <ibm.cf>
#define VendorFile ibm.cf
#define ProjectVendorIncludeFile <ibm.p-cf>
#define ProjectVendorFile ibm.p-cf
#undef ibm
#ifdef _IBMR2
#define RsArchitecture
#undef _IBMR2
#else
#define IBMArchitecture
#ifdef i386
#undef i386
#define PS2Architecture
#endif
#ifdef ibm032
#undef ibm032
#define RtArchitecture
#endif
#ifdef aix
#undef aix
#define AIXArchitecture
#endif
#endif
#endif /* ibm */

#ifdef linux 
#ifdef PPC
#define VendorIncludeFile <linux-pmac.cf>
#define VendorFile linux-pmac.cf
#define ProjectVendorIncludeFile <linux-pmac.p-cf>
#define ProjectVendorFile linux-pmac.p-cf
#undef linux
#define LinuxArchitecture
#define PPCArchitecture
#else
#define VendorIncludeFile <linux.cf>
#define VendorFile linux.cf
#define ProjectVendorIncludeFile <linux.p-cf>
#define ProjectVendorFile linux.p-cf
#undef linux
#define LinuxArchitecture
#define i386Architecture
#endif
#endif /* linux */

#ifdef NeXT
#define VendorIncludeFile <Next.cf>
#define VendorFile Next.cf
#define ProjectVendorIncludeFile <Next.p-cf>
#define ProjectVendorFile Next.p-cf
#undef Next
#define NeXTArchitecture
#endif /* Next */

#ifndef VendorIncludeFile
XCOMM WARNING:  Imake.tmpl not configured; guessing at definitions!!!
XCOMM This might mean that BOOTSTRAPCFLAGS wasn't set when building imake.
#define VendorIncludeFile <generic.cf>
#define VendorFile generic.cf
#define ProjectVendorIncludeFile <generic.p-cf>
#define ProjectVendorFile generic.p-cf
#endif
