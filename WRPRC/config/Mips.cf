/*
 * SET OS VERSION NUMBERS BEFORE MAKING MAKEFILES
 *
 * Also set the definition of MipsHasNroff according to whether you have
 * nroff on your machine.
 */

/*
 * 
 * Problems configuring for Mips RISC/os:
 * 
 * 1) The include hierarchy root is /usr/include for the System V environment
 * and /bsd43/usr/include for the BSD environment.  Similarly, the library
 * directories are /usr/lib and /bsd43/usr/lib.  The latter can be taken
 * care of by making sure the compiler knows which environment it's to
 * conform to, by passing -systype sysv or -systype bsd43 on the command
 * line.
 * 
 * The compiler will also look in the correct header file directory when
 * processing #include directives.  However, for generating header file
 * dependencies, the include hierarchy root must be passed explicitly to
 * makedepend, since makedepend knows nothing about any -systype
 * conventions (and makedepend gets confused about the argument following
 * -systype, anyway).  The include hierarchy root is passed by setting
 * StandardIncludes to the proper directory.  Eventually StandardIncludes
 * becomes part of the value of ALLINCLUDES, which is passed to makedepend
 * by DependTarget().
 * 
 * 2) Other configuration parameters vary when you use the System V environment.
 * Signal semantics differ, for instance.
 * 
 * 3) Pre-4.50 versions of RISC/os don't have the v?printf() functions in the
 * C library.  This file corrects for that by adding -ltermcap to
 * StandardLoadLibs (the v?printf functions are in -ltermcap).
 * 
 * 4) mipsinstall needs -f, or sometimes it will create a destination file
 * as a directory instead, and the install will fail. (If you want unstripped
 * binaries, define InstStrip as nothing, like this:
 *	#define InstStrip
 *
 * 5) Man page installation is difficult because of the arcane arrangement of
 * manual pages under RISC/os.  Also, they should be installed as nroff-ed
 * files.  However, nroff is not included with RISC/os by default.  If you
 * have nroff, define MipsHasNroff as YES below.  If this symbol is NO,
 * manual page rules are disabled so that "make install.man" is a nop.
 *
 * As near as I can tell, manual pages should be arranged as follows.
 * Under /usr/man, there are three directories:
 * a_man - for administrative sections (7, 8)
 * p_man - programmer sections (2-6)
 * u_man - user sections (1, 6)
 * Under these three directories, the manX and bsd_manX directories hold
 * the SystemV and BSD versions of commands. This means the location in which
 * a manual page is installed depends both on the section it should go in,
 * and the compilation environment.  Fun.
 *
 * Anyway, the way it's set up below works on my machine.  Your mileage may
 * vary.
 * 
 * 
 * To use imake with the BSD environment, it should have been built in an
 * imake source distribution using this command in the distribution root:
 * 
 * 	% make World BOOTSTRAPCFLAGS="-DMips -DBSD43" >& make.world
 * 
 * To build for the System V environment, do this instead:
 * 
 * 	% make World BOOTSTRAPCFLAGS="-DMips -DSYSV" >& make.world
 * 
 * Unfortunately, although you can do this for either environment, you can't
 * easily build the distribution for one environment and then use it later
 * to configure projects for the other environment.
 */


#ifndef OSName
#define OSName RISC/os 4.01
#endif
#ifndef OSMajorVersion
#define OSMajorVersion    4
#endif
#ifndef OSMinorVersion
#define OSMinorVersion    01
#endif

XCOMM platform:  Mips.cf 93/06/30
XCOMM operating system: OSName

#ifndef	CCompilerMajorVersion
#define	CCompilerMajorVersion	2
#endif
#ifndef	CCompilerMinorVersion
#define	CCompilerMinorVersion	0
#endif

/* set this early because stuff below depends on it */

#ifndef SystemV
#ifdef SYSV
#define SystemV			YES	/* need System V style */
#else
#define SystemV			NO	/* need BSD style */
#endif
#endif


/*
 * default for ExecableScripts is NO when SystemV is YES; but on RISC/os,
 * scripts are execable regardless of environment.
 */
#ifndef HasExecableScripts
#define HasExecableScripts	YES
#endif

#ifndef HasVarargs
#define HasVarargs		YES
#endif

/* Use signal semantics consistent with compilation environment */

#ifndef HasVoidSignalReturn
#if SystemV
#define	HasVoidSignalReturn	YES
#else
#define	HasVoidSignalReturn	NO
#endif
#endif

/*
 * Default value of ConfigDir is defined in terms of USRLIBDIR, which differs
 * depending on environment, so use /usr/local/lib literally.
*/

#ifndef ConfigRootDir
#define ConfigRootDir	/usr/local/lib/config
#endif


#ifndef UsrLibDir
#if SystemV
#define UsrLibDir /usr/lib
#else
#define UsrLibDir /bsd43/usr/lib
#endif
#endif

#ifndef IncludeRoot
#if SystemV
#define IncludeRoot /usr/include
#else
#define IncludeRoot /bsd43/usr/include
#endif
#endif

#ifndef LocalRootDir
#if SystemV
#define LocalRootDir /usr/local
#else
#define LocalRootDir /bsd43/usr/local
#endif
#endif

/* these don't need to be environment-specific */
#ifndef LocalAdmDir
#define LocalAdmDir /usr/local/adm
#endif
#ifndef LocalBinDir
#define LocalBinDir /usr/local/bin
#endif
#ifndef LocalEtcDir
#define LocalEtcDir /usr/local/etc
#endif

#ifndef StandardCppDefines
#if SystemV
#define StandardCppDefines -DSYSV
#else
#define StandardCppDefines -DBSD43
#endif
#endif

#ifndef StandardDefines
#define StandardDefines StandardCppDefines
#endif
/*
 * The include roots vary depending on SystemV/not-SystemV, so make sure
 * they're explicitly part of the include parameters so "make depend" works.
 */
#ifndef StandardIncludes
#define StandardIncludes -I$(LOCALINCLUDEROOT) -I$(INCLUDEROOT)
#endif

#ifndef HasGcc2
#ifndef CcCmd
#if SystemV
#define CcCmd cc -systype sysv
#else
#define CcCmd cc -systype bsd43
#endif
#endif
#endif

#ifndef StandardLoadLibs
#if SystemV
#define StandardLoadLibs	/* as nothing */
#else
/*
 * For BSD environment, need -ltermcap for v?printf.  It's not in
 * the BSD C library in releases of RISC/os prior to 4.50.
 */
#if OSMajorVersion > 4 || (OSMajorVersion == 4 && OSMinorVersion >= 50)
#define StandardLoadLibs	/* as nothing */
#else
#define StandardLoadLibs	-ltermcap
#endif
#endif
#endif


/*
 * I'm not sure about the utility of setting these correctly; there aren't
 * any lint libraries on my machine even for system libraries...
 */

#ifndef LintLibFlag
#define LintLibFlag -o	/* even if not SystemV */
#endif
#ifndef LintOpts
#define LintOpts -bh	/* even if not SystemV */
#endif



#ifndef InstallCmd
#define InstallCmd /etc/mipsinstall -f
#endif

/*
 * Manual page installation support.
 *
 * If nroff is present, use it to install nroff-ed manual pages.  Otherwise
 * disable the install rule.  (If you don't have nroff, but do have groff,
 * try setting MipsHasNroff to YES and defining nroff as groff -Tascii.)
 */

#ifndef MipsHasNroff
#define MipsHasNroff YES
#endif

#ifndef InstallManFile
#if MipsHasNroff
#define InstallManFile(target,name,dstsuffix,dir)			@@\
target:: i.name.man							@@\
i.name.man:: name.man							@@\
	MakeDir(dir)							@@\
	x=$(TMPDIR)/man$$$$ ; \						@@\
	$(NEQN) name.man | $(NROFF) $(MANMACROS) > $$x ; \		@@\
	$(INSTALL) $(INSTMANFLAGS) $$x dir/name.dstsuffix ; \		@@\
	$(RM) $$x
#else
#define InstallManFile(target,name,dstsuffix,dir)			@@\
target:: i.name.man							@@\
i.name.man:: name.man							@@\
	@echo "$(NROFF) unavailable, cannot install "name.man
#endif /* MipsHasNroff */
#endif /* InstallManFile */

#undef MipsHasNroff

#ifndef ManRoot
#define ManRoot /usr/man
#endif

/*
 * Sections 2-5 go under p_man
 * Sections 7,8 go under a_man
 * Sections 1,6,l,n go under u_man
 */

#ifndef Man2Dir
#if SystemV
#define Man2Dir $(MANROOT)/p_man/man$(MAN2SUFFIX)
#else
#define Man2Dir $(MANROOT)/p_man/bsd_man$(MAN2SUFFIX)
#endif
#endif

#ifndef Man3Dir
#if SystemV
#define Man3Dir $(MANROOT)/p_man/man$(MAN3SUFFIX)
#else
#define Man3Dir $(MANROOT)/p_man/bsd_man$(MAN3SUFFIX)
#endif
#endif

#ifndef Man4Dir
#if SystemV
#define Man4Dir $(MANROOT)/p_man/man$(MAN4SUFFIX)
#else
#define Man4Dir $(MANROOT)/p_man/bsd_man$(MAN4SUFFIX)
#endif
#endif

#ifndef Man5Dir
#if SystemV
#define Man5Dir $(MANROOT)/p_man/man$(MAN5SUFFIX)
#else
#define Man5Dir $(MANROOT)/p_man/bsd_man$(MAN5SUFFIX)
#endif
#endif

#ifndef Man7Dir
#if SystemV
#define Man7Dir $(MANROOT)/a_man/man$(MAN7SUFFIX)
#else
#define Man7Dir $(MANROOT)/a_man/bsd_man$(MAN7SUFFIX)
#endif
#endif

#ifndef Man8Dir
#if SystemV
#define Man8Dir $(MANROOT)/a_man/man$(MAN8SUFFIX)
#else
#define Man8Dir $(MANROOT)/a_man/bsd_man$(MAN8SUFFIX)
#endif
#endif

#ifndef Man1Dir
#if SystemV
#define Man1Dir $(MANROOT)/u_man/man$(MAN1SUFFIX)
#else
#define Man1Dir $(MANROOT)/u_man/bsd_man$(MAN1SUFFIX)
#endif
#endif

#ifndef Man6Dir
#if SystemV
#define Man6Dir $(MANROOT)/u_man/man$(MAN6SUFFIX)
#else
#define Man6Dir $(MANROOT)/u_man/bsd_man$(MAN6SUFFIX)
#endif
#endif

#ifndef ManLDir
#if SystemV
#define ManLDir $(MANROOT)/u_man/man$(MANLSUFFIX)
#else
#define ManLDir $(MANROOT)/u_man/bsd_man$(MANLSUFFIX)
#endif
#endif

#ifndef ManNDir
#if SystemV
#define ManNDir $(MANROOT)/u_man/man$(MANNSUFFIX)
#else
#define ManNDir $(MANROOT)/u_man/bsd_man$(MANNSUFFIX)
#endif
#endif

#ifndef ManDir
#define ManDir $(MANLDIR)
#endif

#ifndef ManSuffix
#define ManSuffix $(MANLSUFFIX)
#endif
