/*
 * Imake.tmpl - imake template for Wisconsin Regional Primate
 * Research Center configuration files, Release 2.08.
 *
 * Bootstrap using these files with:
 *
 *	% imboot -C WRPRC2 [ topdir ]
 *
 * topdir is the path to the project root.  It may be omitted if you
 * are in the project root.
 *
 * WRPRC 2.xx configuration file architecture and content was derived
 * from and influenced by configuration files from X11 (R4, R5, R6.x),
 * WRPRC (release 1), and InterViews (3.1).
 */

/*
 * These files assume you're using an imake from X11R6 or higher, which
 * takes care of XCOMM processing itself.  If you have an X11R5 imake, you'll
 * get a lot of XCOMM lines in your Makefiles.  In that case, try uncommenting
 * the line below.  (But you really should update your imake instead.)  If
 * you uncomment and your cpp complains about the # being a stringizing
 * operator, try using #define XCOMM \# instead.
 */

/*
#define XCOMM #
*/

XCOMM -------------------------------------------------------------------------
XCOMM Makefile generated from IMAKE_TEMPLATE and INCLUDE_IMAKEFILE.
XCOMM
XCOMM Platform-specific parameters may be set in the appropriate .cf and
XCOMM .p-cf configuration files.  Site-wide parameters may be set in the
XCOMM files site.def and site.p-def.  Full rebuilds are recommended if
XCOMM any parameters are changed.

#define	YES	1
#define NO	0

/* Vendor block selection */

#include <Imake.p-cf>
#include <Imake.cf>


XCOMM -------------------------------------------------------------------------
XCOMM site-specific configuration parameters that need to come before the
XCOMM platform-specific parameters - edit site.def or site.p-def to change
#define BeforeVendorCF
#include <site.p-def>
#include <site.def>
#undef BeforeVendorCF


XCOMM -------------------------------------------------------------------------
XCOMM Set the OS name and version macros if imake has defined them

#if !defined(OSName) && defined(DefaultOSName)
#define OSName		DefaultOSName
#endif
#if !defined(OSMajorVersion) && defined(DefaultOSMajorVersion)
#define OSMajorVersion	DefaultOSMajorVersion
#endif
#if !defined(OSMinorVersion) && defined(DefaultOSMinorVersion)
#define OSMinorVersion	DefaultOSMinorVersion
#endif
#if !defined(OSTeenyVersion) && defined(DefaultOSTeenyVersion)
#define OSTeenyVersion	DefaultOSTeenyVersion
#endif


XCOMM -------------------------------------------------------------------------
XCOMM platform-specific configuration parameters specific to this project
XCOMM edit ProjectVendorFile to change
#include ProjectVendorIncludeFile

XCOMM -------------------------------------------------------------------------
XCOMM platform-specific configuration parameters
XCOMM edit VendorFile to change
#include VendorIncludeFile

/*
 * Concat - concatenates two strings.
 */
#ifndef Concat
#if (__STDC__ && !defined(UnixCpp)) || defined(AnsiCpp)
#define Concat(a,b)a##b
#else
#define Concat(a,b)a/**/b
#endif
#endif

/*
 * Concat3 - concatenates three strings.
 */
#ifndef Concat3
#if (__STDC__ && !defined(UnixCpp)) || defined(AnsiCpp)
#define Concat3(a,b,c)a##b##c
#else
#define Concat3(a,b,c)a/**/b/**/c
#endif
#endif

/*
 * Concat4 - concatenates four strings.
 */
#ifndef Concat4
#if (__STDC__ && !defined(UnixCpp)) || defined(AnsiCpp)
#define Concat4(a,b,c,d)a##b##c##d
#else
#define Concat4(a,b,c,d)a/**/b/**/c/**/d
#endif
#endif



XCOMM -------------------------------------------------------------------------
XCOMM site-specific configuration parameters that go after the
XCOMM platform-specific parameters - edit site.def or site.p-def to change
#define AfterVendorCF
#include <site.p-def>
#include <site.def>
#undef AfterVendorCF


XCOMM -------------------------------------------------------------------------
XCOMM project-specific system and project description parameters
XCOMM Edit Imake.p-params to change
#include <Imake.p-params>

XCOMM -------------------------------------------------------------------------
XCOMM Standard system and project description parameters
XCOMM Edit Imake.params to change
#include <Imake.params>


XCOMM -------------------------------------------------------------------------
XCOMM Project-specific imake rules
XCOMM Edit Imake.p-rules to change
#include <Imake.p-rules>

XCOMM -------------------------------------------------------------------------
XCOMM Standard imake rules
XCOMM Edit Imake.rules to change
#include <Imake.rules>


XCOMM -------------------------------------------------------------------------
XCOMM start of Imakefile
#include INCLUDE_IMAKEFILE

XCOMM -------------------------------------------------------------------------
XCOMM common rules for all Makefiles
/*
 * These need to be here so that rules in Imakefile occur first;  the blank
 * emptyrule is to make sure that an empty Imakefile doesn't default to make
 * clean.
 */
emptyrule::

CleanTarget()
TagsTarget()
MakefileTarget()

help::
	@echo "'make Makefile' to rebuild Makefile"
	@echo "'make all' to make everything"
	@echo "'make target' to make a single target"
	@echo "'make depend' to generate header file dependencies"
	@echo "'make install' to install everything (except manual pages)"
	@echo "'make install.man' to install all manual pages"
	@echo "'make i.target' to install a single target"
	@echo "'make lint' to lint source files"
	@echo "'make lint.target' to lint source files for a single target"
	@echo "'make clean' to clean up"
	@make help_aux

help_aux::


#ifdef IHaveSubdirs
XCOMM -------------------------------------------------------------------------
XCOMM rules for building in SUBDIRS

MakeSubdirs($(SUBDIRS))
DependSubdirs($(SUBDIRS))
InstallSubdirs($(SUBDIRS))
InstallManSubdirs($(SUBDIRS))
CleanSubdirs($(SUBDIRS))
TagSubdirs($(SUBDIRS))
MakefileSubdirs($(SUBDIRS))

#else
XCOMM -------------------------------------------------------------------------
XCOMM empty rules for directories that do not have SUBDIRS

all::
depend::
lint::

install::
	@echo "install in $(CURRENT_DIR) done"

install.man::
	@echo "install.man in $(CURRENT_DIR) done"

Makefiles::

#endif /* if subdirectory rules are needed */

XCOMM -------------------------------------------------------------------------
XCOMM dependencies generated by makedepend
