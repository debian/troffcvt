Changes to the WRPRC configuration distribution for Release 1.03
1 July 1991

o	There is now an installation document.

o	Some Sun support was added, as I now have a Sun SPARCstation 2.

o	A small amount of largely untested HP-UX support was added.
	This was done some time ago, however, on a pre-snake machine.

o	StdConfigDir/STDCONFIGDIR and ProjConfigDir/PROJCONFIGDIR were
	renamed PubConfigDir/PUBCONFIGFIR and PrivConfigDir/PRIVCONFIGDIR
	to better reflect their functions as directories holding public
	(system-wide) and private (project-specific) configuration files.
	Once the distribution has been built and installed, there should
	be no impact on any other projects.

o	No longer uses a modified xmkmf.  imkmf is used instead, which
	allows xmkmf from X11R4 to exist unmolested on machines which
	have it.

o	Some additional rules and symbols were defined.  Of these, the most
	important is probably BootstrapTarget().  If the WRPRC
	configuration tools and files are installed, ``make Bootstrap''
	should be reasonably portable across platforms and allow
	bootstrapping even with a Makefile that was configured on some
	other system.

o	Default owner and group names used to be defined in the site.def
	file with the intentions that this would select the defaults for
	system-owned files and that it could it be modified on a
	project-specific basis in site.pdef.  The problem with this is that
	system ownership varies on a platform basis, not a site basis.  Now
	the names are defined in platform.cf and can be overridden in
	platform.pcf.

o	Further experience with the WRPRC configuration files led to some
	other changes to imkmf.  The original version (mimicking X11R4
	xmkmf), tried to determine whether to define UseInstalled based on
	its arguments.  However, sometimes it's useful to be able to force
	UseInstalled on or off unconditionally, so a -d option was added to
	force use of the installed programs with -DUseInstalled, and a -u
	option to force UseInstalled to be undefined.  This allows some ugly
	bootstrapping difficulties to be worked around.  imkmf can still
	be used to bootstrap Makefiles for X programs built outside of
	the X source tree, provided the compatibility hacks described in
	the installation document are followed.  imkmf was also modified
	to pass through -D and -U options unchanged.

