.\"
.\" these macros are used in addition to the -ms macros.
.so tmac.wrprc
.\"
.\"
.Ah "Limitations of the WRPRC2 Architecture"
.\"
.LP
The WRPRC2 configuration file architecture
explicitly allows for two levels at which configuration information may
be specified.
However, there are certain limitations inherent to the architecture.
It allows no way to specify information that applies
only to a given subtree of a project, for instance.
And there's no acknowledgment that more than two levels of
configuration files might be needed.
.LP
A fixed number of levels of specification can be implemented using a
static structure.
For instance, if rule macros can be specified on two levels, you can process
two files, one that's specific to the project being configured, and one that
provides the general baseline set of rules:
.Ps
#include <Imake.p-rules>	\fIProject-specific rule information\fP
#include <Imake.rules>		\fIBaseline rule information\fP
.Pe
However, we don't have a way to dynamically extend the number of levels
of file inclusion.
We could do so if we could do the following:
.B "Describe."
.LP
But this doesn't work.
.B "Say why."
.LP
.B "Similar problems for make variables (below)."
.LP
The notion of levels of configuration information specification can be 
considered more generally.
That is, how many and which levels should there be?
.LP
But the
more general problem is more difficult, i.e., how to allow a variable,
or arbitrary, number of levels of specification, such that you may not know
in advance how many levels there are.
You may want to access a set of files, which themselves access other files.
You don't necessarily want to know or care about what those other files are.
.LP
To illustrate:
The X11R5 configuration file architecture allows for one level of
configuration files.
If your project wants to provide its own configuration information, to
extend or override the X11 information, that can be difficult.
To circumvent this, the X11 architecture can be converted to a two-level
architecture similar to that used by the WRPRC2 files.
Thus an X project can use the X11 configuration files and provide its own
files, too.
.LP
Motif can be thought of as ``an X project''.
And the Motif distribution can then provide its own extension of the baseline
X11 configuration information.
But what if you're writing a project that uses Motif?
Now you have a third level.
Hm.
What to do?
Convert the X11 architecture to allow three levels?
That's not a solution, since your project may be intended for use by yet
other projects, and then you have four levels.
.LP
There are similar problems with respect to variable values.
For a given number of levels, you can concatenate a fixed number of
subsidiary variables:
.Ps
ALLINCLUDES = $(PROJECT_INCLUDES) $(STD_INCLUDES)
.sp .3v
 ALLDEFINES = $(STD_DEFINES) $(PROJECT_DEFINES)
.Pe
The order of information in variable assignments depends on how
the information is used.
For
.I \-I 's,
the parameters are given most-specific first.
For
.I \-D 's,
the parameters are given most-specific last.
.LP
This illustrates two kinds of information ``stacking'':
.Ls B
.Li
Specification of files to be processed:
.Ps
#include most-specific
\&...
#include least-specific
.Pe 0
.Li
Specification of
.I make
variable values:
.Ps
VAR = least-specific ... most-specific
.sp .3v
VAR = most-specific ... least-specific
.Pe 0
.Le


.LP
Problems.
No good mechanism for dynamic extension of file inclusion (late binding).
No good mechanism for dynamic extension of variable values (can't redefine
in terms of current value).
