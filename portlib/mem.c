/*
 * mem.c - memory operations
 */

#include	"portlib.h"

#if HAS_BCOPY

/*
 * Use bcopy(), bzero(), bcmp()
 */

#include	<strings.h>


void
BCopy (src, dst, n)
char	*src;
char	*dst;
int	n;
{
	bcopy (src, dst, n);
}


void
BZero (s, n)
char	*s;
int	n;
{
	bzero (s, n);
}


int
BCmp (s1, s2, n)
char	*s1;
char	*s2;
int	n;
{
	return (bcmp (s1, s2, n));
}

# else

/*
 * Use memcpy(), memset(), memcmp()
 */

#include	<string.h>


void
BCopy (src, dst, n)
char	*src;
char	*dst;
int	n;
{
	memcpy (dst, src, n);
}


void
BZero (s, n)
char	*s;
int	n;
{
	memset (s, '\0', n);
}


int
BCmp (s1, s2, n)
char	*s1;
char	*s2;
int	n;
{
	return (memcmp (s1, s2, n));
}

#endif /* HAS_BCOPY */
