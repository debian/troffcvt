/*
 * dirtest - test portlib directory-reading functions
 *
 * This program takes one argument, a directory name, and displays
 * the names of the files in that directory.
 *
 * 29 November 1996
 * Paul DuBois
 * dubois@primate.wisc.edu
 * http://www.primate.wisc.edu/people/dubois/
 */

#include	<stdio.h>

#include	"portlib.h"

int
main (argc, argv)
int	argc;
char	*argv[];
{
FILE	*f;
char	*p;

	--argc;		/* skip program name */
	++argv;

	if (argc != 1)
	{
		fprintf (stderr, "Usage: dirtest dirname\n");
		exit (1);
	}
	if (OpenDir (argv[0]) == 0)
	{
		fprintf (stderr, "could not open directory\n");
		exit (1);
	}
	while ((p = ReadDir ()) != (char *) NULL)
		printf ("%s\n", p);
	CloseDir ();
	exit (0);
}
