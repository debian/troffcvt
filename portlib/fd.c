/*
 * MaxFileDesc ()
 *
 * Returns the maximum number of file descriptors allowed.
 */

#include	"portlib.h"

#if HAS_SYSCONF

#include	<unistd.h>

int
MaxFileDesc ()
{
	return (sysconf (_SC_OPEN_MAX));
}

# else

int
MaxFileDesc ()
{
	return (getdtablesize ());
}

#endif /* HAS_SYSCONF */
