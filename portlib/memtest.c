/*
 * memtest - test portlib memory functions
 *
 * Does very rudimentary tests.
 *
 * 16 Janyary 1997
 * Paul DuBois
 * dubois@primate.wisc.edu
 * http://www.primate.wisc.edu/people/dubois/
 */

#include	<stdio.h>

#include	"portlib.h"

#define	bufSiz	10

int
main (argc, argv)
int	argc;
char	*argv[];
{
char	buf1[bufSiz], buf2[bufSiz];
int	result;
int	i;

	printf ("test BZero...");
	for (i = 0; i < bufSiz; i++)
		buf1[i] = 1;
	BZero (buf1, 10);
	result = 1;
	for (i = 0; i < bufSiz; i++)
	{
		if (buf1[i] != 0)
			result = 0;
	}
	printf ("%s\n", result ? "okay" : "NOT okay");

	printf ("test BCopy...");
	for (i = 0; i < bufSiz; i++)
	{
		buf1[i] = 1;
		buf2[i] = 2;
	}
	BCopy (buf1, buf2, 10);
	result = 1;
	for (i = 0; i < bufSiz; i++)
	{
		if (buf1[i] != buf2[i])
			result = 0;
	}
	printf ("%s\n", result ? "okay" : "NOT okay");

	printf ("test BCmp(1)...");
	for (i = 0; i < bufSiz; i++)
	{
		buf1[i] = 1;
		buf2[i] = 1;
	}
	printf ("%s\n", BCmp (buf1, buf2, bufSiz) == 0 ? "okay" : "NOT okay");

	printf ("test BCmp(1)...");
	for (i = 0; i < bufSiz; i++)
	{
		buf1[i] = 1;
		buf2[i] = 2;
	}
	printf ("%s\n", BCmp (buf1, buf2, bufSiz) != 0 ? "okay" : "NOT okay");

	exit (0);
}
