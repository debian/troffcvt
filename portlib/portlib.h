/*
 * portlib.h
 *
 * Portability Library stuff
 */

#ifndef _PORTLIB_H

#define _PORTLIB_H

extern int	AcquireLock ();
extern void	ReleaseLock ();

extern int	OpenDir ();
extern char	*ReadDir ();
extern void	CloseDir ();

extern int	MaxFileDesc ();

extern short	StrToShort ();
extern int	StrToInt ();
extern long	StrToLong ();
extern double	StrToDouble ();

extern void	BCopy ();
extern void	BZero ();
extern int	BCmp ();

#endif /* _PORTLIB_H */
