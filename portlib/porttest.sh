
# Script type:	Bourne shell

ato=n
dir=n
fd=n
lock=n
mem=n

if [ $# -eq 0 ]; then		# test all functions
	ato=y
	dir=y
	fd=y
	lock=y
	mem=y
else
	for a in $*; do
		case $a in
			ato) ato=y ;;
			dir) dir=y ;;
			fd) fd=y ;;
			lock) lock=y ;;
			mem) mem=y ;;
			*)
				echo "Usage: $0 [ato] [dir] [fd] [lock] [mem]" 1>&2
				;;
		esac
	done
fi

if [ $ato = y ]; then		# test string conversion
	echo test string conversion
	err=0
	n="12345"
	res=`./atotest i $n`
	if [ "$res" != "$n" ]; then
		echo "$n converted INCORRECTLY as $res"
		err=`expr $err + 1`
	fi
	n="-12345"
	res=`./atotest i $n`
	if [ "$res" != "$n" ]; then
		echo "$n converted INCORRECTLY as $res"
		err=`expr $err + 1`
	fi
	n="123456"
	res=`./atotest l $n`
	if [ "$res" != "$n" ]; then
		echo "$n converted INCORRECTLY as $res"
		err=`expr $err + 1`
	fi
	n="-123456"
	res=`./atotest l $n`
	if [ "$res" != "$n" ]; then
		echo "$n converted INCORRECTLY as $res"
		err=`expr $err + 1`
	fi
	n="1.5"
	res=`./atotest f $n`
	if [ "$res" != "$n" ]; then
		echo "$n converted INCORRECTLY as $res"
		err=`expr $err + 1`
	fi
	n="-1.5"
	res=`./atotest f $n`
	if [ "$res" != "$n" ]; then
		echo "$n converted INCORRECTLY as $res"
		err=`expr $err + 1`
	fi
	if [ $err -eq 0 ]; then
		echo "*** tests successful"
	fi
fi

if [ $dir = y ]; then		# test directory reading
	echo test directory reading
	err=0
	ls=`ls -a . | sort`
	ls2=`./dirtest . | sort`
	if [ "$ls" != "$ls2" ]; then
		echo "test FAILED"
		err=`expr $err + 1`
	fi
	if [ $err -eq 0 ]; then
		echo "*** tests successful"
	fi
fi

if [ $fd = y ]; then		# test file descriptor count
	echo test file descriptor count
	./fdtest
	echo "*** you will have to verify if this is correct yourself"
fi

if [ $lock = y ]; then		# test file locking
	echo "test file locking (takes a few seconds)"
	err=0
	./locktest > /dev/null 2>&1&
	sleep 2				# give locktest time to set lock
	./locktest > /dev/null 2>&1
	if [ $? -eq 0 ]; then		# preceding command should fail!
		echo "test 1 FAILED"
		err=`expr $err + 1`
	fi
	wait				# wait for first locktest to exit
	./locktest > /dev/null 2>&1
	if [ $? -ne 0 ]; then		# preceding command should succeed!
		echo "test 2 FAILED"
		err=`expr $err + 1`
	fi
	if [ $err -eq 0 ]; then
		echo "*** tests successful"
	fi
fi

if [ $mem = y ]; then		# test memory operations
	echo "test memory operations"
	err=0
	./memtest > /dev/null 2>&1&
	if [ $err -eq 0 ]; then
		echo "*** tests successful"
	else
		echo "memory tests FAILED"
	fi
fi

exit 0
