/*
 * atotest - test portlib string-to-number functions
 *
 * Syntax: atotest conversion-type string
 *
 * conversion-type should be i (int), l (long), or f (floating point).
 * string is the string to be converted to a number.
 * The output is the numeric value after conversion.
 *
 * 2 December 1996
 * Paul DuBois
 * dubois@primate.wisc.edu
 * http://www.primate.wisc.edu/people/dubois/
 */

#include	<stdio.h>

#include	"portlib.h"

static char	*usage =
"Usage: atotest conversion-type string\n\
conversion type should be one of:\n\
i\tinteger\nl\tlong integer\nf\tfloating-point\n";

int
main (argc, argv)
int	argc;
char	*argv[];
{
char	*str;
int	i;
long	l;
double	f;

	--argc;		/* skip program name */
	++argv;
	if (argc != 2)
	{
		fprintf (stderr, "%s", usage);
		exit (1);
	}
	str = argv[1];
	if (argv[0][0] == 'i' && argv[0][1] == '\0')
	{
		i = StrToInt (str);
		printf ("%d\n", i);
		exit (0);
	}
	if (argv[0][0] == 'l' && argv[0][1] == '\0')
	{
		l = StrToLong (str);
		printf ("%ld\n", l);
		exit (0);
	}
	if (argv[0][0] == 'f' && argv[0][1] == '\0')
	{
		f = StrToDouble (str);
		printf ("%g\n", f);
		exit (0);
	}
	fprintf (stderr, "%s", usage);
	exit (1);
}
