/*
 * ato.c - string-to-number conversions.
 *
 * The primary reason for these routines is to hide the need to know which
 * header file to include to get the proper declaration of the underlying
 * atox() routines.  Some systems declare them in math.h, others in stdlib.h.
 * When using portlib, include portlib.h, call the routines defined below,
 * and you'll get the correct results.
 *
 * This should use strtol() if that is available, probably.
 */

#include	<stdio.h>
#ifdef __STDC__
#include	<stdlib.h>
#else
#include	<math.h>
#endif

short
StrToShort (s)
char	*s;
{
	return ((short) atoi (s));
}

int
StrToInt (s)
char	*s;
{
	return (atoi (s));
}

long
StrToLong (s)
char	*s;
{
	return (atol (s));
}

double
StrToDouble (s)
char	*s;
{
	return (atof (s));
}
