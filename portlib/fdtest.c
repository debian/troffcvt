/*
 * fdtest - test portlib file-descriptor functions
 *
 * This program display the maximum number of file descriptors a system
 * should have.
 *
 * 29 November 1996
 * Paul DuBois
 * dubois@primate.wisc.edu
 * http://www.primate.wisc.edu/people/dubois/
 */

#include	<stdio.h>

#include	"portlib.h"

int
main (argc, argv)
int	argc;
char	*argv[];
{
	printf ("maximum file descriptors = %d\n", MaxFileDesc ());
	exit (0);
}
