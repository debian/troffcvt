/*
 * Return UNIX process id
 */

#include	<stdio.h>

#include	"troffcvt.h"


Param
GetPid (void)
{
	return ((Param) getpid ());
}
