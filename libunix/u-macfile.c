/*
 * u-macfile.c -- file operations for UNIX versions of translators.
 *
 * UnixOpenMacroFile () - Open a troff macro file.  Looks in the current
 * directory and the MACROLIBDIR directory.
 *
 * Exception: if file is an absolute pathname, look only for file as named.
 *
 * Returns NULL if file cannot be found and opened.
 */
 
#include	<stdio.h>

#include	"memmgr.h"
 
#include	"tcgen.h"
#include	"tcunix.h"
 
 
FILE *
UnixOpenMacroFile (char *file, char *mode)
{
FILE	*f;
char	buf[bufSiz];
char	*p;
 
	if ((f = fopen (file, mode)) != (FILE *) NULL)
		return (f);
	/* if abolute pathname, give up, else look in library */
	if (file[0] == '/')
	{
		return ((FILE *) NULL);
	}
	sprintf (buf, "%s/%s", MACROLIBDIR, file);
	f = fopen (buf, mode);	/* NULL if it fails */
	return (f);
}
