/*
 * Routine for opening troff macro files.  The procedure that's actually
 * used must be installed on a platform-dependent basis by the application
 * driver.
 */

#include	<stdio.h>

#include	"troffcvt.h"

/*
 * Open a macro file.
 */


static FILE	*(*macroFileOpen) () = NULL;



void
SetOpenMacroFileProc (FILE *(*proc) ())
{
	macroFileOpen = proc;
}


FILE *
OpenMacroFile (char *file, char *mode)
{
	if (macroFileOpen == NULL)
		return ((FILE *) NULL);
	return ((*macroFileOpen) (file, mode));
}
