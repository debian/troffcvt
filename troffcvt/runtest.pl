
# Script type: perl

require "getopts.pl";

($prog = $0) =~ s|.*/||;	# get script name for messages

$usage = "Usage: $prog [ -d ] [ testname ]";

&Getopts ("d") || die "$usage\n";
$showdiff = defined ($opt_d);

# unbuffer STDOUT
select (STDOUT);
$| = 1;

$alltests = (@ARGV == 0);	# run all tests if no specific test named

$troffcvt = "../troffcvt";
$actions = "../actions";

die "$prog: $troffcvt not found\n" unless -x $troffcvt;

if ($alltests == 0)
{
	@tests = @ARGV;
}
else
{
	print "Running all tests...\n";
	opendir (DH, ".") || die "$prog: can't read current directory\n";
	@tests = sort (grep (s/\.result$//, readdir (DH)));
	closedir (DH);
}

foreach $test (@tests)
{
	print "$test: ";
	if (! -f $test)
	{
		print "test input file not found\n";
	}
	elsif (! -f "$test.result")
	{
		print "test result file not found\n";
	}
	else
	{
		system "$troffcvt -A -a $actions $test > tmp 2>&1";
		$result = system "$(CMP) $test.result tmp > /dev/null 2>&1";
		$result /= 256;
		print $result == 0 ? "OK\n" : "*** not OK ***\n";
		if ($result != 0 && $showdiff)
		{
			print "diff $test.result tmp:\n";
			system "diff $test.result tmp";
		}
	}
}

unlink "tmp" unless $showdiff;

exit (0);
