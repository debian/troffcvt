# -man-specific stuff.

# The requests below are used for font changes, because if the groff -man
# macros are used, some of the macros with these names are implemented
# using "while", which troffcvt doesn't understand.

imm remove-name n B
req B parse-macro-args eol \
	push-string ".ie $$=0 .ft B\n.el \\&\\fB$*\\fP\n"

imm remove-name n BR
req BR parse-macro-args eol \
	push-string ".if $$=0 .ft B\n" \
	push-string ".if $$=1 \\fB$1\\fR\n" \
	push-string ".if $$=2 \\fB$1\\fR$2\n" \
	push-string ".if $$=3 \\fB$1\\fR$2\\fB$3\\fR\n" \
	push-string ".if $$=4 \\fB$1\\fR$2\\fB$3\\fR$4\n" \
	push-string ".if $$=5 \\fB$1\\fR$2\\fB$3\\fR$4\\fB$5\\fR\n" \
	push-string ".if $$>5 \\fB$1\\fR$2\\fB$3\\fR$4\\fB$5\\fR$6\n"

imm remove-name n BI
req BI parse-macro-args eol \
	push-string ".if $$=0 .ft B\n" \
	push-string ".if $$=1 \\fB$1\\fR\n" \
	push-string ".if $$=2 \\fB$1\\fI$2\\fR\n" \
	push-string ".if $$=3 \\fB$1\\fI$2\\fB$3\\fR\n" \
	push-string ".if $$=4 \\fB$1\\fI$2\\fB$3\\fI$4\\fR\n" \
	push-string ".if $$=5 \\fB$1\\fI$2\\fB$3\\fI$4\\fB$5\\fR\n" \
	push-string ".if $$>5 \\fB$1\\fI$2\\fB$3\\fI$4\\fB$5\\fI$6\\fR\n"

imm remove-name n I
req I parse-macro-args eol \
	push-string ".ie $$=0 .ft I\n.el \\&\\fI$*\\fP\n"

imm remove-name n IR
req IR parse-macro-args eol \
	push-string ".if $$=0 .ft I\n" \
	push-string ".if $$=1 \\fI$1\\fR\n" \
	push-string ".if $$=2 \\fI$1\\fR$2\n" \
	push-string ".if $$=3 \\fI$1\\fR$2\\fI$3\\fR\n" \
	push-string ".if $$=4 \\fI$1\\fR$2\\fI$3\\fR$4\n" \
	push-string ".if $$=5 \\fI$1\\fR$2\\fI$3\\fR$4\\fI$5\\fR\n" \
	push-string ".if $$>5 \\fI$1\\fR$2\\fI$3\\fR$4\\fI$5\\fR$6\\fR\n"

imm remove-name n IB
req IB parse-macro-args eol \
	push-string ".if $$=0 .ft I\n" \
	push-string ".if $$=1 \\fI$1\\fR\n" \
	push-string ".if $$=2 \\fI$1\\fB$2\\fR\n" \
	push-string ".if $$=3 \\fI$1\\fB$2\\fI$3\\fR\n" \
	push-string ".if $$=4 \\fI$1\\fB$2\\fI$3\\fB$4\\fR\n" \
	push-string ".if $$=5 \\fI$1\\fB$2\\fI$3\\fB$4\\fI$5\\fR\n" \
	push-string ".if $$>5 \\fI$1\\fB$2\\fI$3\\fB$4\\fI$5\\fB$6\\fR\n"

imm remove-name n RB
req RB parse-macro-args eol \
	push-string ".if $$=0 .ft R\n" \
	push-string ".if $$=1 \\fR$1\n" \
	push-string ".if $$=2 \\fR$1\\fB$2\\fR\n" \
	push-string ".if $$=3 \\fR$1\\fB$2\\fR$3\n" \
	push-string ".if $$=4 \\fR$1\\fB$2\\fR$3\\fB$4\\fR\n" \
	push-string ".if $$=5 \\fR$1\\fB$2\\fR$3\\fB$4\\fR$5\n" \
	push-string ".if $$>5 \\fR$1\\fB$2\\fR$3\\fB$4\\fR$5\\fB$6\\fR\n"

imm remove-name n RI
req RI parse-macro-args eol \
	push-string ".if $$=0 .ft R\n" \
	push-string ".if $$=1 \\fR$1\n" \
	push-string ".if $$=2 \\fR$1\\fI$2\\fR\n" \
	push-string ".if $$=3 \\fR$1\\fI$2\\fR$3\n" \
	push-string ".if $$=4 \\fR$1\\fI$2\\fR$3\\fI$4\\fR\n" \
	push-string ".if $$=5 \\fR$1\\fI$2\\fR$3\\fI$4\\fR$5\n" \
	push-string ".if $$>5 \\fR$1\\fI$2\\fR$3\\fI$4\\fR$5\\fI$6\\fR\n"
