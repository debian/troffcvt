/*
 * Unit conversion and formatting stuff.
 *
 * The basis for this comes from the troff manual, section 1.3,
 * although the basic resolution may be different.
 *
 * 1 point = 1/72 in.
 * 1 Pica = 1/6 in.
 *
 * Unit indicator 'x' means "ignore explicit unit indicators".
 */

#include	<stdio.h>
#include	<ctype.h>

#include	"etm.h"

#include	"troffcvt.h"


/*
 * Convert a number into basic units
 */

Param
ToBU (double n, int u)
{
Param	param;

	switch (u)
	{
	case 'x':	/* ignore unit indicator */
	case 'u':
		param = (Param) n;
		break;
	case 'v':
		param = (Param) (n * curVSize);
		break;
	case 'p':	/* p = 1/72 in. */
		param = (Param) ((n * resolution) / 72.0);
		break;
	case 'n':
		param = (Param) ((n * resolution * curSize) / 144.0);
		break;
	case 'm':
		param = (Param) ((n * resolution * curSize) / 72.0);
		break;
	case 'P':	/* P = 1/6 in. */
		param = (Param) ((n * resolution) / 6.0);
		break;
	case 'c':
		param = (Param) ((n * resolution * 50.0)/127.0);
		break;
	case 'i':
		param = (Param) (n * resolution);
		break;
	default:
		ETMPanic ("ToBU: bad units indicator <%c>", u);
	}
	return (param);
}


/*
 * Convert a number from basic units into some other unit.
 */

double
FromBU (Param n, int u)
{
double	v;

	switch (u)
	{
	case 'x':	/* x = ignore unit indicator */
	case 'u':	/* u = already in basic units */
		v = (double) n;
		break;
	case 'v':
		v = ((double) n / (double) curVSize);
		break;
	case 'p':
		v = ((double) (n * 72.0) / (double) resolution);
		break;
	case 'n':
		v = (Param) ((n * 144.0) / (double) (resolution * curSize));
		/*v = ((double) (n * 2.0) / (double) curSize);*/
		break;
	case 'm':
		v = (Param) ((n * 72.0) / (double) (resolution * curSize));
		/*v = ((double) n / (double) curSize);*/
		break;
	case 'P':
		v = ((double) (n * 6.0) / (double) resolution);
		break;
	case 'c':
		v = ((double) (n * 127.0) / ((double) (resolution * 50.0)));
		break;
	case 'i':
		v = ((double) n / (double) resolution);
		break;
	default:
		ETMPanic ("FromBU: bad units indicator <%c>", u);
	}
	return (v);
}
