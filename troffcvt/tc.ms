# -ms-specific stuff.  Helps a little to make title/author/etc. stuff better.

# define an internal-use-only request to reset the paragraph state

req reset*para*params eol \
	break center 0 fill adjust b font R \
	point-size 10 spacing 12 line-spacing 1

imm remove-name n TL
req TL parse-macro-args eol \
	push-string ".reset*para*params\n" \
	push-string ".ce 999\n.ps +2\n.vs 14\n.ft B\n"

imm remove-name n AU
req AU parse-macro-args eol \
	push-string ".reset*para*params\n" \
	push-string ".ce 999\n.ft I\n"

imm remove-name n AI
req AI parse-macro-args eol \
	push-string ".reset*para*params\n" \
	push-string ".ce 999\n"

# needs to write "ABSTRACT" if arg1 isn't "no"
imm remove-name n AB
req AB parse-macro-args eol \
	push-string ".reset*para*params\n" \
	push-string ".if !'$1'no' \\{\\\n.sp\n.ce\n\\fIABSTRACT\\fR\n.sp\n.\\}\n"

# groff -ms does a weird thing with .TS.  Disable it here.
imm remove-name n TS
req TS eol
