/*
 * Fake routine to return "process id" on Macintosh
 *
 * Actually, since System 7 is required for the applications now,
 * perhaps a real process id can be returned.
 */

# include	<stdio.h>

# include	"troffcvt.h"


Param GetPid ()
{
	return ((Param) 0);
}
