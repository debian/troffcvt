/*
	Allocate a piece of memory and return a pointer to it, or NULL
	if insufficient memory is available.
*/

# include	<stdio.h>

# include	"memmgr.internal.h"
# include	"memmgr.h"


char *AllocNP (size)
int	size;
{
#ifdef MALLOC_0_RETURNS_NULL
	++size;
#endif
	return ((char *) malloc ((allocSizeType) size));
}
