/*
	Panic version of StrAllocNP().
*/

# include	<stdio.h>

# include	"etm.h"

# include	"memmgr.internal.h"
# include	"memmgr.h"


char *StrAlloc (s)
char	*s;
{
char	*p;

	if (s == (char *) NULL)
		ETMPanic ("StrAlloc: trying to copy null pointer");
	if ((p = StrAllocNP (s)) == (char *) NULL)
		ETMPanic ("StrAlloc: out of memory");
	return (p);
}
