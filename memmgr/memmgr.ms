.\"
.\" xroff -ms % | lpr
.\"
.\" revision date - change whenever this file is edited
.ds RD 25 October 1991
.nr PO 1.2i	\" page offset 1.2 inches
.nr PD .7v	\" inter-paragraph distance
.\"
.EH 'Memory Manager Library'- % -''
.OH ''- % -'Memory Manager Library'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.\" I - italic font (taken from -ms and changed)
.de I
.nr PQ \\n(.f
.if t \&\\$3\\f2\\$1\\fP\&\\$2
.if n .if \\n(.$=1 \&\\$1
.if n .if \\n(.$>1 \&\\$1\c
.if n .if \\n(.$>1 \&\\$2
..
.de IS	\" interface routine description header start
.DS L
.ta .8i
.ft B
..
.de IE	\" interface routine description header end
.DE
.ft R
..
.TL
MemMgr \*- A Simple Memory Management Library
.sp .5v
.ps -2
Release 1.02
.AU
Paul DuBois
dubois@primate.wisc.edu
.AI
Wisconsin Regional Primate Research Center
Revision date:\0\0\*(RD
.NH
Introduction
.LP
.I MemMgr
is a fairly trivial memory management library.
There is little it does that cannot be done using routines in the C library.
(In fact, allocation and disposal is implemented using C library routines.)
The purposes of
.I MemMgr
are two-fold.
.IP (i)
Minimize configuration burden on applications that dynamically allocate
memory.
For instance,
.I malloc()
on some systems returns a
.I char
pointer; on others it returns a
.I void
pointer.
The
.I MemMgr
library routines encapsulate system-specific configuration
differences and exports a fixed interface which is system-independent.
Once you compile and install it, you just use it without thinking about
whether your UNIX is System V or BSD inspired.
.IP (ii)
Provide two parallel sets of allocation routines which either return NULL
(for applications which want to check) or panic (for applications which
simply want to die) on allocation failures.
Panicking is implemented using the ETM library, which introduces a dependency
on the ETM distribution.
So be it.
I use ETM for all my programs anyway.
.NH
Installation
.LP
This release of
.I MemMgr
is configured using
.I imake
and the standard WRPRC multiple-project configuration files, so you
also need to obtain the WRPRC configuration distribution (version 1.03 or
higher) if you want to build it the usual way.
(If you want to avoid
.I imake ,
the
.I Makefile
is simple enough that you should be able to tweak it by hand.)
.LP
You also need to obtain and install the Exception/Termination Manager
(ETM) library to build this distribution.
.LP
If you are using
.I imake ,
take a look at the files in the
.I config
directory and change things if you want.
Then
build the distribution with ``make World'' or:
.LP
.DS
make Bootstrap
make depend
make
make install
.DE
.NH
Programming Interface
.LP
Source files for applications using
.I MemMgr
routines should include
.I memmgr.h .
.LP
Applications using
.I MemMgr
should be linked with
.I "\-lmemmgr \-letm" .
.IS
char *AllocNP (size)
int	size;
.IE
Allocate
.I size
bytes of memory and return a pointer to it, or NULL if insufficient
memory is available.
The memory is not guaranteed to be zeroed.
.IS
char *Alloc (size)
int	size;
.IE
Like
.I Alloc() ,
but panics if insuffient memory is available.
.IS
char *StrAllocNP (s)
char	*s;
.IE
Allocates space for the string pointed to by
.I s
(including a null byte at the end),
copies the string into it, and returns a pointer to the copy.
Returns NULL if
.I s
is NULL or insufficient memory is available.
.IS
char *StrAlloc (s)
char	*s;
.IE
Like
.I StrAllocNP() ,
but panics if
.I s
is NULL or insuffient memory is available.
.IS
char *VAllocNP (count, size)
int	count, size;
.IE
Allocate
.I count
objects of
.I size
bytes of memory and return a pointer to it, or NULL if insufficient
memory is available.
The memory is not guaranteed to be zeroed.
.IS
char *VAlloc (count, size)
int	count, size;
.IE
Like
.I VAlloc() ,
but panics if insuffient memory is available.
.IS
char *ReallocNP (p, size)
char	*p;
int	size;
.IE
Reallocate the block of memory pointed to by
.I p
to be
.I size
bytes long, free
.I p
and return a pointer to the new block.
.I p
should have been allocated by one of the other
.I MemMgr
routines.
Returns NULL if insufficient memory is available; in this case the
block pointed to by
.I p
is
.B not
freed.
Also returns NULL if
.I p
is NULL.
.IS
char *Realloc (p, size)
char	*p;
int	size;
.IE
Like
.I Realloc() ,
but panics if
.I p
is NULL or insuffient memory is available.
.IS
void Free (p)
char	*p;
.IE
Frees the chunk of memory pointed to by
.I p ;
the memory should have been allocated by one of the other
.I MemMgr
routines, with the exception that it is safe to pass NULL to
.I Free() .
.NH
Distribution and Update Availability
.LP
The
.I MemMgr
distribution may be freely circulated and is available for
anonymous FTP access under the
.I ~ftp/pub
directory on host
.I ftp.primate.wisc.edu .
Updates appear there as they become available.
.LP
The ETM and WRPRC configuration distributions are available on
.I ftp.primate.wisc.edu
as well.
.LP
If you do not have FTP access, send requests to the address
.I software@primate.wisc.edu .
Bug reports, questions, suggestions and comments may be sent to
this address as well.
