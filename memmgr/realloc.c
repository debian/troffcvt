/*
	Panic version of ReallocNP().
*/

# include	<stdio.h>

# include	"etm.h"

# include	"memmgr.internal.h"
# include	"memmgr.h"


char *Realloc (p, size)
char	*p;
int	size;
{
	if (p == (char *) NULL)
		ETMPanic ("Realloc: null pointer");
	if ((p = ReallocNP (p, size)) == (char *) NULL)
		ETMPanic ("Realloc: out of memory");
	return (p);
}
