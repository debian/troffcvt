/*
	Free a hunk of memory returned by one of the other MemMgr routines.
	It's safe to pass NULL to this routine.
*/

# include	<stdio.h>

# include	"memmgr.internal.h"
# include	"memmgr.h"


void Free (p)
char	*p;
{
	if (p != (char *) NULL)
		(void) free ((allocPtrType *) p);
}
