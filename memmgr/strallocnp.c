/*
	Allocate space for a string, copy the string into it, and return
	a pointer to the copy, or NULL if couldn't get the memory.
*/

# include	<stdio.h>

# include	"memmgr.internal.h"
# include	"memmgr.h"


char *StrAllocNP (s)
char	*s;
{
char	*p;

	if (s == (char *) NULL)
		return ((char *) NULL);
	if ((p = AllocNP (strlen (s) + 1)) == (char *) NULL)
		return ((char *) NULL);
	return (strcpy (p, s));
}
