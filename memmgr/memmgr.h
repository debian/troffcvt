/*
 * memmgr.h - memory management library header file
 */

#ifndef _MEMMGR_H

#define _MEMMGR_H


/*
 * These defines cause failure at link time if this header
 * file wasn't included in each source file using MemMgr
 * functions.
 */

# define	AllocNP		AllocNP_
# define	Alloc		Alloc_
# define	StrAllocNP	StrAllocNP_
# define	StrAlloc	StrAlloc_
# define	VAllocNP	VAllocNP_
# define	VAlloc		VAlloc_
# define	ReallocNP	ReallocNP_
# define	Realloc		Realloc_
# define	Free		Free_

extern char	*AllocNP ();
extern char	*Alloc ();
extern char	*StrAlloc ();
extern char	*StrAllocNP ();
extern char	*VAllocNP ();
extern char	*VAlloc ();
extern char	*Realloc ();
extern char	*ReallocNP ();
extern void	Free ();

#endif /* _MEMMGR_H */
