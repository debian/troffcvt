/*
 * malloc0
 *
 * Test whether or not a zero-size allocation returns NULL.
 */

# include	<stdio.h>

# include	"memmgr.internal.h"
# include	"memmgr.h"


int
main (argc, argv)
int	argc;
char	*argv[];
{
char	*p;

	p = malloc ((allocSizeType) 0);
	printf ("malloc(0) returns %#lx\n", p);
}
