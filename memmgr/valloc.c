/*
	Panic version of VAllocNP().
*/

# include	<stdio.h>

# include	"etm.h"

# include	"memmgr.internal.h"
# include	"memmgr.h"


char *VAlloc (count, size)
int	count, size;
{
char	*p;

	if ((p = VAllocNP (count, size)) == (char *) NULL)
		ETMPanic ("VAlloc: out of memory");
	return (p);
}
