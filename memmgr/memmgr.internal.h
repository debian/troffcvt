#ifndef _MEMMGR_INTERNAL_H

#define _MEMMGR_INTERNAL_H

extern char	*strcpy ();

# if defined(__STDC__) || defined(THINK_C)
# include	<stdlib.h>
# define	allocPtrType	void
# define	allocSizeType	size_t
#  else /* !THINK_C */
# ifdef	SYSV
# include	<malloc.h>
# define	allocPtrType	void
# define	allocSizeType	size_t
# else	/* !SYSV */
# define	allocPtrType	char
# define	allocSizeType	unsigned
extern allocPtrType	*calloc ();
extern allocPtrType	*malloc ();
extern allocPtrType	*realloc ();
# endif /* SYSV */
# endif /* THINK_C */


#ifndef	allocSiz
#define	allocSiz	1024
#endif

#endif /* _MEMMGR_INTERNAL_H */
