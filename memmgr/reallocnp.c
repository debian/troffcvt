/*
	Reallocate p to size, free old memory and return pointer to new.
	If can't allocate the memory, returns NULL, and the old memory is
	NOT freed.
*/

# include	<stdio.h>

# include	"memmgr.internal.h"
# include	"memmgr.h"


char *ReallocNP (p, size)
char	*p;
int	size;
{
char	*s;

	if (p == (char *) NULL)
		return ((char *) NULL);
	if ((s = (char *) realloc (p, size)) == (char *) NULL)
		return ((char *) NULL);
	/*Free (p);*/	/* <- whoops! */
	return (s);
}
