/*
	Panic version of AllocNP().
*/

# include	<stdio.h>

# include	"etm.h"

# include	"memmgr.internal.h"
# include	"memmgr.h"


char *Alloc (size)
int	size;
{
char	*p;

	if ((p = AllocNP (size)) == (char *) NULL)
		ETMPanic ("Alloc: out of memory");
	return (p);
}
