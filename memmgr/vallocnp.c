/*
	Allocate a piece of memory to hold "count" elements of size "size",
	and return a pointer to it, or NULL if insufficient memory is
	available.
*/

# include	<stdio.h>

# include	"memmgr.internal.h"
# include	"memmgr.h"


char *VAllocNP (count, size)
int	count, size;
{
	return ((char *) calloc (count, (allocSizeType) size));
}
