Changes to troffcvt distribution for release 1.01.

(1.00 was the original unnumbered beta)

troffcvt is known to still have severe problems with -mm, but at
least it doesn't go into infinite recursion anymore.

Fixed macro body scanner to look for the proper control character
on the terminating line.  (Was looking for curCtrlChar, not ctrlChar.)

Added debug-flag and dump-macro actions.  Fake requests can be defined
in the actions file to make these available from within troff files.

Documentation is somewhat more complete.

.rm and .rn allow up to 40 arguments now.

Some additional registers are supported: .b, .c, c., $$, .R

Fixed bug whereby .nr for existing register didn't carry forward increment
value if increment param was missing on new .nr request.

Fixed \w bug - was picking up any unit character after closing delimiter;
shouldn't have been.  Same bug occurred with \h, \v and \x, too.

The line-drawing sequences \l and \L are supported (but incompletely) now.

troffcvt control output lines \motion c N and \line c N are now
\motion N c and \line N c [rep] (rep is the repetition character if
specified).

Better handling of escaped \x and special \(xx characters when given
in odd places (like ".cc \(*a")

ParseNameRef doesn't allow whitespace, fixing problems which occur
when "\f " is given.

tc2rtf special character file rtf-specials now split up into one file per
character set.  The four-file thing is more unwieldy but gets around a
couple of ugly problems.
tc2rtf and troff2rtf support options to allow desired character set to
be specified (Macintosh set still default and is most complete.)

Further imake-ized configuration information.

Changed meaning of "\space-size N" in troffcvt output.  N was formerly
expressed as the space size in basic units.  That's incorrect as the
space size depends on the current value of 1 em (and thus point size).
Now N is number of 1/36th ems.  This means postprocessors should
recompute space size whenever point size changes.

Motion support from Dave Davey added.

Changed troffcvt action names {control,special,text}-output to
to output-{control,special,text} (so they'd group together in
actions.ms :-)).

tc2rtf.c significantly reorganized to handle paragraph formatting
properties better (putatively).
