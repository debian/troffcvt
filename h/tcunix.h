/*
 * Library functions for UNIX versions of translators
 */

void	UnixSetProgPath (char *path);
FILE	*UnixOpenLibFile (char *file, char *mode);

FILE	*UnixOpenMacroFile (char *file, char *mode);
