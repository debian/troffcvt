/*
 * tc-mac.h
 */

/*
 * troffcvt folder name in Preferences folder
 */

#define	tcLibFolderName	"\ptroffcvt"

#define	docType		'TEXT'	/* default file type */
#define	docCreator	'ttxt'	/* default output/error file creator */

/*
 * Required resource numbers
 */

#define	aboutAlrtRes	1000		/* "About" alert */
#define	msgeAlrtRes		1001		/* general message alert */
#define	quitAlrtRes		1002		/* quit confirmation alert */

#define	whatStringRes	1000		/* "what application does" string */

#define	fileMenuRes		1000		/* File menu */
#define	editMenuRes		1001		/* Edit menu */

#define	statusWindRes	1000		/* status window */
#define	logWindRes		1001		/* log window */
#define	helpWindRes		1002		/* help window */

/*
 * Optional resource numbers
 */

#define	helpTextRes		1000		/* help window text */

#define	acurRes			128			/* animated cursor info */


typedef	struct AppState	AppState;

struct AppState
{
	short	documentsProcessed;
	Boolean	processingDocument;
	Boolean	quitRequestPending;
	Boolean	quitAfterDocsProcessed;
	FSSpec	fssInput;
	FSSpec	fssOutput;
	long	inputSize;
};


/*
 * Structure used by host application to pass information to MacintoshWrapper()
 * so it knows how to call writer code, and for extending the basic Macintosh
 * interface elements.
 *
 * The file suffixes should be Pascal strings and must be non-nil.
 */

typedef	struct AppInfo	AppInfo;

struct AppInfo
{
	void	(*pMenuInit) ();		/* application-specific menu setup */
	void	(*pMenuAdjust) ();		/* application-specific menu adjustment */

	void	(*pWriterInit) ();		/* writer one-time initialization */
	void	(*pWriterEnd) ();		/* writer one-time cleanup */

	int		(*pWriterBeginFile) ();	/* per-file initialization */
	void	(*pWriterEndFile) ();	/* per-file cleanup */

	void	(*pPrefs) ();			/* function to handle Preferences dialog */

	Str31	inputSuffix;			/* input file suffix to strip if present */
	Str31	outputSuffix;			/* output file suffix for autonaming */
	Str31	errorSuffix;			/* error file suffix for autonaming */

	long	outputCreator;			/* creator of output files */
	long	errorCreator;			/* creator of error files */

	int		flags;					/* flags to control wrapper behavior */
};

/*
 * If persistentStatus is set, the status window is always visible.  Otherwise,
 * it's visible only during file translations.
 *
 * If prefsInEditMenu is set, the wrapper adds a gray line and a
 * "Preferences..." item to the bottom of the Edit menu, and calls the pPrefs
 * function in the AppInfo structure to process the preferences dialog.
 *
 * If usesOutputFile is set, it means the translator expects to write to
 * an output file.  If the flag is set, the autoNameOutputFile and
 * autoOpenOutputFile flags are also examined (they're ignored if
 * usesOutputFile isn't set).
 * - If autoNameOutputFile is set, the output filename is automatically
 * generated, and the user can't override it.  If the flag isn't set, the
 * name is still generated, but used as the suggested default that the user
 * can override by means of the standard file dialog that is presented.
 * - If autoOpenOutputFile is set, the output file is opened by the wrapper on
 * stdout.  Otherwise, the writer is expected to open the output file itself.
 *
 * usesErrorFile and autoNameErrorFile are similar to autoNameOutputFile
 * and autoOpenOutputFile, but they apply to the error file.  There is no
 * autoOpenErrorFile; * if an error file name is used, the wrapper always
 * opens it (on stderr).
 *
 * The writer can call ETMMsg()/ETMPanic() even if no error file is created,
 * because diagnostic output will go the log window anyway.
 *
 * If the autoname flags are set, the file suffixes in the AppInfo structure
 * are used for creating the names.
 */

#define	persistentStatus	0x01
#define	prefsInEditMenu		0x02
#define	usesOutputFile		0x04
#define	autoNameOutputFile	0x08
#define	autoOpenOutputFile	0x10
#define	usesErrorFile		0x20
#define	autoNameErrorFile	0x40

#define	TstAppFlag(ap,mask)	((((ap)->flags) & (mask)) == (mask))

/* non-prototyped (because passed to troffcvt lib as callback) */

FILE *MacOpenLibFile ();


/* prototyped */

/* fileops.c */

void MakeDirPath (long dirID, short vRefNum, StringPtr path);
void MakeCFilePath (StringPtr dir, StringPtr basename, char *file);
char *AppFolderPath (void);
char *PrefsFolderPath (void);
Boolean SelectInputFile (FSSpec *fss, OSType type);
Boolean SelectOutputFile (FSSpec *fss, StringPtr fileType);
void MakeOutputFileName (StringPtr inName, StringPtr inSuffix,
							StringPtr outName, StringPtr outSuffix);

/* aevents.c */

void AppleEventsInit (void);
short PendingDocuments (void);
void AddDocument (FSSpec *fss);
Boolean RemoveDocument (FSSpec *fss);

/* helpwind.c */

void HelpWindow (void);

/* logwind.c */

void LogWindInit (void);

/* prefs.c */

Boolean ReadPrefs (StringPtr name, char **p, long *size);
Boolean WritePrefs (StringPtr name, OSType creator, char *p, long size);

/* statuswind.c */

void StatusWindInit (void);
void StatusWindUpdate (void);

/* misc.c */

StringPtr ApplicationName (void);
void PStrCpy (StringPtr dst, StringPtr src);
void PStrCat (StringPtr dst, StringPtr src);
void PStrInsert (StringPtr dst, StringPtr src);
Boolean PStrEqual (StringPtr s1, StringPtr s2);

/* wrapper.c */

void MacintoshWrapper (AppInfo *ap);
void RequestQuit (void);

extern AppState	*appState;
extern AppInfo	*appInfo;
