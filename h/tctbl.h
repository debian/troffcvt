/*
 * Stuff for handling tblcvt-related troffcvt output
 */

#define	tblMaxCol	200	/* should be enough...! */

/*
 * indices of \table-begin arguments
 */

#define	tblRowsArg	0
#define	tblColsArg	1
#define	tblHdrRowsArg	2
#define	tblAlignArg	3
#define	tblExpandArg	4
#define	tblBoxArg	5
#define	tblAllBoxArg	6
#define	tblDblBoxArg	7

/*
 * indices of \table-column-info arguments
 */

#define	colWidthArg	0
#define	colSepArg	1
#define	colEqWidthArg	2

/*
 * indices of \table-cell-info arguments
 */

#define	cellTypeArg	0
#define	cellVSpanArg	1
#define	cellHSpanArg	2
#define	cellVAdjustArg	3
#define	cellBorderArg	4
