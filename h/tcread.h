/*
 * tcread.h -- troffcvt reader header file
 */

#ifndef	bufSiz
#define	bufSiz	1024
#endif


/*
 * Return pointer to new element of type t, or NULL
 * memmgr.h should be included in any source file that makes
 * use of this.
 */

#define	New(t)	((t *) Alloc ((int) sizeof (t)))


#if defined(SYSV) || defined(SVR4)
#define	index	strchr
#endif


/*
 * How to add a new control or special text keyword:
 * 
 * 1) #define it below somewhere.
 * 2) Add it to the appropriate lookup table in r-read.c.  If it's
 * a control keyword, add it to the arg count table in r-read.c.
 * 3) Put it in the appropriate switch statements in your application,
 * and add code to take whatever action is necessary when it occurs.
 */

#define	tcrEOF		0	/* eof on input */
#define	tcrControl	1	/* control word */
#define	tcrText		2	/* literal text char */
#define	tcrSText	3	/* special text */


/*
 * Control class major numbers
 */

#define	tcrCUnknown	0
#define	tcrComment	1
#define	tcrSetupBegin	2
#define	tcrSetupEnd	3
#define	tcrResolution	4
#define	tcrPass		5
#define	tcrInputLine	6
#define	tcrOther	7

#define	tcrPointSize	10
#define	tcrSpaceSize	11
#define	tcrConstantWidth 12
#define	tcrNoConstantWidth 13
#define	tcrEmbolden	14
#define	tcrSEmbolden	15
#define	tcrFont		16

#define	tcrPageLength	20
#define	tcrBeginPage	21
#define	tcrPageNumber	22
#define	tcrOffset	23
#define	tcrNeed		24
#define	tcrMark		25

#define	tcrBreak	30
#define	tcrCFA		31	/* CFA = center, fill, adjust */
#define		tcrNofill	0
#define		tcrCenter	1
#define		tcrAdjFull	2
#define		tcrAdjLeft	3
#define		tcrAdjRight	4
#define		tcrAdjCenter	5
#define	tcrBreakSpread	32

#define	tcrSpacing	40
#define	tcrLineSpacing	41
#define	tcrSpace	42
#define	tcrExtraSpace	43

#define	tcrLineLength	50
#define	tcrIndent	51
#define	tcrTempIndent	52

#define	tcrDiversionBegin	60
#define	tcrDiversionAppend	61
#define	tcrDiversionEnd	62

#define	tcrResetTabs	70
#define	tcrFirstTab	71
#define	tcrNextTab	72
#define	tcrTabChar	73
#define	tcrLeaderChar	74

#define	tcrUnderline	80
#define	tcrNoUnderline	81
#define	tcrCUnderline	82
#define	tcrULineFont	83

#define	tcrMotion	90

#define	tcrBeginOverstrike 100
#define	tcrOverstrikeEnd 101
#define	tcrBracketBegin	102
#define	tcrBracketEnd	103
#define	tcrZeroWidth	104
#define	tcrLine		105

#define	tcrHyphenate	110

#define	tcrTitleLength	120
#define	tcrTitleBegin	121
#define	tcrTitleEnd	122

#define tcrTableBegin	130
#define	tcrTableEnd	131
#define	tcrColumnInfo	132
#define	tcrRowBegin	133
#define	tcrRowEnd	134
#define tcrRowLine	135
#define	tcrCellInfo	136
#define	tcrCellBegin	137
#define	tcrCellEnd	138
#define tcrCellLine	139
#define tcrEmptyCell	140
#define tcrSpannedCell	141


/*
 * Special text major numbers.  The only ones explicitly
 * assigned numbers are those built into troffcvt, for the
 * most part.
 */

#define	tcrSTUnknown	0
#define	tcrBackslash	1
#define	tcrAtSign	2
#define	tcrLSglQuote	3
#define	tcrRSglQuote	4
#define	tcrLDblQuote	5
#define	tcrRDblQuote	6
#define	tcrZeroSpace	7
#define	tcrTwelfthSpace	8
#define	tcrSixthSpace	9
#define	tcrDigitSpace	10
#define	tcrHardSpace	11
#define	tcrMinus	12
#define	tcrGraveAccent	13
#define	tcrAcuteAccent	14
#define	tcrBackspace	15
#define	tcrOptHyphen	16
#define	tcrTab		17
#define	tcrLeader	18
#define	tcrFieldBegin	19
#define	tcrFieldEnd	20
#define	tcrFieldPad	21


#define	tcrBold		0x01
#define	tcrItal		0x02

typedef	struct TCRFont	TCRFont;	/* font information */

struct TCRFont
{
	char	*tcrTroffName;		/* font's troff name */
	char	*tcrTypeFace;		/* typeface (font family) */
	long	tcrAtts;		/* font attributes */
	int	tcrFontNum;		/* font number */
	TCRFont	*nextTCRFont;		/* next font in list */
};



void	TCRInit (void);
int	TCRGetToken (void);
int	TCRGetLine (char *buf, int size, FILE *f);

int	TCRReadFonts (char *filename);
TCRFont	*TCRLookupFont (char *fontname);

extern TCRFont	*tcrFontList;
extern int	tcrClass;
extern int	tcrMajor;
extern int	tcrMinor;
extern int	tcrArgc;
extern char	*tcrArgv[];
extern long	tcrLineNum;
