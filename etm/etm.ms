.\"
.\" ${TBL} file | ${TROFF} ${MSMACROS} | ${PRINTER}
.\"
.\" This should be a constant-width font available at your site.
.\" If there isn't one, .fp 4 I is a suitable substitute.
.fp 4 CW
.\" revision date - change whenever this file is edited
.ds RD 10 April 1997
.nr PO 1.2i	\" page offset 1.2 inches
.nr PD .7v	\" inter-paragraph distance
.\"
.EH 'Exception and Termination Manager'- % -''
.OH ''- % -'Exception and Termination Manager'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.\" I - italic font (taken from -ms and changed)
.de I
.nr PQ \\n(.f
.if t \&\\$3\\f2\\$1\\fP\&\\$2
.if \\n(.$=0 .ft 2
.if n .if \\n(.$=1 \&\\$1
.if n .if \\n(.$>1 \&\\$1\c
.if n .if \\n(.$>1 \&\\$2
..
.\"
.\" B - boldface font (taken from -ms and changed)
.de B
.nr PQ \\n(.f
.if t \&\\$3\\f3\\$1\\fP\&\\$2
.if \\n(.$=0 .ft 3
.if n .if \\n(.$=1 \&\\$1
.if n .if \\n(.$>1 \&\\$1\c
.if n .if \\n(.$>1 \&\\$2
..
.\"
.\" Cw - constant width font
.de Cw
.nr PQ \\n(.f
.if t \&\\$3\\f4\\$1\\fP\&\\$2
.if \\n(.$=0 .ft 4
.if n .if \\n(.$=1 \&\\$1
.if n .if \\n(.$>1 \&\\$1\c
.if n .if \\n(.$>1 \&\\$2
..
.\" the first IS should be preceded by .RS and the text associated
.\" with the last IE should be followed by .RE.
.\"
.de IS	\" interface routine description header start
.RE
.DS L
.ta .5i +.5i +.5i
.Cw
..
.de IE	\" interface routine description header end
.DE
.ft R
.RS
..
.TL
.ps +2
ETM \*- A Program Exception and Termination Manager
.sp .5v
.ps
.AU
Paul DuBois
.H*ahref mailto:dubois@primate.wisc.edu
dubois@primate.wisc.edu
.H*aend
.AI
.H*ahref http://www.primate.wisc.edu/
Wisconsin Regional Primate Research Center
.H*aend
Revision date:\0\0\*(RD
.\"
.H*toc*title "Table of Contents"
.\"
.NH
Introduction
.LP
This document describes Exception and Termination Manager (ETM),
a simple(-minded) library to manage exceptional conditions that arise during
program execution, and to provide for orderly program shutdown.
.LP
There are at least a couple of approaches one may adopt for handling
error conditions within an application:
.IP \(bu
Have functions always return a value and have all callers test the return
value and respond accordingly.
.IP \(bu
Force the program to give up and exit early.
.LP
Each approach has strengths and weaknesses.
A difficulty with the first is that actions composed of
many subsidiary actions, each of which may themselves succeed or fail,
can easily become very unwieldy when an attempt is made to handle
all possible outcomes.
However, such a program will also continue in the face of extreme
adversity.
.LP
An advantage of the second approach is that it is, conceptually at least,
simpler to let a program die when a serious error occurs.
The difficulty lies in making sure the program
cleans up and shuts down properly before it exits.
This can be a problem especially when a program uses a number of independent
modules which can each encounter exceptional conditions and need
to be shut down, and which may know nothing of each other.
ETM is designed to alleviate the difficulties of this second approach.
.LP
The general architecture assumed for this discussion
is that of an application which uses
zero or more subsystems which may be more or less independent of each other,
and which may each require initialization and/or termination.
Also, other application-specific initialization and/or termination
actions may need to be performed which are
unrelated to those of the subsystems, e.g., temporary files
created at the beginning of the application
need to be removed before final termination, network connections
need to be shut down, terminal state needs to be restored.
.LP
Ideally, when an application executes normally, it will initialize,
perform the main processing, then shut down in an orderly fashion.
This does not always occur.
Exceptional conditions may be detected which necessitate a ``panic''
(an immediate program exit) because processing cannot continue further,
or because it is judged too burdensome to try to continue.
.LP
An individual subsystem may be easily written such that a panic within
itself causes its own shutdown code to be invoked.
It is more difficult to arrange for other subsystems to be
notified of the panic so that they can shut down as well, since the
subsystem in which the panic occurs may not even know about them.
.LP
An additional difficulty is that some exceptions may occur for
reasons not related to algorithmically detectable conditions.
For instance, the user of an application may cause a signal to be delivered
to it at any time.
This has nothing to do with normal execution and cannot be predicted.
.LP
The goals of ETM are thus twofold:
.IP (1)
Panics triggered anywhere within an application or any of its
subsystems should cause orderly shutdown of all subsystems and the
application itself.
.IP (2)
Signals that normally terminate a program should be caught and trigger
a panic to allow shutdown as per (1).
.NH
Processing Model
.LP
The model used by ETM
is that the application initializes subsystems in the order
required by any dependencies among them, and then terminates them in
the reverse order.
The presumption here is that if subsystem
.I ss2
is dependendent upon subsystem
.I ss1 ,
then
.I ss1
should be initialized first
and terminated last; the dependency is unlikely to make it wise
to shut down
.I ss1
before
.I ss2 .
.LP
ETM must itself be initialized before any other subsystem which uses it.
The initialization call,
.Cw ETMInit() ,
takes as an argument a pointer to a routine which performs
any application-specific
cleanup not related to its subsystems, or
.Cw NULL
if there is no such routine.
.LP
Each of the subsystems should then be initialized.
A subsystem's initialization routine should call
.Cw ETMAddShutdownProc()
to register its own shutdown routine with ETM, if there is one.
(Some subsystems may require no explicit initialization or termination.
However, if there is a shutdown routine, you should at least call
.Cw ETMAddShutdownProc()
to register it.)
.LP
When the program detects an exceptional condition, it calls
.Cw ETMPanic()
to describe the problem and exit.
.Cw ETMPanic()
is also called automatically when a signal is caught.
A message is printed, and all the shutdown routines that have
been registered are automatically executed, including the
application-specific one.
.LP
ETM is designed to handle shutting down under unusual
circumstances, but it also works well for terminating normally.
Instead of calling
.Cw ETMPanic() ,
the application calls
.Cw ETMEnd() .
This is much like calling
.Cw ETMPanic() ,
except that no error message is printed, and
.Cw ETMEnd()
returns to the caller.
which takes care of calling all the shutdown routines that have been
registered.
.LP
It is evident that the functionality provided by ETM
is somewhat like that of the
.Cw atexit()
routine provided on some systems.
Some differences between the two are:
.IP \(bu
.Cw atexit()
is either built in or not available.
ETM can be put on any system to
which it can be ported (extent unknown, but includes at least SunOS, Ultrix,
Mips RISC/os and THINK C).
.IP \(bu
ETM is more suited for handling
exceptional conditions.
.IP \(bu
ETM shutdown routines can be installed and removed later.
.Cw atexit()
provides only for installation (although you could simulate removal
by setting a flag which shutdown routines examine to see whether to
execute or not).
.LP
Here is a short example of how to set up and shut down using ETM.
.LP
.DS L
.ta .3i +.5i +1.5i
.Cw
main ()
{
	\&. . .
	ETMInit (Cleanup);	/* register application-specific cleanup */
	SS1Init ();	/* registers SS1End() for shutdown */
	SS2Init ();	/* registers SS2End() for shutdown */
	SS3Init ();	/* registers SS3End() for shutdown */
.sp .5v
	\&...\0main processing here\0...
.sp .5v
	ETMEnd ();	/* calls SS3End (), SS2End () and SS1End () */
	exit (0);
}
.ft R
.DE
.LP
Subsystems that are themselves built on other subsystems may follow
this model, except that they would not call
.Cw ETMInit()
or
.Cw ETMEnd() .
.LP
If there is no special initialization or shutdown activity,
and you don't care about catching signals,
it is not necessary to call
.Cw ETMInit()
and
.Cw ETMEnd() .
The application may still call
.Cw ETMPanic()
to print error messages and terminate.
(Even if the application does use
.Cw ETMInit()
and
.Cw ETMEnd() ,
it is safe to call
.Cw ETMPanic()
before any initialization has been done, because nothing needs to be
shut down at that point yet.)
.LP
If ETM itself encounters an exceptional condition (e.g., it cannot allocate
memory when it needs to), it will\*-of course\*-trigger a panic.
This should be rare, but if it occurs, ETM will generate a message
indicating what the problem was.
.NH
Caveats
.LP
Shutdown routines shouldn't call
.Cw ETMPanic() ,
since
.Cw ETMPanic()
causes shutdown routines to be executed.
ETM detects loops of this sort, but their occurrence
indicate a flaw in program logic.
Similarly, if you install a print routine to redirect ETM's
output somewhere other than
.Cw stderr ,
the routine shouldn't call ETM to print any messages.
.LP
.I "kill \-9"
is uncatchable and there's nothing you can do about it.
.NH
Programming Interface
.LP
The ETM library should be installed in
.Cw /usr/lib/libetm.a
or local equivalent, and applications should link in the ETM
library with the
.Cw \-letm
flag.
Source files that use ETM routines should include
.Cw etm.h .
If you use ETM functions in a source file without including
.Cw etm.h ,
you will get undefined symbol errors at link time.
.LP
The abstract types
.Cw ETMProcRetType
and
.Cw ETMProcPtr
may be used for declaring and passing pointers to functions that
are passed to ETM routines.
By default these will be
.Cw void
and
.Cw void(*)() ,
but on deficient systems with C compilers lacking
void pointers they will be
.Cw int
and
.Cw int(*)() ,
the usual C defaults for functions.
.LP
These types make it easier to declare properly typed functions and
.Cw NULL
pointers.
For instance, if you don't pass any shutdown routine to
.Cw ETMInit() ,
use
.LP
.DS
.Cw
ETMInit ((ETMProcPtr) NULL);
.ft R
.DE
.LP
If you do, use
.LP
.DS
.Cw
ETMProcRetType ShutdownProc () { . . . }
\&. . .
main ()
{
	\&. . .
	ETMInit (ShutdownProc);
	\&. . .
}
.ft R
.DE
.LP
Descriptions of the ETM routines follow.
.RS
.IS
ETMProcRetType ETMInit (p)
ETMProcPtr	p;
.IE
Registers the application's cleanup routine
.Cw p
(which should be
.Cw NULL
if there is none) and registers
default handlers for the following signals (all of which normally cause
program exit):
.Cw SIGHUP ,
.Cw SIGINT ,
.Cw SIGQUIT ,
.Cw SIGILL ,
.Cw SIGSYS ,
.Cw SIGTERM ,
.Cw SIGBUS ,
.Cw SIGSEGV ,
.Cw SIGFPE ,
.Cw SIGPIPE .
If
.Cw p
is not
.Cw NULL ,
it should point to a routine that takes no arguments and
returns no value.
.IS
ETMProcRetType ETMEnd ()
.IE
Causes all registered shutdown routines to be executed.
The application may then exit normally with
.Cw exit(0) .
.IS
ETMProcRetType ETMPanic (fmt, ...)
char	*fmt;
.IE
.Cw ETMPanic()
is called when a panic condition occurs, and the program cannot continue.
The arguments are as those for
.Cw printf()
and are used to print a message
after shutting down all subsystems and executing the application's
cleanup routine, and
before calling
.Cw exit() .
.Cw ETMPanic()
adds a newline to the end of the message.
.LP
.Cw ETMPanic()
may be called at any time, including prior to calling
.Cw ETMInit() ,
but only those shutdown routines which have been registered are invoked.
.LP
A common problem with applications that encounter exceptional conditions
such as segmentation faults is that you often don't see all the output your
application has produced.
This is because
.Cw stdout
is often buffered.
To alleviate this problem,
.Cw stdout
is flushed before any message is printed, so that any pending application
output is flushed and appears before the error message.
.LP
By default,
.Cw ETMPanic()
prints the message on
.Cw stderr .
This behavior may be modified with
.Cw ETMSetPrintProc() .
.LP
The default
.Cw exit()
value is 1.
This may be modified with
.Cw ETMSetExitStatus() .
.IS
ETMProcRetType ETMMsg (fmt, ...)
char	*fmt;
.IE
.Cw ETMMsg()
is like
.Cw ETMPanic()
except that it just prints the message and returns.
It is useful in that if panic message output has been redirected somewhere
other than
.Cw stderr
(e.g., to the system log),
.Cw ETMMsg()
will write its output there, too.
The application does not need to know whether such redirection has taken
place.
.LP
.Cw ETMMsg()
may be called at any time, including prior to calling
.Cw ETMInit() .
.IS
ETMProcRetType ETMAddShutdownProc (p)
ETMProcPtr	p;
.IE
Register a shutdown routine with ETM.
This is normally called within a subsystem's initialization routine.
.Cw p
should point to a routine that takes no arguments and
returns no value.
.IS
ETMProcRetType ETMRemoveShutdownProc (p)
ETMProcPtr	p;
.IE
Deregister a previously-registered shutdown routine with ETM.
This is useful for routines that only need to be registered temporarily,
e.g., during execution of some piece of code that temporarily creates some
file that needs to be removed if the program crashes, but which removes
it itself if execution proceeds normally.
.IS
ETMProcRetType ETMSetSignalProc (signo, p)
int	signo;
ETMProcPtr	p;
.IE
Register a signal-catching routine to override ETM's default.
The routine will be called with one argument, the signal number.
It should return no value,
.ft I
regardless of the usual return type of signal handler routines on
your system.
.ft R
(When ETM is configured on your system, it knows the proper return value
for
.Cw signal()
but hides differences among systems from your application so you don't
have to think about it.)
.LP
To return a signal to its default action or to cause a signal to be
ignored, pass the following values for
.Cw p
(these are defined in
.Cw etm.h ):
.LP
.DS
.ta 2i
\f4ETMSigIgnore\fP	signal is ignored
\f4ETMSigDefault\fP	signal default action is restored
.DE
.IS
ETMProcPtr ETMGetSignalProc (signo)
int	signo;
.IE
Returns the function current used to catch signal
.Cw signo ,
or
.Cw NULL
if the signal is handled with the default action or being ignored
(it's not possible to distinguish between the last two cases).
.IS
ETMProcRetType ETMSetPrintProc (p)
ETMProcPtr	p;
.IE
This routine is used to register a procedure that ETM can use to print
messages.
The default is to send messages to
.Cw stderr ,
which is appropriate for most programs.
Applications may prefer to send messages elsewhere.
For instance, non-interactive programs like network servers might
send them to
.Cw syslog()
instead.
Or a program may wish to send messages to multiple destinations.
.LP
To override the default, pass the address of an alternate print
routine to
.Cw ETMSetPrintProc() .
The routine should take one argument, a pointer to a character string,
and return no value.
The argument will be the fully formatted
panic message, complete with a newline on the end.
To restore the default, pass
.Cw NULL .
.LP
The printing routine shouldn't call
.Cw ETMPanic()
or
.Cw ETMMsg()
or a loop will be detected and ETM will conveniently panic
as a service to let you know you have a logic error in your program.
.IS
ETMProcPtr ETMGetPrintProc ()
.IE
Returns a pointer to the current printing function,
.Cw NULL
if the default is being used.
.IS
ETMProcRetType ETMSetExitStatus (status)
int	status;
.IE
This routine is used to register the status value that is passed to
.Cw exit()
when a panic occurs.
The default is 1.
For some applications it is desirable to return a different value.
For instance, a mail server that processes messages may send back a
message to the person who sent mail when a request is erroneous,
then panic (perhaps by writing a message to the system log).
On some systems, if a program invoked to handle mail returns non-zero,
the mailer will send another message to that person stating that there was
a problem handling the mail.
This extra message is unnecessary, and can be suppressed by registering an
exit status of 0.
.LP
If
.Cw ETMSetAbort()
has been called to force an
.Cw abort()
on a panic, the exit status is not returned.
.IS
int ETMGetExitStatus ()
.IE
Returns the current exit status which will be returned if a panic occurs.
.IS
ETMProcRetType ETMSetAbort (val)
int	val;
.IE
Calling this function with a non-zero value of
.Cw val
causes ETM to try to generate a core image when
.Cw ETMPanic()
is called (after the panic message is printed).
This can sometimes be useful for debugging.
If
.Cw val
is zero, image generation is suppressed.
The default is no image.
.LP
.Cw ETMSetAbort()
is meaningless on systems with no concept of a core image.
Also, if you install a signal catcher for
.Cw SIGABRT ,
you may end up in a panic loop.
.IS
int ETMGetAbort ()
int	val;
.IE
Return current image generation value.
.\" this RE should follow the last function description
.RE
