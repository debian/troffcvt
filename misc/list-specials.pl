
# Script type: perl5

# make-spec-list - read actions file for "imm special-char" lines and
# generate a troff-able file listing each one.  This can be run through
# troffcvt translators to see what they do with each one.

# This should also know about the characters in escMap and the quote
# characters... (although those must be written out differently).

($prog = $0) =~ s|.*/||;	# get script name for messages

$usage = "Usage: $prog";

die "$usage\n" if @ARGV;

$actions = "$(TOP)/troffcvt/actions";	# troffcvt actions file

# troff strings listing builtin special character names and invocation sequences

@bispecial = (
	"backslash \\ee \\\\",
	"at @ @",
	"quoteleft ` `",
	"quoteright ' '",
	"quotedblleft `` ``",
	"quotedblright '' ''",
	"zerospace \\e& not visible",
	"twelfthspace \\e^ not visible",
	"sixthspace \\e| not visible",
	"digitspace \\e0 not visible",
	"hardspace \\e(space) not visible",
	"minus \\e- \\-",
	"grave \\e` \\`",
	"acute \\e' \\'",
	"backspace \\e(backspace) not visible",
	"opthyphen \\e% \\%",
	"tab \\et not visible",
	"leader varies",
	"fieldbegin varies",
	"fieldend varies",
	"fieldpad varies"
);

# troff strings listing special character names and invocation sequences

@special = ();

open (IN, $actions) || die "$prog: cannot open $actions\n";
while (<IN>)
{
	chomp;
	@word = split (' ', $_);
	next unless $word[0] eq "imm";
	next unless $word[1] eq "special-char";
	$seq = $word[2];
	$seq =~ s/^(['"])(.+)\1$/$2/;
	$name = $word[3];
	warn "$name\n" if $name !~ /^@/;
	$name =~ s/^@//;
	push (@special, "$name \\e[$seq] \\[$seq]");
}
close (IN);

@bispecial = sort (@bispecial);
@special = sort (@special);

print ".nf\n";

printf "Built-in Special Characters:\n";
while (@bispecial)
{
	# put the colon in after the name (it's not put in above when the
	# string is initially created because it messes up sorting).
	($bispecial = shift (@bispecial)) =~ s/ /: /;
	print "\t$bispecial\n";
}

printf "\nactions File Special Characters:\n";
while (@special)
{
	# put the colon in after the name (it's not put in above when the
	# string is initially created because it messes up sorting).
	($special = shift (@special)) =~ s/ /: /;
	print "\t$special\n";
}

exit (0);
