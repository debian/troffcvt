.LP
This document describes the prerequisite software you'll need to build
the
.I troffcvt
distribution.
.LP
An ANSI C compiler to compile the C code.
.LP
Perl.
If you want to use
.I tblcvt
rather than
.I tbl
for preprocessing
.I tbl
input (hint: you do, you'll get better results), you'll need Perl 5.
The other Perl scripts can use either Perl 4 or Perl 5.
.LP
.LP
You need \fIimake\fP
and related build tools (e.g., \fImakedepend\fP, \fImkdirhier\fP)
.LP
The version of \fIimake\fP you use should be a version at least as recent
as the one distributed with the X Window System, Release 6 (X11R6).
.LP
If you don't have \fIimake\fP, you can get it by obtaining the \fIitools\fP
distribution.
This is available at:
.Ps
http://http.primate.wisc.edu/software/imake-book/
ftp://ftp.primate.wisc.edu/software/imake-book/
.Pe
Build and installation instructions for \fIitools\fP are available at the same
location.
Look for the "Appendix B" files.
These contain the text of
Appendix B of the O'Reilly imake book (the "boa book").

.LP
You need \fIimboot\fP for bootstrapping Makefiles.
\fIimboot\fP is included in the
\fIitools\fP distribution, and is also available in standalone form at:
.Ps
http://http.primate.wisc.edu/software/imake-stuff/
ftp://ftp.primate.wisc.edu/software/imake-stuff/
.Pe
\fIimboot\fP 1.03 was used to build this distribution.

.LP
You need \fImsub\fP for building one or more files in this distribution.
\fImsub\fP is included in the \fIitools\fP distribution,
and is also available in standalone form at:
.Ps
http://http.primate.wisc.edu/software/imake-stuff/
ftp://ftp.primate.wisc.edu/software/imake-stuff/
.Pe
\fImsub\fP 1.13 was used to build this distribution.

.LP
You need the WRPRC imake configuration files
.LP
The WRPRC configuration files are available at:
.Ps
http://http.primate.wisc.edu/software/imake-stuff/
ftp://ftp.primate.wisc.edu/software/imake-stuff/
.Pe
WRPRC release 2.11 was used to build this distribution.
.LP
The X11 configuration files are *not* used to build this distribution.

.LP
You need some other libraries
.LP
This distribution depends on some other libraries, which you should build
and install before attempting to build the
\fItroffcvt\fP distribution.
Each library is built using \fIimake\fP and the WRPRC configuration files.
Each is available at:
.Ps
http://http.primate.wisc.edu/software/
ftp://ftp.primate.wisc.edu/software/
.Pe
.LP
The specific libraries you'll need are:
.LP
The portlib portability library, available at:
.Ps
http://http.primate.wisc.edu/software/portlib/
ftp://ftp.primate.wisc.edu/software/portlib/
.Pe
portlib release 1.01 was used to build this distribution.

.LP
The ETM (Exception and Termination Manager) library, available at:
.Ps
http://http.primate.wisc.edu/software/ETM/
ftp://ftp.primate.wisc.edu/software/ETM/
.Pe
ETM release 1.09 was used to build this distribution.

.LP
The tokenscan token scanning library, available at:
.Ps
http://http.primate.wisc.edu/software/TS/
ftp://ftp.primate.wisc.edu/software/TS/
.Pe
tokenscan release 1.08 was used to build this distribution.

.LP
The MemMgr memory manager library, available at:
.Ps
http://http.primate.wisc.edu/software/MemMgr/
ftp://ftp.primate.wisc.edu/software/MemMgr/
.Pe
MemMgr release 1.04 was used to build this distribution.

