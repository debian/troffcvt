troffcvt (1.04+repack1-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 12 to 13.

 -- Colin Watson <cjwatson@debian.org>  Mon, 02 Jan 2023 14:04:20 -0000

troffcvt (1.04+repack1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dpkg-dev.
    + troffcvt: Drop versioned constraint on debianutils and groff in Depends.

  [ Colin Watson ]
  * Repackage using multiple upstream tarballs; split up patches.
  * Remove undefined macros from manual pages (closes: #679150).

 -- Colin Watson <cjwatson@debian.org>  Fri, 16 Dec 2022 23:10:34 +0000

troffcvt (1.04-26) unstable; urgency=medium

  [ Logan Rosen ]
  * patches/WRPRC-2.11.diff: Change ar command to use cq instead of clq to
    fix compatibility with binutils 2.36 (closes: #983264).

 -- Colin Watson <cjwatson@debian.org>  Tue, 24 May 2022 23:01:30 +0100

troffcvt (1.04-25) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/rules: Remove trailing whitespaces

  [ Debian Janitor ]
  * Drop no longer supported add-log-mailing-address setting from
    debian/changelog.
  * Bump debhelper from deprecated 7 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Colin Watson <cjwatson@debian.org>  Mon, 08 Feb 2021 21:45:52 +0000

troffcvt (1.04-24) unstable; urgency=medium

  * Switch to git; adjust Vcs-* fields.
  * Expand out the canned "build-easy-library" recipe in debian/rules; the
    use of a nested canned recipe doesn't seem to work with recent versions
    of GNU make (closes: #894563).
  * Change priority to optional, since "Priority: extra" is now deprecated.

 -- Colin Watson <cjwatson@debian.org>  Sun, 01 Apr 2018 23:43:17 +0100

troffcvt (1.04-23) unstable; urgency=medium

  * Depend on perl, not perl5 (thanks, Dominic Hargreaves; closes: #808324).

 -- Colin Watson <cjwatson@debian.org>  Fri, 18 Dec 2015 16:57:36 +0000

troffcvt (1.04-22) unstable; urgency=low

  * Add a Homepage field.
  * Simplify debian/rules using /usr/share/dpkg/buildflags.mk.
  * Use "find -perm /111" rather than "find -perm +111" (closes: #803018).

 -- Colin Watson <cjwatson@debian.org>  Tue, 27 Oct 2015 11:45:05 +0000

troffcvt (1.04-21) unstable; urgency=low

  * Use dpkg-buildflags to enable hardening options.
  * Build-depend on groff for the -ms macros.

 -- Colin Watson <cjwatson@debian.org>  Fri, 22 Jun 2012 14:34:34 +0100

troffcvt (1.04-20) unstable; urgency=low

  * Update Vcs-Bzr field for Alioth changes.
  * Add build-arch and build-indep targets.
  * Add ${misc:Depends}.

 -- Colin Watson <cjwatson@debian.org>  Sat, 12 Nov 2011 12:51:38 +0000

troffcvt (1.04-19) unstable; urgency=low

  * Quote ConfigRootDir in WRPRC-2.11/config/site.def so that words in the
    directory name that happen to correspond to imake definitions aren't
    expanded.  Should fix current build failures on kfreebsd-i386 and sparc.

 -- Colin Watson <cjwatson@debian.org>  Sat, 20 Feb 2010 23:40:54 +0000

troffcvt (1.04-18) unstable; urgency=low

  * Upgrade to debhelper v7.

 -- Colin Watson <cjwatson@debian.org>  Mon, 07 Sep 2009 02:09:51 +0100

troffcvt (1.04-17) unstable; urgency=low

  * Add a debian/rules patch target.
  * Add debian/README.source explaining our (messy) source package handling.
  * Imported into a branch on bzr.debian.org; add Vcs-Bzr control field.
  * Build-depend on xutils-dev (for makedepend and imake) rather than the
    obsolete xutils (closes: #485223).

 -- Colin Watson <cjwatson@debian.org>  Fri, 05 Sep 2008 17:40:11 +0100

troffcvt (1.04-16) unstable; urgency=low

  * patches/WRPRC-2.11.diff:
    - Don't strip binaries on 'make install'; let dh_strip do that according
      to DEB_BUILD_OPTIONS (closes: #438221).
  * Use debhelper v4.
  * patches/troffcvt-1.04.diff:
    - Always include <string.h> (I don't know how to fix the Imake
      configuration to detect it properly ...).
    - Remove contents of troffcvt-1.04/config/linux-pmac.p-cf, which serve
      only to interfere with our attempts to set groff's macro path
      correctly on powerpc.
    - Fix handling of tbl separator characters that also happen to be Perl
      regex metacharacters (thanks, Lars Helgeland; closes: #255132).
  * Correct tc2text(1) to talk about unroff rather than troff2text, though
    also create troff2text as a convenience symlink (closes: #397079).

 -- Colin Watson <cjwatson@debian.org>  Sun, 19 Aug 2007 11:28:12 +0100

troffcvt (1.04-15) unstable; urgency=low

  * Tweak short description for lintian's benefit.
  * Build msub with debugging symbols.
  * patches/msub-1.13.diff: Include <stdlib.h>, <string.h>, and <unistd.h>
    in msub.c rather than the incomplete list of manual prototypes. This
    seems to get rid of valgrind's complaints, so I think should fix the
    reported segfault; if it doesn't, please reopen (closes: #173010).
  * Policy version 3.5.6. Still need to sort out how to compile everything
    with debugging symbols in order to comply with later versions.

 -- Colin Watson <cjwatson@debian.org>  Sat, 14 Dec 2002 17:12:03 +0000

troffcvt (1.04-14) unstable; urgency=low

  * patches/troffcvt-1.04.diff: OK, so I should have better thought out how
    to handle groff's versioned library directory. Use 1.17.1-1's new
    'current' symlink.
  * debian/control: Bump groff dependency to >= 1.17.1-1.

 -- Colin Watson <cjwatson@debian.org>  Sat, 23 Jun 2001 01:47:19 +0100

troffcvt (1.04-13) unstable; urgency=low

  * New maintainer (closes: #100267).
  * Modernize packaging somewhat. Moved debian/mk-binary into debian/rules.
  * Compress all upstream changelogs and link the latest to changelog.gz.
  * 'set -e' all over the place so that I know when things fail.
  * patches/troffcvt-1.04.diff:
    - Change <sys/time.h> to <time.h> so that it builds (closes: #100643).
    - Cope with groff 1.17's macro file name changes (versioned macro
      directory, tmac.* -> *.tmac). Depend on groff (>= 1.17) as a result.

 -- Colin Watson <cjwatson@debian.org>  Wed, 13 Jun 2001 01:49:16 +0100

troffcvt (1.04-12) unstable; urgency=low

  * Added xutils to Build-Depends; closes: #93111

 -- Dr. Guenter Bechly <gbechly@debian.org>  Mon, 16 Apr 2001 09:41:35 +0200

troffcvt (1.04-11) unstable; urgency=low

  * Fixed error in post-installation script; closes: #92170

 -- Dr. Guenter Bechly <gbechly@debian.org>  Fri, 30 Mar 2001 10:33:35 +0200

troffcvt (1.04-10) unstable; urgency=low

  * Added Build-Depends in control.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Thu, 29 Mar 2001 15:30:37 +0200

troffcvt (1.04-9) unstable; urgency=low

  * Package adopted from new maintainer after negotiation with the previous
    maintainer. There was no ITA since the package was already removed from
    unstable.
  * Slight changes to the mk-binary file to fix some lintian warnings.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Thu, 18 Jan 2001 16:17:23 +0100

troffcvt (1.04-8) unstable; urgency=low

  * Migrate to FHS.

 -- Ben Pfaff <blp@gnu.org>  Sat, 13 Nov 1999 12:53:42 -0500

troffcvt (1.04-7) unstable; urgency=low

  * Depend on perl | perl5.

 -- Ben Pfaff <blp@gnu.org>  Sat, 10 Jul 1999 18:59:41 -0400

troffcvt (1.04-6) frozen unstable; urgency=low

  * troff2html: Create temp files securely with tempfile(1).  Fixes bug
    #29281 reported by Richard Kettlewell <rjk@greenend.org.uk>.

  * Therefore, depend on debianutils >= 1.6

 -- Ben Pfaff <pfaffben@pilot.msu.edu>  Fri, 13 Nov 1998 22:27:11 -0500

troffcvt (1.04-5) unstable; urgency=low

  * Fix permissions, for real this time.

 -- Ben Pfaff <pfaffben@pilot.msu.edu>  Sun, 22 Feb 1998 14:04:31 -0500

troffcvt (1.04-4) unstable; urgency=low

  * Fix permissions.

 -- Ben Pfaff <pfaffben@pilot.msu.edu>  Sun, 15 Feb 1998 15:27:00 -0500

troffcvt (1.04-3) unstable; urgency=low

  * Fix standards-version.

  * Add extended description.

  * Compress manpages.

  * Fix manpage permissions.

 -- Ben Pfaff <pfaffben@pilot.msu.edu>  Wed, 11 Feb 1998 18:55:09 -0500

troffcvt (1.04-2) unstable; urgency=low

  * chmod debian/mk-binary as executable.  (Bug #15682)

  * Fix hardcoded i386 in Makefile.  (Bug #15682)

 -- Ben Pfaff <pfaffben@pilot.msu.edu>  Tue, 27 Jan 1998 18:32:09 -0500

troffcvt (1.04-1) unstable; urgency=low

  * Initial release.

 -- Ben Pfaff <pfaffben@pilot.msu.edu>  Mon, 10 Nov 1997 19:19:31 -0500
