# if !defined(SYSV) && !defined(SVR4)
# define	memcmp	bcmp	/* assume BSD close-equivalent available */
# endif

/*
	Compare paragraph formats (passed by address)
*/

# define	ParFmtEq(a,b)	\
		(memcmp ((char *) a,(char *) b,(int) sizeof (ParFmt)) == 0)

# define	maxTabStops	35
# define	maxCharSet	4

/*
	Character sets - must be zero-based and sequential
*/

# define	ansiCharSet	0
# define	macCharSet	1
# define	pcCharSet	2
# define	pcaCharSet	3


/*
	FTabInfo - information to map the TCR typeface names onto the
	names to use in \fonttbl.
*/

typedef	struct FTabInfo	FTabInfo;


struct FTabInfo
{
	TCRFont	*tcrInfo;	/* info maintained by reader */
	char	*rtfName;	/* rtf name per charset */
	FTabInfo	*nextFTabInfo;
};


/*
	Special character structure
*/


typedef	struct SpChar	SpChar;

struct SpChar
{
	char	*spName;
	char	*spFont;
	char	*spValue;
	SpChar	*spNext;
};


/*
	Paragraph formatting properties.  All positional values are
	in twips.
*/

typedef	struct ParFmt	ParFmt;

struct ParFmt
{
	long	pJustify;		/* justification */
	long	pLIndent;		/* left indent */
	long	pRIndent;		/* right indent */
	long	pFIndent;		/* first indent */
	long	pSpaceBefore;		/* space before paragraph */
	long	pSpaceAfter;		/* space after paragraph */
	long	pSpaceBetween;		/* space between lines */
	int	pTabCount;		/* number of tabstops */
	long	pTabPos[maxTabStops];	/* tabstop positions */
	char	pTabType[maxTabStops];	/* tabstop types */
};


/*
	Character formatting properties (not used properly yet)
*/

typedef	struct CharFmt	CharFmt;

struct CharFmt
{
	int	dummy;
};


int		ReadFontInfo ();
int		ReadSpecials ();
FTabInfo	*LookupFont ();
SpChar		*LookupSpecial ();


extern FTabInfo	*fTabList;
extern SpChar	*spList;
extern int	charSet;
extern char	*charSetName;
