#define	defaultFontFile "html-fonts"
#define	specialCharFile	"html-specials"

#define	charsPerInch	10

#define	maxTabStops	35

#define	lf	'\n'		/* linefeed */
#define	tab	'\t'		/* tab */

#define	maxElt		20	/* max depth of element stack */
#define	maxTitle	2048	/* title buffer size */
#define maxHeader	2048	/* header buffer size */
#define maxTOC		4096	/* TOC entry size */

#define	titleTag	"TITLE"
#define	endTitleTag	"/TITLE"
#define	headerTag	"H1"
#define	endHeaderTag	"/H1"
#define tocTitleTag	"H2"
#define endTocTitleTag	"/H2"

#define displayFont	"CW"

/*
 * Document parts
 */

#define inNone		0	/* not in any document part */
#define inHead		1	/* in document HEAD part */
#define inBody		2	/* in document BODY part */

/*
 * Keyword types
 */

#define	keyUnknown	0
#define	keyAnchor	1
#define	keyElement	2

/*
 * Anchor types
 */

#define	anchorUnknown	0
#define	anchorHref	1
#define	anchorName	2
#define anchorTOC	3
#define	anchorEnd	4

/*
 * Document structural elements
 */

#define eltUnknown		0
#define eltTitle		1
#define	eltPara			2
#define	eltDisplay		3	/* i.e., preformatted text */
#define	eltDisplayEnd		4
#define	eltDisplayIndent	5
#define	eltList			6
#define	eltListEnd		7
#define	eltListItem		8
#define eltHeader		9
#define eltHeaderEnd		10
#define eltBlockquote		11
#define	eltBlockquoteEnd	12
#define	eltDefTerm		13
#define	eltDefDesc		14
#define eltShiftRight		15
#define	eltShiftLeft		16

/*
 * List types
 */

#define	listNone	0
#define	listUnordered	1
#define	listOrdered	2

/*
 * Definition-list states
 */

#define	defListNone	0
#define	defListTerm	1
#define	defListDesc	2

/*
 * Adjustment types (there is no "adjust-both"; \adjust-full maps to adjLeft)
 */

#define	adjLeft		0	/* left justify */
#define	adjCenter	1	/* center */
#define	adjRight	2	/* right justify */


/*
 * Font information structure
 */

typedef	struct Font	Font;

struct Font
{
	char	*fontName;
	char	*beginTag;
	char	*endTag;
	char	*dispBeginTag;
	char	*dispEndTag;
	short	inDisplay;
	Font	*next;
};


/*
 * Structure for mapping the keywords on \html control lines
 * to type/subtype codes.
 */

typedef	struct KeyMap	KeyMap;

struct KeyMap
{
	char	*name;
	short	type;
	short	subtype;
};


/*
 * Structure for maintaining an HTML element stack
 */

typedef	struct Elt	Elt;

struct Elt
{
	short	eltType;	/* type of element */
	Font	*eltFont;	/* current font/style when element entered */
	short	eltMisc;	/* list type, header level, etc. */
	Elt	*eltNext;
};


/*
 * Structure for maintaining the table-of-contents list
 */

typedef	struct TOC	TOC;

struct TOC
{
	short	tocLevel;	/* indentation level of entry */
	char	*tocStr;	/* text of entry */
	TOC	*tocNext;	/* next entry in list */
};


/*
 * FTabInfo - information to map the TCR typeface names onto the
 * names to use in \fonttbl.
 */

typedef	struct FTabInfo	FTabInfo;


struct FTabInfo
{
	TCRFont	*tcrInfo;	/* info maintained by reader */
	char	*rtfName;	/* rtf name per charset */
	FTabInfo	*nextFTabInfo;
};


/*
 * Special character structure
 */


typedef	struct SpChar	SpChar;

struct SpChar
{
	char	*spName;	/* name in troffcvt output */
	char	*spValue;	/* value to output for character */
	SpChar	*spNext;
};
