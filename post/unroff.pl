
# Script type: perl

# unroff - invoke troffcvt preprocessor and tc2text postprocessor

# Understands the usual troffcvt and tc2text arguments.  In addition,
# understands the following options:
# -n	no execution, just show commands that would be executed
# -p	don't run the postprocessor tc2text

# Adds in redefinition files according to any macro package that is named
# in the command-line arguments.  For example, if "-ms" is specified on the
# command line, unroff passes "-ms -a tc.ms" to troffcvt.

# 17 Mar 97
# Paul DuBois
# dubois@primate.wisc.edu
# http://www.primate.wisc.edu/people/dubois

# 17 Mar 97 V1.00
# - Created.

($prog = $0) =~ s|.*//||;		# get script name for messages

$troffcvt = "troffcvt";
$postproc = "tc2text";

$noexec = 0;
$nopostproc = 0;

$preargs = "-tnroff";
$postargs = "";

while (@ARGV)
{
	$_ = shift (@ARGV);		# get next argument
	if ($_ eq "-n")			# recognize and delete -n
	{
		$noexec = 1;
		next;
	}
	if ($_ eq "-p")			# recognize and delete -p
	{
		$nopostproc = 1;
		next;
	}
	if (!/^-./)			# not a flag, just a filename or "-"
	{
		$preargs .= " $_";
		next;
	}
	($opt, $rest) = /^-(.)(.*)/;
	if ($opt eq "m")		# -mxx macro package argument
	{
		die "$prog: -m requires macro package name\n" if $rest eq "";
		$preargs .= " -m$rest -a 'tc.m$rest'";
		next;
	}
	if ($opt =~ /^[ACl]$/)		# troffcvt arguments that stand alone
	{
		$preargs .= " -$opt";
		next;
	}
	if ($opt =~ /^[adrt]$/)		# troffcvt arguments that require an
	{				# additional argument
		if ($rest eq "")
		{
			die "$prog: -$opt requires following argument\n"
						if @ARGV == 0;
			$rest = shift (@ARGV);
		}
		$preargs .= " -$opt '$rest'";
		next;
	}
	if ($opt =~ /^[EJ8]$/)		# tc2text arguments that stand alone
	{
		$postargs .= " -$opt";
		next;
	}
	warn ("Bad argument: -$opt\n");
}

if ($nopostproc)
{
	# basic translation
	$cmd = "$troffcvt $preargs";
	warn "$cmd\n" if $noexec;
	system $cmd if !$noexec;
}
else
{
	# basic translation
	$cmd = "$troffcvt $preargs | $postproc $postargs";
	warn "$cmd\n" if $noexec;
	system $cmd if !$noexec;
}

exit (0);
