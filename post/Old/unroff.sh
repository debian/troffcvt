
#	Script type: Bourne shell

#	unroff - convert troff documents to text (smart deroff)

#	12 May 92	Paul DuBois	dubois@primate.wisc.edu

#	12 May 92 V1.00.  Created

args=
pargs=

for a in "$@"; do
	case $1 in
		-m*)
			macro=`echo $1|sed -e 's/^..//'`
			redefs=m$macro-redefs
			args="$args $1 -a $redefs"
			shift
			;;
		-join|-split?*)
			pargs="$pargs $1"
			shift
			;;
		-split)
			pargs="$pargs $1"
			shift
			if [ $# -gt 0 ]; then
				case $1 in
					[0-9]*)
						pargs="$pargs $1"
						shift
						;;
				esac
			fi
			;;
		-*)
			args="$args $1"
			shift
			;;
		*)
			break
			;;
	esac
done

if [ $# -eq 0 ]; then
	files="-"
else
	files="$@"
fi

troffcvt -tnroff $args $files | tc2text $pargs
