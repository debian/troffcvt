
#	Script type: Bourne shell

#	troff2null - "convert" troff documents to nothing

#	12 May 92	Paul DuBois	dubois@primate.wisc.edu

#	12 May 92 V1.00.  Created

args=

for a in "$@"; do
	case $1 in
		-m*)
			macro=`echo $1|sed -e 's/^..//'`
			redefs=m$macro-redefs
			args="$args $1 -a $redefs"
			shift
			;;
		-*)
			args="$args $1"
			shift
			;;
		*)
			break
			;;
	esac
done

if [ $# -eq 0 ]; then
	files="-"
else
	files="$@"
fi

troffcvt $args $files | tc2null $rargs
