
#	Script type: Bourne shell

#	troff2rtf - convert troff documents to RTF format.

#	07 May 92	Paul DuBois	dubois@primate.wisc.edu

#	07 May 92 V1.00.  Created

args=
rargs=

for a in "$@"; do
	case $1 in
		-m*)
			macro=`echo $1|sed -e 's/^..//'`
			redefs=m$macro-redefs
			args="$args $1 -a $redefs"
			shift
			;;
		-cs)
			if [ $# -lt 2 ]; then
				echo "Missing charset after -cs" 1>&2
				exit 1
			fi
			rargs="$rargs -cs $2"
			shift; shift
			;;
		-*)
			args="$args $1"
			shift
			;;
		*)
			break
			;;
	esac
done

if [ $# -eq 0 ]; then
	files="-"
else
	files="$@"
fi

troffcvt -ttroff -r1440 $args $files | tc2rtf $rargs
