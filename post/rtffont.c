# include	<stdio.h>
# ifndef	STRING_H
# define	STRING_H <string.h>
# endif
# include	STRING_H

# include	"etm.h"
# include	"tokenscan.h"
# include	"memmgr.h"

# include	"tcgen.h"
# include	"tcread.h"

# include	"tc2rtf.h"

/*
 * Read table for mapping typefaces onto names to use in \fonttbl
 */


int
ReadFontInfo (filename)
char	*filename;
{
FILE	*f;
char	buf[bufSiz];
TCRFont	*fi;
FTabInfo	*ft;
char	*face, *ftabName, *p;
int	i;

	if ((f = TCROpenLibFile (filename, "r")) == (FILE *) NULL)
		return (0);
	while (TCRGetLine (buf, (int) sizeof (buf), f))
	{
		if (buf[0] == '#')
			continue;
		TSScanInit (buf);
		if ((face = TSScan ()) == (char *) NULL)	/* typeface */
			continue;
		ftabName = (char *) NULL;
		/* names for each character set */
		for (i = 0; i < maxCharSet; i++)
		{
			if ((p = TSScan ()) == (char *) NULL)
				break;
			if (i == charSet)
				ftabName = p;
		}
		if (ftabName == (char *) NULL)
		{
			ETMMsg ("can't find charset name for typeface <%s>",
								face);
			continue;
		}
		for (fi = tcrFontList; fi != (TCRFont *) NULL; fi = fi->nextTCRFont)
		{
			if (strcmp (fi->tcrTypeFace, face) == 0)
				break;
		}
		if (fi == (TCRFont *) NULL)
		{
			ETMMsg ("can't find info for typeface <%s>", face);
			continue;
		}
		ft = New (FTabInfo);
		ft->tcrInfo = fi;
		ft->rtfName = StrAlloc (ftabName);
		ft->nextFTabInfo = fTabList;
		fTabList = ft;
	}
	(void) fclose (f);
	return (1);
}


/*
 * Read special character list.  Each line has the character name
 * and the RTF string to write out for a given RTF character set.
 * (Character sets being one of those things that makes RTF
 * "portability" suspect.)
 */

int
ReadSpecials (filename)
char	*filename;
{
FILE	*f;
char	buf[bufSiz];
SpChar	*sp;
char	*name, *font, *value;


	if ((f = TCROpenLibFile (filename, "r")) == (FILE *) NULL)
		return (0);
	while (TCRGetLine (buf, (int) sizeof (buf), f))
	{
		if (buf[0] == '#')
			continue;
		TSScanInit (buf);
		if ((name = TSScan ()) == (char *) NULL)	/* char name */
			continue;
		if ((font = index (name, '/')) != (char *) NULL)
			*font++ = '\0';
		value = TSScan ();	/* NULL if missing */
		sp = New (SpChar);
		sp->spName = StrAlloc (name);
		if (font != (char *) NULL)
			sp->spFont = StrAlloc (font);
		else
			sp->spFont = (char *) NULL;
		/*
		 * If value missing, make NULL; this will cause writer
		 * to write "[[name]]" instead of dropping it.
		 */
		if (value == (char *) NULL)
			sp->spValue = (char *) NULL;
		else
			sp->spValue = StrAlloc (value);
		sp->spNext = spList;
		spList = sp;
	}
	(void) fclose (f);
	return (1);
}


SpChar *
LookupSpecial (name)
char	*name;
{
SpChar	*sp;

	for (sp = spList; sp != (SpChar *) NULL; sp = sp->spNext)
	{
		if (strcmp (name, sp->spName) == 0)
			break;
	}
	return (sp);		/* NULL if not found */
}
