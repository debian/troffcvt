# PC character set special characters for troffcvt -> RTF writer

# field	meaning
# -----	-------
# 1	special character name
# 2	rtf string to write for character

# Special character names should match those used in the action
# file used with troffcvt to generate the input to the RTF writer.

# If the name is followed by "/F" then font F will be used to write
# the character.

# If string is explicitly empty ("") nothing will be written.  If
# a special character definition is missing completely
# the character name itself will be written in the form "[[name]]".
# (This is also true if the definition is missing for a particular
# character set and output is being written for that set.)  This
# way the character won't be dropped if you forget to define it here.

# backslashes should be doubled if you want them to appear in the
# output.  Don't forget to quote if you want to include whitespace.
# Don't forget a trailing space if a control word needs one.

# This table is woefully incomplete

# name			\ansi

backslash		\\\\
at			@

# these could be written portably as \lquote, \rquote, \ldblquote,
# \rdblquote if certain readers (e.g., WordPerfect) understood those.

quoteleft		\`
quoteright		\'
quotedblleft		\"
quotedblright		\"

zerospace		""

sixthspace		""
twelfthspace		""
# not really correct
digitspace		" "
hardspace		\\~
# backspace
emdash			-
minus			-

# don't really know what to do with these yet...
fieldbegin		""
fieldend		"\\tab "
fieldpad		""

leader			"\\tab "
tab			"\\tab "

# want this to disappear
opthyphen		""

# bullet
# square
# ru
# onequarter
# onehalf
# threequarters
fi		fi
fl		fl
ff		ff
ffi		ffi
ffl		ffl
# degree
# dagger
# minute
# cent
registered		"(reg.)"
copyright		"(c)"
plusmath		+
minusmath		-
equalmath		=
asteriskmath		*
# section
acute		\'
grave		\`
ul		_
slash			/
# radical
# radicalex
greaterequal		>=
lessequal		<=
# equivalence
# approxequal
# similar
# notequal
# arrowright
# arrowleft
# arrowup
# arrowdown
multiply/S		"\\'b4"
divide/S		"/"
plusminus		"+/-"
# union
# intersection
# propersubset
# propersuperset
# reflexsubset
# reflexsuperset
# infinity
# partialdiff
# gradient
# logicalnot
# integral
# proportional
# emptyset
# element
# br
# daggerdbl
# handright
# handleft
# bell
# bar
# circle
# bracelefttp
# braceleftbt
# bracerighttp
# bracerightbt
# braceleftmid
# bracerightmid
# bv
# bracketleftbt
# bracketrightbt
# bracketlefttp
# bracketrighttp

alpha/S			a
beta/S			b
gamma/S			g
delta/S			d
epsilon/S		e
zeta/S			z
eta/S			y
theta/S			h
iota/S			i
kappa/S			k
lambda/S		l
mu/S			m
nu/S			n
xi/S			c
omicron/S		o
pi/S			p
rho/S			r
sigma/S			s
sigma1/S		V
tau/S			t
upsilon/S		u
phi/S			f
chi/S			x
psi/S			q
omega/S			w
Alpha/S			A
Beta/S			B
Gamma/S			G
Delta/S			D
Epsilon/S		E
Zeta/S			Z
Eta/S			Y
Theta/S			H
Iota/S			I
Kappa/S			K
Lambda/S		L
Mu/S			M
Nu/S			N
Xi/S			C
Omicron/S		O
Pi/S			P
Rho/S			R
Sigma/S			S
Tau/S			T
Upsilon/S		U
Phi/S			F
Chi/S			X
Psi/S			Q
Omega/S			W

AE			AE
ae			ae
angleleft		<
angleright		>
arrowboth		<->
arrowdblboth		<=>
arrowdblleft		<=
arrowdblright		=>
arrowhorizex		--
arrowleft		<-
arrowright		->
asciicircum		^
asciitilde		~
bar			|
br			|
bv			|
braceleft		{
braceright		}
bracketleft		[
bracketright		]
circumflex		^
dollar			$
endash			-
hyphen			-
minute			'
numbersign		#
oe			oe
quotesingle		'
ru			_
second			''
tilde			~
trademark		(tm)
