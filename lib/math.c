/*
	math.c - stuff to compensate for silliness about where math
	routines are declared.
*/

# include	<stdio.h>
# if defined(__STDC_) || defined(THINK_C)	/* for atoi()/atol()/atof() */
# include	<stdlib.h>
# else
# include	<math.h>
# endif

# include	"troffcvt.h"


double Atof (s)
char	*s;
{
	return (atof (s));
}


long Atol (s)
char	*s;
{
	return (atol (s));
}


int Atoi (s)
char	*s;
{
	return (atoi (s));
}
