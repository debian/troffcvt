/*
 * Open a library file.
 */

#include	<stdio.h>

#include	"tcgen.h"


static FILE	*(*libFileOpen) () = NULL;



void
TCRSetOpenLibFileProc (FILE *(*proc) ())
{
	libFileOpen = proc;
}


FILE *
TCROpenLibFile (char *file, char *mode)
{
	if (libFileOpen == NULL)
		return ((FILE *) NULL);
	return ((*libFileOpen) (file, mode));
}
