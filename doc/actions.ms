.\" this document requires the tmac.wrprc macros
.\"
.\" $(TROFF) $(MSMACROS) tmac.wrprc thisfile
.\"
.\" revision date - change whenever this file is edited
.ds RD 11 April 1997
.\"
.EH 'troffcvt Action Reference'- % -''
.OH ''- % -'troffcvt Action Reference'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.de St	\" troffcvt special text
\\&\\$3\fB@\\$1\fR\\$2
..
.de Cl	\" troffcvt control
\\&\\$3\fB\e\\$1\fR\\$2
..
.de Rq	\" troff request
\\&\\$3\fB\.\\$1\fR\\$2
..
.de Es	\" troff escape
\\&\\$3\fB\e\\$1\fR\\$2
..
.de Ac	\" action
.LP
.nf
.ta \n(LLuR
\\$1	(\fB\\$2\fR)
.br
.fi
..
.TL
.ps +2
troffcvt Action Reference
.ps
.AU
Paul DuBois
.H*ahref mailto:dubois@primate.wisc.edu
dubois@primate.wisc.edu
.H*aend
.AI
.H*ahref http://www.primate.wisc.edu/
Wisconsin Regional Primate Research Center
.H*aend
Revision date:\0\0\*(RD
.\"
.H*toc*title "Table of Contents"
.\"
.H*aname introduction
.H*aend
.\"
.Ah Introduction
.\"
.LP
When
.I troffcvt
executes, it reads one or more action files which instruct it how to interpret
.I troff
requests.
The default action file is named
.I actions ;
this file
is read before any other action files or any macro packages named on the
command line.
You should understand the format of action files if you want to modify the
default action file or if you want to write your own action files.
.LP
This document describes the syntax of action files,
and discusses how actions work.
This includes argument parsing and transmission of argument values.
It also includes a list of the available actions.
.\"
.H*aname general-syntax
.H*aend
.Ah "General Syntax"
.\"
.LP
Action files are plain text files.
Blank lines are ignored, as are lines beginning with ``#'' in column 1, which
are taken as comments.
Other lines should begin with
.B imm
or
.B req
and have the following syntax:
.Ps
imm \f(CIimmediate-actions\fP
req \f(CIrequest-name parsing-actions\fP eol \f(CIpost-parsing-actions\fP
.Pe
.B imm
and
.B req
lines may be continued onto the next line with a ``\e'' as the last
character.
.LP
An
.B imm
line specifies actions to be executed as soon as they are read from an
action file.
(No output can be generated from
.B imm
lines because
.I troffcvt
turns off output while action files are being read.)
.LP
A
.B req
line describes what
.I troffcvt
should do when a given request occurs in the input.
.I request-name
is the name of the request (without the leading period).
.I parsing-actions
is either empty or a list of actions to perform to parse the request arguments.
.B eol
is mandatory and causes
.I troffcvt
to skip to the end of the request line.
.I post-parsing-actions
is either empty of a list of actions to perform after the request arguments
have been parsed; typically these actions process the arguments parsed by
the parsing arguments.
.LP
To include whitespace in an argument that is passed to an action, you
can surround it with either single or double quote characters.
For example:
.Ps
push-string ".tm this is an argument with whitespace\en"
push-string '.tm this is an argument with whitespace\en'
.Pe
To include a quote within such an argument, you must quote it with
the other quote character.
For example:
.Ps
push-string ".tm this is a single-quote: '\en"
push-string '.tm this is a double-quote: "\en'
.Pe
.\"
.Ah "Argument Processing"
.\"
.Bh "Argument Collection"
.\"
.LP
Several actions are available to parse various kinds
of request line arguments,
e.g., numbers, macro arguments, single characters.
Most parsing routines read arguments from the request line, store them
in the request argument vector
.Cw rargv[] ,
and bump the request argument count
.Cw rargc .
Arguments stored this way are available to other actions,
which refer to them as
.B $1 ,
.B $2 ,
etc.
.LP
A few parsing routines do not store the arguments in
.Cw rargv[]
but instead act on them directly.
For example,
.B parse-tab-stops
reads the tab values and sets the current stops directly.
.B parse-condition
reads and tests the condition, then executes or skips the rest of
the conditional request line.
.Bh "Argument Transmission \*- General"
.LP
Before an argument is passed to an action, it is examined to see whether it
contains references to request arguments or to escape sequences.
The legal references and sequences are shown below along with their meanings:
.Ps
.ta .75i
$\f(CIn\fP	\fRBecomes the \f(CIn\fR-th request argument, empty if unavailable\f(CW
$$	\fRBecomes the number of arguments parsed from request line\f(CW
$*	\fRBecomes a string consisting of all arguments, space-separated\f(CW
$@	\fRLike \f(CW$*\fR, but arguments are double-quoted as well\f(CW
\e\e	\fRBecomes \f(CW\e
\en	\fRBecomes a linefeed\f(CW
\et	\fRBecomes a tab\f(CW
\e\f(CIX\fP	\fRBecomes \f(CIX\fR, for any character not listed above\f(CW
.Pe
An empty string is substituted for a request argument if
.I n
is not a digit, if there is no such argument, if the argument was empty, or
if the action occurs in an immediate action list.
Note that
.B $*
and
.B $@
produce a
.I single
string, and thus count as a single argument when passed to an action.
.LP
Quotes may be used around an action argument to include whitespace in the
argument.
However, as noted in the
.H*ahref general-syntax
"General Syntax"
.H*aend
section, to include a single quote within an argument, you must quote the
argument with double quotes, and vice-versa.
.LP
.B Example:
The following has defines a string as the second argument
if the first argument isn't empty:
.LP
.Ps
push-string '.if !"$1"" .ds $1 "$2\en'
.Pe
.\"
.Bh "Argument Transmission \*- Numeric Arguments"
.\"
.LP
.LP
The
.B parse-num
action is used to parse a number from the request line.
It takes one argument, a scaling indicator used to scale numbers
in request argument expressions that don't have explicit scaling indicators.
For example, the
.Rq sp
request can be specified like this:
.LP
.Ps
req sp parse-num v eol break space $1
.Pe
.LP
In this case
.B parse-num
uses
.B v 's
as the default scaling.
Then the arguments to the requests
.LP
.Ps
\&.sp 1
\&.sp 1i
\&.sp 1i+1
.Pe
.LP
will be interpreted as ``1v'', ``1i'' and ``1i+1v'', respectively.
(The ``i'', where present, overrides the default of
.B v ).
.LP
In each case, after
.B parse-num
finishes evaluating the expression, the result is converted into basic
units, and that is what is stored in the request argument vector.
If the resolution is 432 units/inch and a
.B v
is 72 units, the arguments for the three requests above would be stored
as the strings ``72'', ``432'' and ``504''.
In other words, the value stored may not look much like the value on
the request line.
.LP
Allowable scaling indicators are
.B i
(inches),
.B c
(centimeters),
.B P
(picas),
.B m
(ems),
.B n
(ens),
.B p
(points),
.B v
(vertical spacing units),
.B u
(basic units) and
.B x
(ignore scaling).
.LP
The
.B x
specifier is special; it means there is no default, numbers are interpreted
without scaling, and the result stored in the request argument vector is
.I not
converted to basic units.
This is appropriate for requests such as
.Rq ce
or
.Rq ls ,
which may be specified like so:
.LP
.Ps
req ce parse-num x eol center $1
req ls parse-num x eol line-spacing $1
.Pe
.LP
If a request parsing action attempts to parse an argument that
is not present on the request line or that does not conform to what
the action is looking for,
the request-parsing action stores an empty string in the
.Cw rargv[]
array.
Functions for actions that allow arguments to be missing (empty) must be
written to test for that case.
.LP
Actions that take numeric parameters usually expect
that the number be given in basic units, unless an explicit scaling
indicator is appended.
The reason for this is that numeric arguments to actions are generally
obtained from parsing actions
earlier in the action list, and those
will usually have been converted to basic units.
This means that even for actions used in
.B imm
lines, the same convention must be followed.
Thus, the two lines following have very different results:
.LP
.Ps
.ta 2i
imm space 12	\fRSet spacing to 12 units\fP
imm space 12p	\fRSet spacing to 12 points\fP
.Pe
.Bh "Argument Transmission \*- Relative-Change Requests"
.LP
Some requests allow parameter values to be changed either to an
absolute value, or relatively by a specified amount.
Examples:
.Ps
.ta 1i
\&.ps 3	\fRSet point size to 3 points\fP
\&.ps +3	\fR... to 3 points greater than current value\fP
\&.nr x 4	\fRDefine number register with initial value 4\fP
\&.nr x +4	\fR...with initial value 4 greater than current value\fP
.Pe
This kind of request is handled with the
.B parse-absrel-num
action.
.B parse-absrel-num
is like
.B parse-num
in that it takes a default scaling indicator, but it also takes a second
argument which specifies the current value of the relevant parameter.
Since you can't specify the current value literally in an action file
(the current value is a volatile value),
you can refer to the parameter symbolically.
The parameters which may be referred to this way are
.B point-size ,
.B spacing ,
.B line-spacing ,
.B indent ,
.B line-length ,
.B page-length ,
.B title-length
and
.B offset .
.LP
.B Example:
the current indent may be set absolutely or relative to the current
value, so it may be specified like this:
.LP
.Ps
req in parse-absrel-num m indent eol break indent $1
.Pe
.LP
If the argument on an
.Rq in
request begins with a plus or minus sign,
the sign is stripped, the rest of the argument is evaluated
to produce a number, and the result is the current indent modified up
or down by that number.
(The result does
.I not
modify the current indent; it is simply stored in
.Cw rargv[] .)
If there is no initial sign, the request argument is an absolute number
and the parse result is simply the value of the argument (the current
indent value is ignored).
In the example above, the result goes into
.B $1 .
The second reference to
.B indent
is as an action, which is what actually sets the indentation (to the value of
.B $1 ).
.LP
As a special-case hack, if the current-value argument to
.B parse-absrel-num
begins with a backslash, the rest of the argument is taken as
the name of a number register, and the value of the register
is used.
This is used in relation to the
.Rq nr
request, which allows a register to be redefined relative to
its current value.
.Rq nr
can be specified in an action file as:
.LP
.Ps
req nr parse-name parse-absrel-num u \e\e$1 parse-num x eol define-register $1 $2 $3
.Pe
.LP
It is necessary that
.B parse-num
and
.B parse-absrel-num
both exist,
because an expression such as ``\-10+10'' means ``0'' as an absolute number,
but ``decrease by 20'' as a relative number.
.\"
.Ah "Action Descriptions"
.\"
.LP
Each action is discussed below.
Action descriptions are accompanied by one or more of the following
indicators to specify the allowable contexts in which the action can be
used:
.Ps
.ta 1i
imm	\fRAllowable in \fPimm\fP lines\fP
p	\fRAllowable in argument parsing section of \fPreq\fP lines\fP
pp	\fRAllowable in post-parsing section of \fPreq\fP lines\fP
.Pe
Whenever you specify an action in an action list,
you must specify all the arguments that it expects.
However, an argument often can be the empty string.
Thus
.B escape-char
.B @
sets the escape character to ``@'' while
.B escape-char
\fB""\fP
restores the default escape character.
.\"
.Ac "\fBabort\fR \fIstring\fR" "pp, imm"
.IP
Aborts
.I troffcvt
after printing
.I string
on
.I stderr .
If
.I string
is empty, ``User abort'' is printed instead.
.\"
.Ac "\fBadjust\fR \fIc\fR" "pp, imm"
.IP
Turns on output line adjusting, with mode
.I c ,
which can be
.B l
(left),
.B c
(center),
.B r
(right),
.B b
or
.B n
(both),
or a number previously obtained from the
.B \.j
register.
An adjustment indicator is written to the output if the request changes
the current adjustment and centering is off and filling is on.
If
.I c
is empty, adjustment is turned on with whatever the current mode is.
.\"
.Ac "\fBalias-macro\fR \fIxx yy\fR" "pp, imm"
.IP
Create the name
.B xx
as an alias for macro, string, or request
.B yy .
This implements the
.I groff
.Rq als
request.
The two names become equivalent.
.Rq xx
continues to exist even if
.Rq yy
is removed.
If
.Rq yy
is redefined,
.Rq xx
becomes redefined as well.
.IP
Actually, there is one difference between aliased names, which is that
.B \e$0
in the macro refers to the actual name under which the macro
was invoked, not the name under which it was first created.
.\"
.Ac "\fBalias-register\fR \fIxx yy\fR" "pp, imm"
.IP
Like
.B alias-macro
but for number registers.
This implements the
.I groff
.Rq aln
request.
.\"
.Ac "\fBappend-macro\fR \fIxx yy\fR" pp
.IP
Like
.B define-macro
but appends to an existing macro definition.
.\"
.Ac "\fBappend-string\fR \fIxx value\fR" "pp, imm"
.IP
Like
.B define-string
but appends to an existing string.
.\"
.Ac "\fBbegin-page\fR \fIN\fR" "pp, imm"
.IP
If
.I N
is empty, writes
.Cl begin-page .
Otherwise, writes
.Cl begin-page
.I N
and sets the current page number and register
.B %
to
.I N .
.\"
.Ac "\fBbreak\fR" "pp, imm"
.IP
Causes a break (writes a
.Cl break
line) unless the no-break control character was given
on the request or unless the current output line doesn't have anything on it.
None of the action functions do a break except that for
.B break
itself, so it
should be specified in the action list for any request that normally
causes a break.
The reason for this is to make sure complete control over when a break occurs
is in the action file, not inside
.I troffcvt .
This is more work for the action file writer, but provides more
flexibility.
.IP
For instance,
.Rq sp
can be specified as ``req sp parse-num v eol break space $1''.
The function associated with
.B space
doesn't do any break itself, so the
.B break
is necessary.
.\"
.Ac "\fBcenter\fR \fIN\fR" "pp, imm"
.IP
Turns on centering and writes
.Cl center
to the output.
.I troffcvt
will generate additional control output
to turn off centering after
.I N
lines of text input have been read (1 line if
.I N
is empty).
.\"
.Ac "\fBconstant-width\fR \fIF N M\fR" "pp, imm"
.IP
Treat font
.I F
as a constant-width font.
See the
.I troff
manual for the interpretation of
.I N
and
.I M .
.\"
.Ac "\fBcontinuous-underline\fR \fIN\fR" "pp, imm"
.IP
Turns on continuous underlining and writes
.Cl cunderline
to the output.
Continuous underlining is turned off after
.I N
lines
of text input
(1 line if
.I N
is empty).
.\"
.Ac "\fBdebug-flag\fR \fIN\fR" "p, pp, imm"
.IP
Turns on debugging flag bit
.I N .
The allowable values are listed in
.I troffcvt.h
and are not of general interest.
If
.I N
is empty, all flags are turned on.
If
.I N
is negative, all flags are turned off.
.\"
.Ac "\fBdefine-macro\fR \fIxx yy\fR" pp
.IP
Defines a macro named
.I xx .
Reads input in copy mode until a line of the form \fB\.\fIyy\fR is found,
at which point \fB\.\fIyy\fR is invoked.
If
.I yy
is empty, a line of the form
.B \.\.
terminates the definition.
.\"
.Ac "\fBdefine-register\fR \fIxx init incr\fR" "pp, imm"
.IP
Defines number register
.I xx
with initial value
.I init
and increment value
.I incr .
.\"
.Ac "\fBdefine-string\fR \fIxx value\fR" "pp, imm"
.IP
Defines string
.I xx
with value
.I value .
.\"
.Ac "\fBdiversion-append\fR \fIxx\fR" pp
.IP
Like
.B diversion-begin
but writes
.Cl diversion-append
.I xx
to the output instead.
.\"
.Ac "\fBdiversion-begin\fR \fIxx\fR" pp
.IP
Writes
.Cl diversion-begin
.I xx
to the output.
When a matching
.Rq di
or
.Rq da
is seen,
.Cl diversion-end
.I xx
is written.
.\"
.Ac "\fBdump-bad-requests\fR \fIN\fR" "pp, imm"
.IP
If
.I N
is non-zero, any unrecognized requests are written to the output
in the form:
.Ps
\eother bad-req: \f(CIoriginal input line\fP
.Pe
If
.I N
is zero, this kind of output is suppressed.
.\"
.Ac "\fBdump-input-stack\fR" "pp, imm"
.IP
Dump information about the input stack to
.I stderr .
This is for debugging.
.\"
.Ac "\fBdump-macro\fR \fIxx\fR" "p, pp, imm"
.IP
Dumps the body of macro
.I xx
to
.I stderr .
This is for debugging.
.\"
.Ac "\fBecho\fR \fIstring\fR" "p, pp, imm"
.IP
Writes
.I string
to
.I stderr .
.\"
.Ac "\fBembolden S \fIF N\fR" "pp, imm"
.IP
This request takes three arguments, but the arguments can actually be
.I F
and
.I N
rather than
.B S ,
.I F ,
and
.I N .
It's necessary to examine the first argument to tell.
.IP
Emboldens font
.I F
by smearing it
.I N
units.
If
.I S
is present, embolden the special font by
.I N
units when the current font is
.I F .
.\"
.Ac "\fBend-input\fR" pp
.IP
Forces input routines to return end-of-input to cause
.I troffcvt
to finish processing immediately.
.\"
.Ac "\fBend-macro\fR \fIxx\fR" "pp, imm"
.IP
Causes macro
.I xx
to be invoked after all other input has been processed.
.I xx
must be a macro, not a request.
.\"
.Ac "\fBenvironment\fR \fIname\fR" "pp, imm"
.IP
Enters environment
.I name
or resume previous environment if
the argument is empty.
.\"
.Ac "\fBeol\fR" "p"
.IP
Skips to the end of the current input line.
This action must terminate the parsing action section of all
.B req
lines.
.\"
.Ac "\fBfill\fR" "pp, imm"
.IP
Turns on fill mode.
.\"
.Ac "\fBfont\fR \fIF\fR" "pp, imm"
.IP
Switches to font
.I F ,
writing
.Cl font
.I F
to the output if this causes a font change.
.\"
.Ac "\fBfont-position\fR \fIN F\fR" "pp, imm"
.IP
Associates font
.I F
with position
.I N .
.\"
.Ac "\fBflush\fP" "pp, imm"
.IP
Flush any pending output.
Similar to
.B break ,
but doesn't write
.Cl break .
.\"
.Ac "\fBhyphenate\fR \fImode\fR" "pp, imm"
.IP
Sets the hyphenation mode, which should be zero to turn off hyphenation,
or a positive value as described in the
.I troff
manual.
.\"
.Ac "\fBhyphen-char\fR \fIc\fR" "pp, imm"
.IP
Sets the hyphenation character to
.I c .
.\"
.Ac "\fBignore\fR \fIyy\fR" "pp, imm"
.IP
Input is read in copy mode but otherwise ignored until a line of the form
\fB\&\.\fIyy\fR is found, at which time
.Rq \fIyy\fR
is invoked.
If
.I yy
is empty, a line of the form
.B \.\.
terminates the
.B ignore
action.
.\"
.Ac "\fBindent\fR \fIN\fR" "pp, imm"
.IP
Sets indent to
.I N .
.\"
.Ac "\fBinput-trap\fR \fIN xx\fR" "pp"
.IP
Sets the input-line trap to
.I xx ,
to spring after
.I N
input lines.
.I xx
must be a macro, not a request.
.\"
.Ac "\fBline-length\fR \fIN\fR" "pp, imm"
.IP
Sets line length to
.I N .
.\"
.Ac "\fBline-spacing\fR \fIN\fR" "pp, imm"
.IP
Sets line spacing to
.I N .
.\"
.Ac "\fBneed\fR \fIN\fR" "pp, imm"
.IP
Indicate that
.I N
lines are needed on the current output page.
If not that many lines are available, postprocessors that pay attention
to this information typically will perform a page break.
.\"
.Ac "\fBno-space\fR \fIc\fR" "pp, imm"
.IP
Turns on no-space mode if the argument is
.B y ,
off if it's
.B n .
.\"
.Ac "\fBnoadjust\fR" "pp, imm"
.IP
Turns off output line adjusting.
.\"
.Ac "\fBnoescape\fR" "pp, imm"
.IP
Turns off escape processing.
.\"
.Ac "\fBnofill\fR" "pp, imm"
.IP
Turns on no-fill mode.
.\"
.Ac "\fBoffset\fR \fIN\fR" "pp, imm"
.IP
Sets offset to
.I N .
.\"
.Ac "\fBoutput-control\fR \fIstring\fR" "pp, imm"
.IP
Writes
.I string
directly to the output, preceded by a
.B \e
character and followed by a newline.
.\"
.Ac "\fBoutput-special\fR \fIstring\fR" "pp, imm"
.IP
Writes
.I string
directly to the output, preceded by a
.B @
character and followed by a newline.
.\"
.Ac "\fBoutput-text\fR \fIstring\fR" "pp, imm"
.IP
Writes
.I string
directly to the output.
.\"
.Ac "\fBpage-length\fR \fIN\fR" "pp, imm"
.IP
Sets page length to
.I N .
.\"
.Ac "\fBpage-num-char\fR \fIc\fR" "pp, imm"
.IP
Sets the page number character (used in titles) to
.I c .
.\"
.Ac "\fBpage-number\fR \fIN\fR" "pp, imm"
.IP
Sets the current page number and the
.B %
register to
.I N .
Writes no output.
.\"
.Ac "\fBparse-absrel-num\fR \fIc N\fR" p
.IP
Parses a number or numeric expression from the request line, which may
be absolute, or relative
to
.I N .
.I c
is the default scaling unit to be used in evaluating expressions.
.\"
.Ac "\fBparse-char\fR" p
.IP
Parses a single-character argument from the request line.
.\"
.Ac "\fBparse-condition\fR \fIc\fR" p
.IP
Parses a condition from the request line, tests it, and executes or skips
the rest of the line depending on the result of the test.
.I c
should be
.B y
if a following else-clause should be expected later (as for
.Rq ie )
or
.B n
if it should not be (as for
.Rq if ).
.\"
.Ac "\fBparse-embolden\fR" p
.IP
An ugly hack used for the
.Rq bd
request. 
Parses both forms of the request, which are:
.Ps
\&.bd \f(CIF N\fP
\&.bd S \f(CIF N\fP
.Pe
The resulting argument vector will have either two or three elements.
.\"
.Ac "\fBparse-filename\fR" p
.IP
Parses a filename from the request line.
.\"
.Ac "\fBparse-macro-args\fR" p
.IP
Parses all remaining arguments (in copy mode) to the
end of the request line.
Arguments on request lines
are separated by spaces, or may be double-quoted to include
whitespace in an argument.
Note that a numeric argument is not evaluated in the same manner
as for the number-parsing actions.
To get around this, you can later interpolate the argument into a
string that's processed with
.B push-string:
.Ps
# assume first argument, if present, is numeric expression to set indent.
req XX parse-macro-args eol push-string ".if $$>0 .in $1\en"
.Pe
.\"
.Ac "\fBparse-name\fR" p
.IP
Parses a name of the form
from the request line.
This action is used to pick up single string, macro, register, etc. names.
If compatibility mode is on, the name can be no more than two characters long.
.\"
.Ac "\fBparse-names\fR" p
.IP
Like
.B parse-name ,
but parses multiple names to the end of the input line.
.\"
.Ac "\fBparse-num\fR \fIc\fR" p
.IP
Parses a number or numeric expression from the request line.
.I c
is the default scaling unit to be used in evaluating expressions.
.\"
.Ac "\fBparse-string-value\fR \fIc\fR" p
.IP
Parses a string in copy mode to the end of the input
line.
.I c
should be
.B y
if a leading double quote should be stripped off (as for
.Rq ds )
or
.B n
if it should not be (as for
.Rq ab
and
.Rq tm ).
.\"
.Ac "\fBparse-tab-stops\fR" p
.IP
Parses the rest of the request line for tab stop settings.
If there are none,
.Cl reset-tabs
is written, which should be taken as meaning reset tab stops to ``every
half inch''.
(The
.I troff
manual doesn't specify how many stops are allowed; presumably postprocessors
should allow for ``several''.)
.IP
If explicit stops are given, the first one is written out as
.Cl first-tab
.I N
.I c ,
and any following are written as
.Cl next-tab
.I N
.I c .
When
.Cl first-tab
is seen, the postprocessor should clear any stops and begin collecting
new ones.
Each
.Cl next-tab
setting should be added to the current list.
.I N
is specified in basic units.
.I c
is
.B l
(left),
.B c
(center) or
.B r
(right).
.\"
.Ac "\fBparse-title\fR" p
.IP
Parses a title of the form \fB\'\fIleft\fB\'\fIcenter\fB\'\fIright\fB\'\fR
from the request line into three elements of
.Cw rargv[] .
Missing title elements become empty arguments in
.Cw rargv[] .
.\"
.Ac "\fBparse-transliteration\fR" p
.IP
Parses a transliteration list from the request line.
.\"
.Ac "\fBpoint-size\fR \fIN\fR" "pp, imm"
.IP
Sets the point size to
.I N .
.I N
is number of points (unscaled), not basic units.
.\"
.Ac "\fBprocess-condition\fR" p
.IP
Executes or skips conditional input.
This is used for the
.Rq el
request, and tests whether the preceding matching
.Rq ie
succeeded or failed.
.\"
.Ac "\fBprocess-do\fR" p
.IP
This action implements the
.I groff
.Rq do
request.
Parses the rest of the input line with compatibility mode disabled,
then executes the line.
The rest of the line must contain a request, but not begin with a control
character.
The control character that begins the
.B do
line (\fB.\fR or \fB\'\fP) is used to begin the request.
.\"
.Ac "\fBpush-file\fR \fIfilename\fR" pp
.IP
Pushes the given file onto the input stack.
.\"
.Ac "\fBpush-macro-file\fR \fIfilename\fR" pp
.IP
Like
.B push-file ,
but looks for the file in the same directories used to find macro
files.
This implements the
.I groff
.Rq mso
request.
.\"
.Ac "\fBpush-string\fR \fIstring\fR" "pp, imm"
.IP
Pushes the given string onto the input stack and processes it.
To push a named string onto the stack, use one of the following
forms:
.Ps
\epush-string "\e*\f(CIx\fP"
\epush-string "\e*(\f(CIxx\fP"
\epush-string "\e*[\f(CIxxx\fP]"
.Pe
To push a request or macro onto the stack, use:
.Ps
\epush-string ".\f(CIxx\fP\en"
.Pe
It can be dangerous to use
.B push-string
in immediate mode, since your execution environment may not yet be
fully set up.
That is, although you can process a string immediately using an
.B imm
line, you should make sure that any requests or macros named in the string
have been defined, or they'll be ignored.
.IP
.\"
.Ac "\fBregister-format\fR \fIxx format\fR" "pp, imm"
.IP
Assigns the given format to number register
.I xx .
.\"
.Ac "\fBremove-name\fR \fIc name\fR" "pp, imm"
.IP
Removes a name.
.I c
is
.B y
if the name refers to a number register,
.B n
if the name
refers to a request, string, or macro.
.\"
.Ac "\fBremove-names\fR \fIc name1 name2 name3 name4 name5 name6 name7 name8 name9\fR" "pp, imm"
.IP
Multiple-name form of
.B remove-name .
There must be nine name arguments specified, but
arguments may be given as empty strings if you're not removing
nine names.
.\"
.Ac "\fBrename\fR \fIxx yy\fR" "pp, imm"
.IP
Renames request, macro, or string
.I xx
to
.I yy .
.\"
.Ac "\fBset-compatibility\fR \fIN\fR" "pp, imm"
.IP
Sets compatibility mode to
.I N ,
which turns the mode on if 
.I N
is non-zero.
When compatibility mode is off, recognition of
long request, macro, string, register, and font names is suppressed.
This action also sets the
.B .C
read-only register.
.\"
.Ac "\fBset-control\fR \fIc\fR" "pp, imm"
.IP
Sets control character to
.I c
.B \. "" (
if
.I c
is empty).
.\"
.Ac "\fBset-control2\fR \fIc\fR" "pp, imm"
.IP
Sets no-break control character to
.I c
.B \' "" (
if
.I c
is empty).
.\"
.Ac "\fBset-escape\fR \fIc\fR" "pp, imm"
.IP
Restores escape processing, setting escape character to
.I c
(\fB\e\fR if
.I c
is empty).
.I c
cannot be an escaped or
.Es ( \fIxx\fR
special character, since then it would be recursive.
.\"
.Ac "\fBset-field\fR \fIdelim pad\fR" "pp, imm"
.IP
Sets field delimiter character to
.I delim
and field padding character to
.I pad .
If
.I pad
is empty, the pad character becomes space.
If
.I delim
and
.I pad
are both empty, the field mechanism is turned off.
.\"
.Ac "\fBset-leader\fR \fIc\fR" "pp, imm"
.IP
Sets leader repetition character to
.I c
or remove it if
.I c
is empty.
.\"
.Ac "\fBset-tab\fR \fIc\fR" "pp, imm"
.IP
Sets tab repetition character to
.I c
or remove it if
.I c
is empty.
.\"
.Ac "\fBshift-args\fR \fIN\fR" "pp, imm"
.IP
In a macro, shifts the arguments left by
.I N
arguments.
If
.I N
is empty, the arguments are shifted left by one place.
If
.I N
is negative or zero, nothing is done.
If
.I N
is equal to or greater than the number of arguments the macro has,
all the arguments are shifted.
.\"
.Ac "\fBspace\fR \fIN\fR" "pp, imm"
.IP
Spaces vertically by
.I N
units (negative = upward).
.\"
.Ac "\fBspace-size\fR \fIN\fR" "pp, imm"
.IP
Sets space size to
.I N /36\fBm\fR.
.I N
is unscaled, not in basic units.
.\"
.Ac "\fBspacing\fR \fIN\fR" "pp, imm"
.IP
Sets vertical base-line spacing to
.I N
units.
1
.B v
then becomes
.I N
units.
.\"
.Ac "\fBspecial-char\fR \fIxx output\fR" "pp, imm"
.IP
Defines a special character with the given name and output sequence.
The name cannot be empty and must consist of plain ASCII characters.
The output sequence is the string that should be written when the
character is referenced.
It too cannot be empty and must consist of plain ASCII characters.
.B special-char
is used in
.B imm
lines in action files to define the set of special characters that
.I troffcvt
should recognize.
This set should reflect the characters known by local versions of
.I troff .
.\"
.Ac "\fBswitch-file\fR \fIname\fR" pp
.IP
Switches the current input file to
.I name .
.\"
.Ac "\fBtemp-indent\fR \fIN\fR" "pp, imm"
.IP
Sets the temporary indent to
.I N .
.\"
.Ac "\fBtitle\fR \fIleft\fR \fImiddle\fR \fIright\fR" "pp, imm"
.IP
Processes a three-part title.
.\"
.Ac "\fBtitle-length\fR \fIN\fR" "pp, imm"
.IP
Sets the title length to
.I N .
.\"
.Ac "\fBtransliterate\fR \fIlist\fR" "pp, imm"
.IP
.I list
should contain pairs of characters.
The first element of each pair will be transliterated into the second
element on output.
If the list has an odd number of characters, the last is transliterated
to a space.
.\"
.Ac "\fBunderline\fR \fIN\fR" "pp, imm"
.IP
Turns on underlining and writes
.Cl underline
to the output.
Underlining is turned off after
.I N
lines
of text input.
.I troffcvt
will generate additional control output
to turn off underlining after
.I N
lines of text input have been read (1 line if
.I N
is empty).
.\"
.Ac "\fBunderline-font\fR \fIF\fR" "pp, imm"
.IP
Sets the underline font to
.I F .
.\"
.Ah "Creating a New Action"
.\"
.LP
If the set of actions is insufficient for your
purposes, here's how to modify
.I troffcvt
to write a new one:
.Ls B
.Li
Read the rest of this section, then study some of the existing
actions in
.I troffcvt
source code to see how they are implemented.
.Li
Pick a name for the action.
This will be the name used in action files to refer to it.
.Li
Decide whether the function used to implement the action should take
any arguments, that is, whether the action should be followed by any
arguments when used in an action file.
You must also decide whether it should be allowed on
.B imm
lines and/or
.B req
lines (and if allowed on the latter, whether allowed in the post-parsing
or parsing sections, or both).
.Li
List the action in the
.Cw aiTab[]
array in
.I action.c ,
and add an extern for the function to
.I troffcvt.h .
.Li
Write the action function.
If the action parses arguments, the function goes in
.I parse.c ,
otherwise it goes in
.I req.c .
It should be declared like this:
.Ps
.ta .75i
int
ActionFunction (int argc, XChar **argv)
{
}
.Pe
The function should perform the action and return non-zero for success,
zero for failure.
Possibly it should simply panic if something catastrophic (unrecoverable)
happens.
Note that sometimes it is useful to return non-zero for certain kinds
of failures.
If an action in an action list returns zero, no actions following it in
an action list are executed.
If you want them to execute anyway, your function should always
return non-zero.
.sp .5v
.Cw argc
is the number of action arguments and
.Cw argv
is an array of pointers to them.
(There is a
.Cw NULL
pointer following the last argument, i.e.,
.Cw argv[argc]
is
.Cw NULL .)
.sp .5v
The function should make no direct reference to the request values
.Cw rargc
and
.Cw rargv[]
unless it is an argument parsing action.
If your action can be used on
.B imm
lines, 
there won't be any request arguments available.
It's also a good idea for the function to refrain from modifying the arguments
passed to it.
(Make copies first and modify the copies.)
.Le
