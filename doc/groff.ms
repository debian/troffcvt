.\" this document requires the tmac.wrprc macros
.\"
.\" $(TROFF) $(MSMACROS) tmac.wrprc thisfile
.\"
.\" revision date - change whenever this file is edited
.ds RD 9 April 1997
.\"
.EH 'troffcvt Support for groff'- % -''
.OH ''- % -'troffcvt Support for groff'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.de St	\" troffcvt special text
\\&\\$3\fB@\\$1\fR\\$2
..
.de Cl	\" troffcvt control
\\&\\$3\fB\e\\$1\fR\\$2
..
.de Rq	\" troff request
\\&\\$3\fB\.\\$1\fR\\$2
..
.de Es	\" troff escape
\\&\\$3\fB\e\\$1\fR\\$2
..
.TL
.ps +2
troffcvt Support for groff
.ps
.AU
Paul DuBois
.H*ahref mailto:dubois@primate.wisc.edu
dubois@primate.wisc.edu
.H*aend
.AI
.H*ahref http://www.primate.wisc.edu/
Wisconsin Regional Primate Research Center
.H*aend
Revision date:\0\0\*(RD
.\"
.LP
.I troffcvt
supports the following
.I groff
extensions to standard
.I troff :
.Ls B
.Li
Long names.
Contexts in which long names are legal include:
.Ps
.ta 1.5i
\&.xxx	\fRLong request and macro names\fP
\e[xxx]	\fRLong special character names\fP
\e*[xxx]	\fRLong string names\fP
\en[xxx]	\fRLong register names\fP
\ef[xxx]	\fRLong font names\fP
\e$[nnn]	\fRLong (>9) macro argument references\fP
.Pe 0
.Li
Compatibility mode.
The
.Rq cp
request and
.B \&.C
register are supported.
Compatibility may be turned on from the
.I troffcvt
command line with the
.B \-C
option.
.Li
The
.Rq mso
request.
.Li
The
.Rq do
request.
.Li
The
.B \e$*
and
.B \e$@
macro argument references.
.Li
The
.B \e$0
macro name reference.
.Li
More than 9 macro arguments may be referenced (up to 99, though
.I groff
allows more).
For macros above
.B \e$9 ,
the notation \fB\e[\fInnn\fB]\fR may be used.
.Li
Aliases (the
.Rq als
and
.Rq aln
requests).
.Li
The
.Rq shift
request.
.Li
Named environments and the
.B \&.ev
string-valued register.
.Li
The
.Es A '\fIxx\fR'
escape sequence.
.Li
The
.Rq if
.B d
conditional test.
.Li
The
.Rq if
.B r
conditional test.
.Li
The
.Rq if
.B c
conditional test.
.Le
