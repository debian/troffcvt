.\" this document requires the tmac.wrprc macros
.\"
.\" $(TROFF) $(MSMACROS) tmac.wrprc thisfile
.\"
.\" revision date - change whenever this file is edited
.ds RD 7 March 1997
.\"
.EH 'troffcvt Notes and Bugs'- % -''
.OH ''- % -'troffcvt Notes and Bugs'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.de St	\" troffcvt special text
\\&\\$3\fB@\\$1\fR\\$2
..
.de Cl	\" troffcvt control
\\&\\$3\fB\e\\$1\fR\\$2
..
.de Rq	\" troff request
\\&\\$3\fB\.\\$1\fR\\$2
..
.de Es	\" troff escape
\\&\\$3\fB\e\\$1\fR\\$2
..
.TL
.ps +2
troffcvt
.sp .3v
Notes, Bugs, Deficiencies
.ps
.AU
Paul DuBois
.H*ahref mailto:dubois@primate.wisc.edu
dubois@primate.wisc.edu
.H*aend
.AI
.H*ahref http://www.primate.wisc.edu/
Wisconsin Regional Primate Research Center
.H*aend
Revision date:\0\0\*(RD
.\"
.H*toc*title "Table of Contents"
.\"
.\"
.Ah "General Information"
.LP
This document contains miscellaneous observations about how
.I troffcvt
behaves, and tries to document its limitations.
It should be read by anyone trying to write a postprocessor for
.I troffcvt
output.
.LP
.I troffcvt
supports the full
.I troff
language, aside from some specific exceptions noted
below.
These are discussed in sections numbered in parallel with Ossanna's
.I troff
manual.
Many are related to insignificant or obscure features of the language
(e.g.,
.lg 0
.Rq fl ,
.lg
.Rq pm ).
Some are more significant (e.g., diversion mishandling).
In some sense these exceptions form the
.I troffcvt
bug list.
.LP
.I troffcvt
supports a limited subset of the
.I groff
extensions to standard
.I troff .
In general, you should assume that any particular
.I groff
extension is not supported by
.I troffcvt ,
but there are some important exceptions such as aliases and long names.
For more details, see the document
.H*ahref groff.html
.I "troffcvt Support for groff\c"
.H*aend
\&.
.LP
The most general and pervasive exception to standard
.I troff
processing is that
.I troffcvt
knows nothing about the characteristics of any output device;
in particular, it uses no font metric information.
This means it doesn't know how wide or tall any character is.
This exception is pervasive in that it affects handling of
a number of requests and other aspects of the language.
Some of the implications are:
.Ls B
.Li
.I troffcvt
can't compute the size of output.
This breaks constructions such as
.Es w "\'\fIstring\fR\',"
which makes
.I troffcvt
pretty well useless for files that have been passed through
.I tbl ,
especially if the tables are complex or require close layout tolerances.
(If your document contains tables, you may get more acceptable output by
preprocessing the document with
.I tblcvt
rather than with
.I tbl .)
.Li
.I troffcvt
has no notion of current position on the page.
.Li
Position-related traps aren't sprung, since it can't be determined when the
output position sweeps past the trap position.
In particular, page transition traps aren't sprung, so
.I troffcvt
doesn't know the current page number.
Multi-column output doesn't multi, either.
.Li
Registers that have values dependent on output position or size are
useless.
This includes the general registers
.B ct ,
.B dl ,
.B dn ,
.B ln ,
.B nl ,
.B sb ,
and
.B st ,
and the read-only registers
.B \.a ,
.B \.d ,
.B \.h ,
.B \.n ,
and
.B \.t .
In
.I troffcvt
the value of these is always zero.
The value of
.B \.w
is
.I always
1
.B en .
The value of the
.B % 
is unreliable as well, affecting requests such as
``\fB\.if \en%>1\fR \fIstuff\fR'' (fairly
common in macro packages).
.Li
Recursive macros may fail.
For instance, the
.B \-mm
macro
.Rq )r
does clever spacing and testing of the current page position, recursing
until it achieves the desired position.
In
.I troff ,
the recursion test eventually terminates properly, but in
.I troffcvt
it doesn't.
.I troffcvt
instead notes ``deep'' recursion and simply fails
once a certain recursion depth is reached.
.Le
Non-use of font metric information is deliberate;
it isn't a goal of
.I troffcvt
to lay out text on pages.
If it were,
.I ditroff
would be more useful.
The goal is to make it easier for
.I other
programs to lay out text, by producing input for those programs
that's more easily interpretable than straight
.I troff
input.
Along with this is the goal of producing input that's easily transformable
before it's fed into the final translator.
Example transformations include:
mapping
.Cl font
lines onto fonts available in the target format;
scaling character sizes up or down for easier reading or tighter packing,
by mapping
.Cl point-size
and
.Cl spacing
lines;
changing page layout, e.g., for production of legal or A4 size pages.
Tying
.I troffcvt
output to font metrics would make these sorts of transformations difficult.
.LP
The numbering of the sections that follow correponds to the section numbering
in the Ossanna
.I troff
manual, to make it easier to determine where
.I troffcvt
bugs affect requests listed in a given section of the Ossanna manual.
.\"
.Ah "1. General Explanation"
.\"
.Bh "1.2. Formatter and device resolution"
.LP
The default resolution used by
.I troffcvt
is 432 units/inch, but it may be changed with the
.B \-r
option.
(A good value might be the least common multiple of 72 and the resolution
you use in the target format.)
.LP
Since resolution is not fixed,
postprocessors should use the value specified on the
.Cl resolution
line that appears as the first line of the setup section.
It indicates number of basic units per inch.
Numeric values on following control lines that are specified in basic units
can be converted to other units as necessary using this resolution.
.\"
.Bh "1.3. Numerical parameter input"
.LP
The default scaling for unscaled numbers in
.I troff
requests is not hardwired into
.I troffcvt .
Instead, scaling is specified in the action file, although it's a good idea
to use the same default there that
.I troff
uses:
.Ps
.ta 4i
req sp parse-num v eol break space $1	\fRgood\fP
req sp parse-num i eol break space $1	\fRbad\fP
.Pe
Expressions that involve calculation of ``amount of motion to reach an
absolute position'' (as in, e.g., \fB|\^3.2c\fR) evaluate to zero.
Since the current position
is unknown, the distance to any other position cannot be determined.
This affects processing of
.I tbl
output particularly, since
.I tbl
is fond of using
.Es h \'|\fIN\fR\'
to line up columns.
.\"
.Ah "2. Font and Character Size Control"
.\"
.Bh "2.2. Fonts"
.LP
Fonts R, I and B are initially mounted on
positions 1, 2 and 3, respectively, and
the special font is mounted on all other positions.
This means fonts
.B R
and
.B 1 ,
.B I
and
.B 2 ,
etc., are considered equivalent.
If a different font is mounted on a given position, references to that font,
either by the name or number, are considered equivalent.
This is logical to me, although in fact it doesn't reflect the behavior of all
.I troff
versions.
For instance,
.I xroff
does not necessarily consider
.B R
and
.B 1
equivalent unless you mess
around with its font map.
.LP
The
.B \.f
register is set to the number of the current font, or zero if the
current font is not mounted
(it is allowable to refer to a font simply by naming it, so the current
font doesn't necessarily have any number).
.Rq "ft 0"
and
.Es f0
are taken as referring to this font.
.LP
Font changes are written by name (not number) in the form
.Cl font
.I name ,
where
.I name
must be interpreted by the postprocessor.
This is a difficult problem since font names tend to be site-specific
and idiosyncratic, although the standard
.I troffcvt
file reader provides some simple font handling support that might be useful.
.LP
Output from the
.Rq bd
request typically appears as the
.Cl embolden
and
.Cl embolden-special
control lines.
It's not clear whether it's worth it for postprocessors to support this request
postprocessors, particularly the special-font variant.
Although one can switch to the special font explicitly
.Rq "ft S" , (
.Es fS ),
characters from the special font are also logically part of the other default
fonts, and thus referenced for particular characters even if
.B S
is not the current font.
To fully support special font bolding, you'd need to keep track of all the
characters in the special font and check every output character to see if it
needs to come from that font.
Besides, this whole business of the relationship between the special font
and other fonts seems tightly linked to the particular typesetting machinery
used when
.I troff
was originally written.
.\"
.Bh "2.3. Character size"
.LP
Any positive character size is allowed.
For historical reasons,
embedded absolute size changes may be one or two digits up to a size of
36, i.e.,
.Es s36
is the same as
.Rq "ps 36"
while
.Es s37
is the same as
.Rq "ps 3"
followed by ``7''.
Non-numeric input following
.Es s
is interpreted the same way as
.Es s0 .
.LP
For the
.Rq cs
request, only the font name is written out on the
.Cl constant-width
line; the width in which the characters are to be written is currently
ignored.
.\"
.Bh "3. Page Control"
.LP
Since the current page
number cannot be reliably determined,
.Rq bp
and
.Rq pn
requests which specify a relative page number change are not reliable.
.LP
My
.I troff
.B "Summary and Index"
indicates that
.I V s
are the default scaling unit for
.Rq bp
and
.Rq po
requests.
The
.I actions
file supplied with the
.I troffcvt
distribution tells
.I troffcvt
to ignore scaling for
.Rq bp
and to use ems for
.Rq po ,
which seems to make more sense.
.LP
.Rq mk
and
.Rq rt
are not supported.
.\"
.Ah "4. Text Filling, Adjusting and Centering"
.LP
.I troff
tosses extra spaces at the end of text lines.
.I troffcvt
tries to do the same but gets confused by sequences such as
``abc\efI\0\0\0\efP''.
The trailing spaces are retained in the output, erroneously.
.\"
.Bh "4.1. Filling and adjusting"
.LP
Use of the
.B \.j
register as the argument to the
.Rq ad
request is allowed.
Note:
This depends on all the internal adjustment mode type values
being single-digit non-negative integers so that the argument can
be parsed by the
.B parse-char
action.
The internal codes are not necessarily the same as those used by any
particular version of
.I troff .
(The codes are known
.I not
to be the same as those assumed by
.I tbl ,
but I'm not sure exactly what
.I tbl
assumes.)
.LP
No hyphenating is done; that is left for the postprocessor.
The optional hyphenation character appears as
.St opthyphen
in the output.
.LP
The
.B \.n ,
.B nl
and
.B \.h
registers are not set.
.LP
.Es p
appears in the output as
.Cl break-spread .
.\"
.Bh "4.2. Text Interruption"
.LP
In all the CFA (center, fill, adjust) modes, text interruption
in the input
.Es c ) (
is processed such that the next text line
appears to be logically glued to the current one.
The resulting logical line counts as a single input line.
(Actually, this appears to be only sometimes true, e.g., for
.Rq ce ,
but not, evidently, for
.Rq ul
or
.Rq it .
Huh.)
.LP
Text interruption in the input
will not appear explicitly in the output and thus
is of no importance for postprocessors.
.Es c
is manifest
in
.I troffcvt
output merely as an absence of a leading space on the next text output line.
Example:
.Ps
.ta 1.5i
\f(CBInput 1	Input 2\fP
abc	abc\ec
def	def

.B "Output 1	Output 2"
abc	abc
\0def	def
.Pe
Postprocessors would write these out as ``abc def'' and ``abcdef'',
respectively.
.\"
.Ah "5. Vertical Spacing"
.\"
.Bh "5.2. Extra line-space"
.LP
The
.B \.a
register is not set.
.LP
.Rq sv ,
.Rq os ,
.Rq ns ,
.Rq rs
are not supported.
.\"
.Ah "7. Macros, Strings, Diversion, and Position Traps"
.LP
The
.I troff
manual doesn't say it, but
.Rq rm
allows multiple names to be specified for removal on a single request.
.I troffcvt
does, too.
.LP
The
.I troff
manual doesn't say that you can invoke macros as strings, either, but you can.
.I troff
prints ``abc'' when given the following input:
.Ps
\&.de xx
abc
\&..
\e*(xx
.Pe
You can also invoke a string as though it is a macro (i.e., by uttering
the string name on a line by itself with a leading dot).
The contents of the string are interpolated into the input in place of
the line on which the invocation occurs.
However, since
strings have no terminating newline, the input line following
this ``macro'' invocation is taken as part of the same input line
on which the invocation occurs.
.LP
.I troffcvt
treats macros and strings as essentially equivalent.
The primary difference is that strings don't have arguments.
.\"
.Bh "7.3.  Copy Mode"
.LP
The copy mode mechanism
doesn't care how long strings are.
.\"
.Bh "7.4. Diversions"
.LP
These are ``supported'' in a poor way that probably should be changed.
Diversion output isn't saved and just goes to
.I stdout
like everything else.
Output for diversion
.I xx
is bracketed by
.Cl diversion-begin
.I xx
and
.Cl diversion-end
.I xx
for
.Rq di
or by
.Cl diversion-append
.I xx
and
.Cl diversion-end
.I xx
for
.Rq da .
Diversion output may be nested, which is one reason support is poor.
(It puts the burden on the postprocessor to unnest them.)
.LP
Diversion output is not saved in a macro body, because diversions are
often linked to position traps and thus might never be called.
Since that would lose the output completely, I judged it better to
interpolate the diversion into the output at the point at which it
is created.
The down side is that for diversions which
.I are
invoked explicitly, the diversion doesn't appear where it should.
.LP
Possibly diversion output should be saved in temporary files and written
to the output when the diversion is done.
But the question is:
when is a diversion ``done''?
(There may be a
.Rq da
later in the input.)
.LP
The
.B \.d ,
.B \.h ,
.B \.t ,
.B dn
and
.B dl
registers are not set.
The
.B \.z
register is the
.I name
of the current diversion,
.I not
a numeric value.
Its value is empty if no diversion is currently active, otherwise the
current diversion name is interpolated into the output.
.\"
.Bh "7.5. Traps"
.LP
Position and diversion traps
.Rq wh , (
.Rq ch ,
.Rq dt )
are not supported.
.I troffcvt
ought at least to write out some of the information for these requests
so that postprocessors could try to use it if they wanted.
.LP
The input line trap
.Rq it ) (
is supported.
.\"
.Ah "8. Number Registers"
.LP
The
.I troff
manual doesn't say it, but
.Rq rr
allows multiple registers to be specified for removal on a single request.
.I troffcvt
does, too.
.LP
The manual also doesn't say that if the increment or format arguments are
missing, and the register already exists, the existing increment and format
carry into the new definition.
In
.I troffcvt ,
only the increment carries through, since formats are broken (see below).
.LP
You cannot set, rename, remove or change the format of read-only
registers.
.LP
The number register formats
.B i ,
.B I ,
.B a
and
.B A
are broken.
These all print in the default format.
Formats 01, 001, etc. are not parsed correctly either, yet.
.LP
The
.B ct ,
.B dl ,
.B dn ,
.B hp ,
.B ln ,
.B nl ,
.B sb ,
and
.B st
registers are not supported.
.LP
The value of the
.B %
register is unreliable, since the ``current page number'' is unknown.
.LP
The
.B \.A ,
.B \.T ,
.B \.a ,
.B \.d ,
.B \.h ,
.B \.n ,
.B \.t ,
.B \.x ,
and
.B \.y
registers are not supported.
.LP
The
.B \.w
register is always set to 1
.B en ,
since
.I troffcvt
calculates widths of strings by assuming that all characters are 1
.B en
wide.
(See \(sc11.)
.LP
The
.B \.z
register is anomalous, since it's not really a number; see notes for
\(sc7.4.
(This isn't a
.I troffcvt
bug;
.I troff
treats
.B \.z
specially, too.)
.LP
References to non-existent or unsupported registers are interpolated as
``0'' (zero).
.\"
.Ah "9. Tabs, Leaders, and Fields"
.LP
Tab and leader characters appear as
.St tab
and
.St leader
in the output.
.LP
.Rq ta
with no arguments is written as
.Cl reset-tabs .
The postprocessor should reset tab settings to ``every half-inch''.
If explicit settings are given, the first one is written as
.Cl first-tab
.I position
.I type
and all following as
.Cl next-tab
.I position
.I type .
.LP
Field delimiter characters are written as
.St fieldbegin
or
.St fieldend ,
depending on whether they begin or end a field.
.LP
Field padding characters are written as
.St fieldpad
when the character occurs between pairs of
field delimiter characters (otherwise it is deleted,
which may or may not be correct).
.\"
.Ah "10. Input and Output Conventions and Character Translations"
.\"
.Bh "10.1. Input character translations"
.LP
STX, ETX, ENQ, ACK, BEL, SO, SI and ESC are not treated specially.
You deserve what you get if you have them in your input files.
So there.
.\"
.Bh "10.2. Ligatures"
.LP
Ligature mode as set by
.Rq lg
is not supported.
The special characters
.lg 0
.Es (ff ,
.Es (fl ,
.Es (fl ,
.Es (Fi
and
.Es (Fi
normally should be defined in the action file to write out
.St ff ,
.St fi ,
.St fl ,
.St ffi
and
.St ffl ,
.lg
and postprocessors should be trained to recognize these sequences.
.\"
.Bh "10.3. Backspacing, underlining, overstriking, etc."
.LP
No motion is generated for backspace characters; they appear as
.St backspace
in the output.
.LP
Underlining is indicated by
.Cl underline
for normal underlining and
.Cl cunderline
for continuous underlining.
These are identical in
.I troff ;
postprocessors may or may not wish to consider them so, depending on
the capabilities of the target format.
Underlining (both kinds) is turned off with
.Cl nounderline .
.\"
.Bh "10.5. Output translation"
.LP
.Rq tr
doesn't work for special characters or for escaped characters.
The output character can be anything, but the input character must be plain
text.
This is legal:
.Ps
\&.tr x\e(**
.Pe
This is not:
.Ps
\&.tr \e(**x
.Pe
.\"
.Bh "10.6. Transparent throughput"
.LP
Transparent mode (\fB\e!\fR) is not supported very well.
.LP
Real-life observations of behavior of
.I troff
versions:
It doesn't appear to be quite true that the rest of the line
after
.Es !
is always passed as is, at least from my observations on
.I groff
and SunOS 4.1.1
.I nroff .
Embedded newlines are still processed.
Comments are still stripped.
If a transparent line within a multi-line section of
conditional input contains
.Es }
on multi-line conditional input is recognized and terminates
the input if it is within a rejected clause.
If it is within an accepted clause, the
.Es }
appears on the transparent line.
.\"
.Bh "10.7. Comments and concealed newlines"
.LP
Comments and concealed newlines are swallowed at a very low level in the
input routines, and are thus unavailable to postprocessors.
.\"
.Ah "11. Local Horizontal and Vertical Motions, and the Width Function"
.LP
.Es w "\'\fIstring\fR\'"
computes widths of strings only to an approximation.
Since character widths are unknown, the width is computed as though all
characters in the string are 1
.B en
wide.
Font and size changes are recognized but ignored, which leads to particularly
egregious errors for constructs such as
\fB\ew\'\es+9\es+9\es+9X\es\-9\es\-9\es\-9\'\fR.
The ramifications of the fact that
.Es w
yields only approximate results are legion, since
.Es w
may be used in any expression, e.g., in numeric arguments to
requests, or in escape sequences such as
.Es h \'\fIN\fR\'.
.\"
.Ah "12. Overstrike, Bracket, Line-drawing and Zero-width Functions"
.LP
.Es b "\'\fIstring\fR\'"
and
.Es o "\'\fIstring\fR\'"
are supported by writing
the characters in
.I string
to the output, sandwiched between
.Cl bracket-begin
.Cl overstrike-begin ) (
and
.Cl bracket-end
.Cl overstrike-end ). (
Certain characters, if present in
.I string ,
are botched, such as
.Es e .
.LP
.Es l "\'\fINc\fR\'"
and
.Es L "\'\fINc\fR\'"
are supported but don't always work.
In particular,
if the repetition character is ``x'', as in
.B \el'10x' ,
the ``x'' is eaten as part of the expression and not recognized
as the repetition character.
Certain other repetition characters aren't written to the output correctly
(same bug as for
.Es b
and
.Es o ).
.LP
.Es z \fIc\fR
appears as
.Cl zero-width
.I c
in the output.
.\"
.Ah "13. Hyphenation"
.LP
.Rq nh
and
.Rq hy
appear as
.Cl hyphenate
.I N
in the output.
The value of
.I N
should be interpreted as indicated in the
.I troff
manual.
If
.I N
is zero, hyphenation should be turned off.
.LP
The current hyphenation character is recognized and appears as
.St opthyphen
in the output.
.LP
.Rq hw
is not supported.
.\"
.Ah "15. Output Line Numbering"
.LP
Not supported, because there is no way to determine how a postprocessor
might lay out text on a page.
This is especially true for
.I tc2html :
the resulting HTML document may be reformatted dynamically whenever a
user viewing the document in a Web browser window resizes the window.
.\"
.Ah "16. Conditional Acceptance of Input"
.LP
Unfortunately,
processing of conditionals
.Rq if , (
.Rq ie/\.el )
is to a large extent meaningless and may introduce
errors into the output.
The tests for the conditions
.B t
and
.B n
are processed properly, but other tests may not be.
For instance, many times a conditional will test the value of the
current page number
.Es n% ), (
which cannot be determined reliably.
.LP
Conditional requests are processed in a special way.
Normally, to process a request, the arguments are parsed first.
Then
.I troffcvt
scans to the end of the request line, to avoid having extraneous junk
be parsed as text or another request, and then any actions
remaining in the request's action list
are executed to interpret the request arguments.
.LP
For conditional requests, that doesn't work.
A different approach is taken.
Here is how the
conditional requests can be specified in an action file:
.Ps
req if parse-condition n eol
req ie parse-condition y eol
req el process-condition eol
.Pe
For
.Rq if
and
.Rq ie ,
the argument is the condition to be tested, but after parsing it the
rest of the line cannot be skipped over without losing some of the
conditional input.
What happens instead is that the
.B parse-condition
action gobbles up the condition and skips any following whitespace.
If the conditional input is a single line (no
.Es {
present), the input-line processor is invoked once recursively, which
causes the rest of the line to be processed as though it were a new
line.
The tricky part is that processing this line will involve reading
the rest of the line,
.I including
the terminating linefeed.
When the inner invocation of the
line processor returns from handling the conditional input, the outer
invocation of the processor that is handling the conditional request (i.e., the
one performing the
.B parse-condition
action), is still in its argument-parsing phase, and still expects
to skip to the end of the input line after parsing the condition.
So a fake linefeed is shoved into the input before returning to
the condition parser.
.LP
For conditional requests that are followed by multi-line input,
a mild elaboration suffices.
If the conditional input begins with
.B \e{ ,
the current conditional level is incremented and the input processor
is called repeatedly until the level returns to the original value.
(The level is decremented by the input routine
.I ChIn()
which simply discards the
.Es }
and returns the next character.)
.LP
If a condition fails, the input is scanned character-by-character
until the end of the
current line (for single-line conditional input), or until a
.Es }
matching the beginning
.Es {
is found (for multi-line input).
.LP
The else part of the
.Rq ie/\.el
if-else construction is accepted or rejected by remembering the value of
the previous
.Rq ie .
If the
.Rq ie
succeeded, the
.Rq el
part is skipped, otherwise it's processed.
.LP
It does not appear to be necessary that the
.Rq el
.I immediately
follow
.Rq ie ,
so
.I troffcvt
does not require that.
.Rq el
following
.Rq if
is skipped, as is
.Rq el
following another
.Rq el .
.LP
Observations about
.I troff
versions (which don't really belong here, but I'm writing them down
so I don't completely forget about them):
.Ls B
.Li
.Es }
anywhere in a line terminates accepted multiple-line conditional input.
.Li
.Es }
immediately terminates rejected multiple-line conditional input.
The rest of the line is processed as a new line.
.Li
.Es }
in a transparent line is passed through if it occurs in
accepted multiple-line conditional input.
.Li
.Es }
in a transparent line immediately terminates rejected multiple-line conditional
input.
.Le 0
.\"
.Ah "17. Environment Switching"
.LP
It's not explicit in the
.I troff
manual, but for revertible parameters such as indent or point size, the current
.I and
.I previous
values are saved in the environment.
.I troffcvt
does this, too.
.\"
.Ah "18. Insertions from Standard Input"
.LP
.Rq rd
is not supported.
.\"
.Ah "19. Input/Output File Switching"
.LP
.Rq nx
doesn't properly unwind the input stack if current input source
is not a file.
The request is simply ignored after printing an error message.
.LP
.Rq pi
is not supported.
.\"
.Ah "20. Miscellaneous"
.LP
.Rq mc ,
.Rq pm ,
.lg 0
.Rq fl
.lg
are not supported.
For
.Rq fl ,
this doesn't matter because output isn't buffered anyway.
.\"
.Ah Addendum
.LP
Most of the stuff mentioned in the
.I troff
addendum is unimplemented.
.\"
.Bh "A.1. Command-Line Options"
.LP
These are irrelevant to
.I troffcvt .
.\"
.Bh "A.2. Requests"
.LP
The description of the
.Rq ab
request doesn't specify whether the string argument is to be read in
copy mode or not.
Assuming that it should be,
.Rq ab
can be defined in the action file as
.Ps
req ab parse-string-value n eol abort $1
.Pe
The
.Rq ad ,
.Rq ft
and
.Rq so
requests behave as described.
.LP
Other requests in this section are unsupported.
.\"
.Bh "A.3. New Escape Sequences"
.LP
These are all unsupported.
.\"
.Bh "A.4. New Predefined Number Registers"
.LP
.B \.R
and
.B c\.
are supported as general registers.
.B \.R
always contains a large value, since
.I troffcvt
always assumes it can get more memory.
.LP
.B $$ ,
.B \.L ,
.B \.b ,
and
.B \.j
are supported as read-only registers.
.LP
.B \.P ,
.B \.k ,
.B \.T
are not supported.
.\"
.Bh "A.5. Other Important Changes"
.LP
Conditional input is treated as described.
