.\" this document requires the tmac.wrprc macros
.\"
.\" $(TROFF) $(MSMACROS) tmac.wrprc thisfile
.\"
.\" revision date - change whenever this file is edited
.ds RD 11 April 1997
.\"
.EH 'troffcvt Front End Writing'- % -''
.OH ''- % -'troffcvt Front End Writing'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.de St	\" troffcvt special text
\\&\\$3\fB@\\$1\fR\\$2
..
.de Cl	\" troffcvt or RTF control
\\&\\$3\fB\e\\$1\fR\\$2
..
.de Rq	\" troff request
\\&\\$3\fB\.\\$1\fR\\$2
..
.de Es	\" troff escape
\\&\\$3\fB\e\\$1\fR\\$2
..
.TL
.ps +2
troffcvt Front End Writing
.ps
.AU
Paul DuBois
.H*ahref mailto:dubois@primate.wisc.edu
dubois@primate.wisc.edu
.H*aend
.AI
.H*ahref http://www.primate.wisc.edu/
Wisconsin Regional Primate Research Center
.H*aend
Revision date:\0\0\*(RD
.\"
.LP
.I troffcvt
converts
.I troff
input into an intermediary easily-parsed token stream.
.I troffcvt
postprocessors turn that token stream into some other target format.
For example,
.I tc2rtf
turns the token stream into Rich Text Format (RTF), whereas
.I tc2text
turns it into slightly-formatted plain text.
.LP
A front end is typically a shell or Perl script that glues
.I troffcvt
and some postprocessor together via a pipe.
It provides a convenience for users, who then need neither know nor
care about the existence of
.I troffcvt
itself or the postprocessor; they only know that their
.I troff
input turns into some other format by invoking the front end.
.LP
When you write a
.I troffcvt
front end,
the main thing you need to be concerned about is argument processing.
.I troffcvt
understands a certain set of flags, and the postprocessor very likely
understands certain flags as well.
The front end can determine its own syntax to some extent, but some
of
.I troffcvt 's
options
should be recognized and passed straight through.
For instance, in order to preserve the correspondence between
.I troff
and
.I troffcvt ,
the front end should pass through any
.B \-m \fIxxx\fR
flags.
.LP
Suppose you normally use something like this to format a document
.I myfile.ms :
.Ps
% \f(CBtroff -ms myfile.ms\fP
.Pe
With
.I troffcvt ,
you might use a command like this:
.Ps
% \f(CBtroffcvt -ms -a tc.ms myfile.ms | \f(CIpostprocessor\fP
.Pe
If additional files are used for the postprocessor, the front end can typically
deduce what they are on the basis of any
.B \-m \f[BI]xx\fP
argument.
In most cases, if
.B \-m \f[BI]xx\fP
is specified, the front end should also pass
.B \-a
.B tc.m \f[BI]xx\fP
to provides any general
.B \-m \f[BI]xx\fP-specific
redefinitions.
The front end may also pass a file that provides redefinitions that are
appropriate for a specific postprocessor.
For example, if
.B \-ms
is specified,
.I troff2html
will pass not only
.I tc.ms ,
but
.I tc.ms-html
as well.
There may be other arguments that the front end supplies.
For example,
.I tc2html
has its own action file
.I actions-html ), (
that's used in addition to the standard action file
.I actions ) (
that
.I troffcvt
reads when it starts up:
.Ps
% \f(CBtroffcvt -a actions-html -ms -a tc.ms -a tc.ms-html myfile.ms | tc2html\fP
.Pe
