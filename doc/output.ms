.\" this document requires the tmac.wrprc macros
.\"
.\" $(TBL) tmac.wrprc thisfile | $(TROFF) $(MSMACROS)
.\"
.\" revision date - change whenever this file is edited
.ds RD 21 May 1997
.\"
.EH 'troffcvt Output Format'- % -''
.OH ''- % -'troffcvt Output Format'
.OF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.EF 'Revision date:\0\0\*(RD''Printed:\0\0\n(dy \*(MO 19\n(yr'
.\"
.de St	\" troffcvt special text
\\&\\$3\fB@\\$1\fR\\$2
..
.de Cl	\" troffcvt control
\\&\\$3\fB\e\\$1\fR\\$2
..
.de Rq	\" troff request
\\&\\$3\fB\.\\$1\fR\\$2
..
.de Es	\" troff escape
\\&\\$3\fB\e\\$1\fR\\$2
..
.de Co	\" control output
.LP
\fB\e\\$1\fR \\$2 \\$3 \\$4 \\$5 \\$6 \\$7 \\$8 \\$9
.ft R
.br
..
.TL
.ps +2
troffcvt Output Format
.sp .3v
and
.sp .3v
Postprocessor Writing
.ps
.AU
Paul DuBois
.H*ahref mailto:dubois@primate.wisc.edu
dubois@primate.wisc.edu
.H*aend
.AI
.H*ahref http://www.primate.wisc.edu/
Wisconsin Regional Primate Research Center
.H*aend
Revision date:\0\0\*(RD
.\"
.H*toc*title "Table of Contents"
.\"
.Ah Introduction
.\"
.LP
.I troffcvt
turns
.I troff
input files into a more easily parsed intermediate format to assist in
the process of developing
.I troff -to-XXX
translators.
To provide further assistance, the
.I troffcvt
distribution contains code for a library that sequences
.I troffcvt
output into tokens.
The library is called the
.I troffcvt
reader (which means that it reads
.I troffcvt
output, not that it is used by
.I troffcvt ).
The combination of
.I troffcvt
and the
.I troffcvt
reader essentially turns
.I troff
files into a typed token stream.
This simplifies the job of writing postprocessors.
Generally a postprocessor sits on one side of a pipe reading the input
from
.I troffcvt ,
which sits on the other side of the pipe.
The reader code is linked into the postprocessor and is called by it
to get the next token from the pipe.
.LP
This document describes
.I troffcvt
output format and discusses how to write postprocessors that convert
such output into some target format.
If you decide not to use the reader when writing a postprocessor, you
must understand how to interpret
.I troffcvt
files.
If you do use the reader, then you don't need to know as much about
.I troffcvt
format since the reader tokenizes everything for you.
However, it's still useful to have at least a rudimentary
knowledge of the format.
.\"
.Ah "Output Format"
.LP
.I troffcvt
writes three kinds of lines:
.Ls B
.Li
Control lines used to control text processing.
These begin with a backslash ``\fB\e\fR'' and a keyword, and may have
arguments separated by spaces following the keyword.
Control lines pertain to document structure.
.Li
Special text lines used to indicate special characters.
These begin with an at-sign ``\fB@\fR'' followed by
a special character name, e.g.,
.St grave
.B \` ), (
.St quotedblleft
.B `` ), (
or greek
.St alpha
.B \(*a ). (
The special character should be mapped to a character that
is appropriate for the target format.
.Li
Plain text lines consisting of literal text to be written without further
interpretation.
Together with special text lines, plain text lines comprise the content
of a document.
.Le
A
.I troffcvt
output file is structured as follows:
.Ps
\esetup-begin
\eresolution \f(CIN\fP
\fIother setup lines...\fP
\esetup-end
\fIrest of document...\fP
.Pe
The first portion of the file consists of
a setup section bracketed by
.Cl setup-begin
and
.Cl setup-end
lines.
The lines in between indicate the initial document layout.
The first line of this information is
.Cl resolution
.I N ,
where
.I N
is the number of basic units per inch.
This indicates the resolution at which
.I troffcvt
performed its calculations.
Numbers obtained from other control lines may be converted to
ems, points, etc., using this resolution.
For instance, if the resolution is 1440, the control line
.Cl spacing
.B 240
indicates a baseline spacing of 1/6 inch.
.LP
The default resolution used by
.I troffcvt
is 432, but can be changed to whatever you want.
Probably the
.I lowest
resolution you want to use is the least common multiple of 72 and the
resolution you expect to use in the target format.
Otherwise you may end up with ugly round-off errors when you convert units
back to ems, points, etc.
.LP
If the resolution is
.I r ,
other common
.I troff
units may be calculated as follows.
.I S "" (
is the current point size.)
.LP
.TS
center box;
cfB | cfB | lfB
c|l|l.
Unit	Name	Number of basic units
_
\fBi\fR	inch	\fIr\fR
\fBc\fR	centimeter	\fIr\fR\^\(mu\^50\(sl127
\fBP\fR	pica = 1\(sl6 inch	\fIr\fR/6
\fBm\fR	em = \fIS\fR points	\fIS\fR\^\(mu\^\fIr\fR/72\^=\^\fISr\fR/72
\fBn\fR	en = em\(sl2	\fIS\fR\^\(mu\^\fIr\fR/72\^\(mu\^1/2\^=\^\fISr\fR/144
\fBp\fR	point = 1\(sl72 inch	\fIr\fR/72
\fBu\fR	basic unit	1
\fBv\fR	vertical line space	varies; set by \fB\espacing\fR \fIN\fR
.TE
.LP
The rest of the lines in the setup section contain information for the
page length, page width, indents, etc.
.Ah "Special Text Lines"
.LP
.I troffcvt
knows the names of special characters from two sources of information.
The input sequence and output sequence for a given special character
is either built in and recognized
implicitly, or taken from an action file that is read at runtime.
Special characters with an input sequence of the
form \fB\e(\fIxx\fR or \fB\e[\fIxxx\fB]\fR are always in the latter category.
.\"
.Bh "Built-in Special Characters"
.LP
Built-in characters form a short list.
Most of these are listed
in the ``Escape Sequences for Characters, Indicators, and Functions''
section of the
.I troff
.B "Summary and Index"
document.
.LP
.TS
center box;
cfB | cfB | cfB
lfB | l | l.
Input Sequence	Output Sequence	Note
_
\ee	@backslash	affected by \fB.ec\fR
\`	@quoteleft
\'	@quoteright
\`\`	@quotedblleft
\'\'	@quotedblright
\e&	@zerospace
\e^	@twelfthspace
\e|	@sixthspace
\e0	@digitspace
\e(space)	@hardspace
\e\-	@minus
\e\`	@grave
\e\'	@acute
\e%	@opthyphen	affected by \fB.hc\fR
\ea\fR,\fISOH\fR	@leader
\et\fR,\fITAB\fR	@tab
\e(backspace)	@backspace
\fIvaries\fP	@fieldbegin	affected by \fB.fc\fR
\fIvaries\fP	@fieldend	affected by \fB.fc\fR
\fIvaries\fP	@fieldpad	affected by \fB.fc\fR
.TE
.LP
The output sequence for
.Es e
actually depends on the current escape character, which may be changed with
.Rq ec .
The input sequence for the optional hyphenation character may be changed
with
.Rq hc .
.LP
The characters
.B @
and
.B \e
have special meaning in
.I troffcvt
files, so they are indicated in
.I troffcvt
output by the specials
.St at
and
.St backslash
where they are to appear in
the final output literally.
Postprocessors should convert them back to
.B @
and
.B \e
characters.
.LP
The field delimiter and field pad characters defined with
.Rq fc
are written out as
.St fieldbegin
or
.St fieldend ,
and
.St fieldpad .
(Odd delimiters begin fields; even ones end fields).
.\"
.Bh "Non-built-in Special Characters"
.LP
The number of non-built-in characters is not fixed.
All the special characters listed in the Ossanna
.I troff
manual are defined in
the default action file supplied with the
.I troffcvt
distribution, but the list may be modified as necessary to reflect
extra special characters available in
your local version(s) of
.I troff .
The default action file as distributed with
.I troffcvt
includes a number of special characters known by
.I groff .
.\"
.Ah "The troffcvt Reader"
.\"
.LP
The
.I troffcvt
file reader reads
.I troffcvt
output and tokenizes it, setting several global variables in the process:
.Ps
.ta 1.1i
tcrClass	\fRtoken class\fP
tcrMajor	\fRtoken major number\fP
tcrMinor	\fRtoken minor number\fP
tcrArgv[]	\fRtoken text vector\fP
tcrArgc	\fRnumber of elements in \fPtcrArgv[]\fP vector\fP
.Pe
All tokens are assigned to a class.
The other variables are set or not depending on the class.
The classes are:
.Ps
.ta 1.1i
tcrEOF	\fRend of input\fP
tcrControl	\fRcontrol line\fP
tcrText	\fRplain text character\fP
tcrSText	\fRspecial text character\fP
.Pe
Elements of
.Cw tcrArgv[]
are null-terminated strings.
There are
.Cw tcrArgc
elements in the vector.
For plain text and special text tokens,
.Cw tcrArgc
is always 1.
For control tokens, the rest of the line is automatically
parsed to find any following arguments and these are placed into
.Cw tcrArgv[1]
through
.Cw tcrArgv[tcrArgc-1] .
.Cw tcrArgv[tcrArgc]
is
.Cw NULL
in all cases.
.LP
All numbers on control lines are written as integers.
Because numbers may be quite large, postprocessors
generally should convert them to
.Cw long
rather than to
.Cw short
or
.Cw int .
.LP
To use the reader, call
.Cw TCRInit() ,
then call
.Cw TCRGetToken()
repeatedly.
.Cw TCRGetToken()
returns the token class value, which it also stores in the variable
.Cw tcrClass .
When
.Cw TCRGetToken()
returns
.Cw tcrEOF ,
the input stream is exhausted and postprocessor can finish up.
.LP
Here is how the global variables are set for the various token classes.
.LP
\f(CWtcrClass = tcrEOF\fR:
.Ps
.ta 1.1i
	\fRnone of the other variables are set\f(CW
.Pe
\f(CWtcrClass = tcrControl\fR:
.Ps
.ta 1.1i
tcrMajor	\fRcontrol major number (see \fItcr.h\fR)\f(CW
tcrMinor	\fRmajor number subtype (not set for all control words, see \fItcr.h\fR)\f(CW
tcrArgv[i]	\fRfor i = 0, text of control word, including leading \e character\f(CW
	\fRfor i > 0, argument following control word\f(CW
tcrArgc	\fRnumber of arguments, including control word\f(CW
.Pe
\f(CWtcrClass = tcrText\fR:
.Ps
.ta 1.1i
tcrMajor	\fRASCII value of character (each character is a separate token)\f(CW
tcrArgv[0]	\fRone-byte string containing the character\f(CW
tcrArgc	\fR= 1\f(CW
.Pe
\f(CWtcrClass = tcrSText\fR:
.Ps
.ta 1.1i
tcrMajor	\fRusually \f(CWtcrSTUnknown\fR, but see below\f(CW
tcrArgv[0]	\fRtext of special character name, including leading @ character\f(CW
tcrArgc	\fR= 1\f(CW
.Pe
All the built-in special characters are recognized and assigned distinct
major numbers.
Other specials are assigned the major number
.Cw tcrSTUnknown
and the postprocessor must
examine the text of the token
.Cw tcrArgv[0] ) (
to determine what it is and what to do with it.
There is no way for the reader to assign fixed numbers to these since the
set of special characters understood by
.I troffcvt
isn't fixed.
One way of dealing with the problem is to
read at runtime a file of all the special character
names you expect to see.
(Usually the same set of names specified in the action file used with
.I troffcvt .)
.LP
Note:
although built-in special characters
.I do
have fixed major numbers assigned, there is nothing to prevent you
from processing them like other specials, i.e., by examining the token text.
It may be more convenient to treat all specials uniformly.
.LP
The reader changes the characteristics of the default token scanner.
This is done in
.Cw TCRInit() .
If you use the token scanning library for other purposes in your application,
you need to change the scanner's characteristics to what you want and then
restore them, or
.Cw TCRGetToken()
may not work correctly.
.\"
.Bh "Other Global Variables"
.\"
.LP
The
.Cw tcrLineNumber
holds the current input line number.
This may be useful when printing error messages.
Be aware that this is
not the line number of the original
.I troff
input given to
.I troffcvt ;
it's the line number of the output from
.I troffcvt .
.\"
.Ah Postprocessors
.\"
.LP
I assume in this section that you use the
.I troffcvt
reader to write a postprocessor.
It's not necessary that you do so, but if you don't, most of the following
comments don't apply.
.LP
It's best that you examine the source for some of the postprocessors supplied
in the
.I troffcvt
distribution before trying to write one of your own.
You should also read the document
.H*ahref bugs.html
\fItroffcvt \*- Notes, Bugs, Deficiences\fR
.H*aend
to acquaint yourself with
.I troffcvt 's
many limitations.
.LP
A postprocessor can be set up this way:
.Ls B
.Li
Call the reader to read tokens until
.Cl setup-end
is seen,
processing control lines to initialize the document
layout state.
.Li
Dump out initial layout information (if necessary) in a form appropriate
to the target format.
.Li
Call the reader to read the rest of the document until it returns
.Cw tcrEOF .
Process each token as it is returned:
.Ls B
.Li
.B "Plain text"
.sp .5v
Write the character to the output, except that
end of line character(s) should be ignored (CR, LF or CRLF, depending
on the convention used on your system).
.Li
.B "Special text"
.sp .5v
The special character should be mapped to a character appropriate for the
target format and written out.
If no exact equivalent is available, something reasonably close should be substituted.
For instance,
.St quotedblleft
denotes double left curly quotes.
If the output format has only straight quotes (\fB"\fR), that
might be a reasonable substitute.
.sp .5v
In extreme cases, no reasonable substitute exists.
When this happens,
some translators supplied in the distribution adopt the convention of writing
``[[\fIcharacter-name\fR]]''.
This is based on the assumption that it's better to write some sequence
that makes it obvious the character occurred in the input than simply to
drop it.
.Li
.B "Control tokens"
.sp .5v
Modify the document processing state.
.Le 0
.Le
The procedure described above
may seem deceptively simple, and, considered in the abstract, it
is.
I find it easiest to begin with a copy of
.I tc2null.c ,
which simply routes tokens through a bunch of switch statements.
The switches can be filled in as you decide what to do
with various tokens, which allows incremental development of a postprocessor.
You'll probably find that, although this approach is simple conceptually, in
practice the details quickly can become more complex than you'd like.
Some of the important issues about which you need to be concerned are
discussed in the following sections.
.\"
.Bh "Special Character Handling"
.LP
How will you specify what to do with special characters?
Remember that the reader assigns distinct major numbers to only those
special characters for which recognition is built into
.I troffcvt .
.LP
Generally, postprocessors read in a list of special characters
that parallels the list given in the action file used by
.I troffcvt .
If the action file list is changed, all the lists used
by various postprocessors need to be changed, too.
This is a headache, but at least the changes can be made by editing text files
rather than by recompiling programs.
.LP
To get a list of all the special character names, run this command in
the
.I misc
directory:
.Ps
% \f(CBchk-specials /dev/null > junk
.Pe
This puts into
.I junk
all the special character names that are not found in
.I /dev/null ,
which, since that file is empty, will be all the names.
You can use the contents of
.I junk
as a basis for constructing the output sequences you want the postprocessor
to emit for various special characters.
.LP
To test the postprocessor, you can run
.I list-specials ,
another command in the
.I misc
directory that generates a
.I troff -format
listing of all the special characters and their names.
But running the output of
.I list-specials
through
.I troffcvt
and the postprocessor, you can see how each special character is
actually treated.
.\"
.Bh "Text Centering, Filling, and Adjusting"
.\"
.LP
Text centering, filling, and adjusting interact in
.I troff .
My understanding of how this works is indicated below.
Since my conceptual scheme is instantiated in the code,
let's hope it's correct.
.LP
Centering
.Rq ce ) (
takes precedence over filling and adjustment.
When centering is not on, no-fill mode
.Rq nf ) (
suspends filling and adjustment; input lines are copied to the
output, left justified.
If centering is off and filling is on
.Rq fi ), (
input lines are joined
as necessary to fill output lines, which are then adjusted according to
the current adjustment specified by
.Rq ad .
Adjustment may be suspended with
.Rq na .
.LP
Turning off filling merely suspends adjustment.
The adjustment setting is remembered and goes back into effect
when filling is turned back on.
Similarly, centering doesn't change the filling or adjustment settings;
they are suspended while centering is in effect and resume when centering
terminates.
.LP
.I troffcvt
removes the need for postprocessors to handle these centering, filling
and adjusting (CFA) interactions, by always explicitly writing
out which CFA control code to use.
This means the postprocessor only need remember the most recent one.
If
.I troffcvt
did not do this, postprocessors would need to maintain a bunch of
state variables (currently centering?
currently filling?
currently adjusting?
which type of adjustment?).
.LP
The CFA control words are:
.Ps
\eadjust-center
\eadjust-full
\eadjust-left
\eadjust-right
\ecenter
\eno-fill
.Pe
When
.Cl center
occurs, centering should be
turned on.
All text up to a
.Cl break
should be placed on a single output line and centered.
Centering continues until a different CFA control occurs.
.LP
When
.Cl nofill
occurs, no-fill mode should be turned on.
All text up to a
.Cl break
should be placed on a single output line,
left-justified.
No-fill mode continues until a different CFA control occurs.
.LP
If neither centering nor no-fill are in effect, filling is on and
one of the adjustment modes
.Cl adjust-left ,
.Cl adjust-right ,
.Cl adjust-full
or
.Cl adjust-center
will be issued.
All text up to the next
.Cl break
should be used to fill output lines.
All output lines in a paragraph except the last should be adjusted
in the proper way.
.LP
Postprocessors can likely treat
.Cl center
and
.Cl adjust-center
as equivalent.
Ditto for
.Cl nofill
and
.Cl adjust-left .
.LP
Note that there are no control words such as
.Cl nocenter ,
.Cl fill
or
.Cl noadjust .
Centering is turned off by
.Cl nofill
and the adjustment indicators.
Filling is turned on by the adjustment indicators.
The
.I troff
no-adjust request
.Rq na
seems functionally equivalent to left-adjustment and so is indicated
with
.Cl adjust-left .
The reason for the
.Rq na
request seems to be so that
.Rq na
can be followed by
.Rq ad
(with no argument) to resume whatever adjustment mode was in effect
prior to the
.Rq na .
Since
.I troffcvt
keeps track of adjustment modes it can write out the proper indicator
explicitly.
.LP
It is not the case that
.I troffcvt
output will contain a single line of text corresponding to each input
line when no-fill or centering are in effect.
For example,
when input contains special characters, each of these appears on a
separate output line.
Thus, it's important to read text until a
.Cl break
is seen.
.\"
.Bh Paragraphing
.\"
.LP
Some document formats indicate paragraphs when they begin,
others when they end.
The postprocessor will need to follow whichever convention is
used in the target format.
This should be a simple matter since
paragraph beginnings and endings both are readily located in
.I troffcvt
output.
.B \ebreak
corresponds to paragraph endings.
Beginnings are easily found also:
the first text line begins one, and every time a
.B \ebreak
occurs, the following text line begins one.
(Remember that there may be other non-text lines between the
.B \ebreak
and the following text line, though.)
.LP
Paragraph text should be treated conceptually as one unbroken string of text,
even though it may appear physically on several lines of
.I troffcvt
output.
Thus, successive text lines (either plain or special) should
be considered to be part of the same paragraph until a
.Cl break
control line occurs.
The postprocessor should perform line filling and wrapping according to
the most recent centering, filling or adjustment control line (one of
.Cl center ,
.Cl nofill ,
.Cl adjust-left ,
.Cl adjust-right ,
.Cl adjust-full
or
.Cl adjust-center ).
.LP
All characters on plain text lines are significant except the terminating
linefeed, which should be ignored.
Postprocessors should not treat leading or trailing
spaces as extraneous without a good reason.
Postprocessors also should not insert space characters between successive
text lines; where necessary, spaces will already
have been placed within the text itself.
One exception is that the
decision as to whether to put one or two spaces between sentences
is left to the postprocessor.
The main difficulty is determining when a sentence ends.
If the usual suggested style for creation of
.I troff
input files is followed (i.e., that each sentence should begin on a new line),
sentence-terminating periods, question marks and exclamation
points will occur
at the ends of lines.
This property is preserved in
.I troffcvt
output.
Postprocessors thus can locate sentence endings and
have the information they need for determining whether
to insert extra spaces, should they wish to do so.
.\"
.Bh "Font Handling"
.\"
.LP
Font handling can be a difficult issue.
How do
.I troff
fonts correspond to the fonts available in your target format?
One problem is that cannot predict in advance which fonts might be used in a
.I troff
document (although you can probably determine which ones are available
at your site).
.LP
Another problem is that the way
fonts are treated in
.I troff
doesn't correspond well to the way they're treated in
other document formats (at least in my experience).
In
.I troff
one switches from plain text to italic or boldface by switching fonts,
e.g., from
.B R
to
.B I ,
or from
.B R
to
.B B .
It is evident that
.I troff
collapses the two dimensions of typeface and style onto a single-dimensional
font namespace.
For some formats this can be handled by leaving the typeface the same but
applying different style attributes to it.
.LP
For purposes of font support in the
.I troffcvt
reader
it may be more fruitful to map
.I troff
font names onto typeface-style pairs, where the typeface is the
font family a given font derives from and the style indicates those
attributes that need to be applied to the plain font in that
family to produce the effect of the
.I troff
font.
For instance, the default
.I troff
fonts
.B R ,
.B I
and
.B B
can be described as follows:
.LP
.TS
center box;
lfB | lfB | lfB
l | l | l .
Font	Typeface	Style
_
R	Times	plain
I	Times	italic
B	Times	bold
.TE
.LP
Treating fonts this way allows
.I troff
fonts to be manipulated so that ``font'' changes that really
correspond to style changes can be handled as such.
.LP
A simple font to typeface-style map is included in the distribution
(the
.I tcr-fonts
file).
This file should be modified as necessary to reflect fonts available locally
at your site and installed into the
.I troffcvt
library directory.
.I r-font.c
contains the code to use the font map.
The sample postprocessor
.I tc2rtf.c
shows one way to use it.
.\"
.Bh Tabs
.LP
Tab stops should be interpreted relative to the current indent,
.I not
the page offset.
This means that if tab stops are set and then the indent is changed,
the effective tab stops relative to the page offset change.
Some
postprocessors may need to reset tabs in the target format
when that happens.
.\"
.Ah "Control Line Reference"
.\"
.LP
The section documents the syntax of all control lines produced by
.I troffcvt .
The descriptions are grouped according to the section of the
Ossanna
.I troff
manual to which they are most closely related.
The exceptions are section 0, which contains descriptions for miscellaneous
controls that don't correspond to anything in the
.I troff
manual, and section 15, which describes controls for table processing.
.LP
Unless otherwise indicated,
numeric values on control lines are specified in basic units.
.\"
.Bh "\(sc 0. Miscellaneous"
.\"
.Co setup-begin
.\"
.IP
Indicates the beginning of the setup section of
.I troffcvt
output.
.\"
.Co setup-end
.\"
.IP
Indicates the end of the setup section of
.I troffcvt
output.
When it occurs, the basic layout of the document will have been specified.
.\"
.Co resolution \fIN\fR
.\"
.IP
This line occurs first within the setup section.
It indicates the resolution at which
.I troffcvt
performed its calculations, in basic units per inch.
.\"
.Co comment \fIstring\fR
.\"
.IP
Indicates a comment, which may be ignored.
The entire
.I string
will be in
.Cw tcrArgv[1].
.Cl comment
lines may appear
.I "at any time" ,
even before
.Cl setup-begin .
.\"
.Co pass \fIstring\fR
.\"
.IP
Pass
.I string
literally through to the output without
interpretation.
This is used for postprocessor-specific purposes.
The entire
.I string
will be in
.Cw tcrArgv[1].
.\"
.Co line \fIfilename linenumber\fR
.\"
.IP
This indicates the point at which line
.I linenumber
was read from file
.I filename .
.Cl line
controls are generated if the
.B \-l
command line option was given to
.I troffcvt .
They provide a way of tracking the output that results from each input line.
.\"
.Co other \fIstring\fR
.\"
.IP
Indicates a line that doesn't fall into any other class.
The contents of
.I string
can be used for anything.
.I string
is parsed into separate arguments.
.\"
.\"
.Bh "\(sc 2. Fonts and Character Size Control"
.\"
.Co font \fIF\fR
.\"
.IP
Switch to font
.I F .
.\"
.Co constant-width \fIF\fR
.\"
.IP
Treat font
.I F
as though it is non-proportional (each character the same width).
.\"
.Co noconstant-width \fIF\fR
.\"
.IP
Stop treating font
.I F
as though it is non-proportional.
.\"
.Co embolden \fIF N\fR
.\"
.IP
Embolden font \fIF\fR by smearing it
.I N
units.
If
.I N
is zero, emboldening should be turned off.
.IP
If you implement emboldening,
you might find it more profitable to ignore the smear value and make the
font bold by some means other than reprinting the characters slightly
displaced from the original printing like
.I troff
does.
.\"
.Co embolden-special \fIF N\fR
.\"
.IP
Like
.Cl embolden ,
but embolden characters in the special font whenever the current font is
.I F .
In practice, this probably has no meaning for most postprocessors,
because the special font in
.I troff
is logically part of the three default fonts, something unlikely to be
true in the target output format.
.\"
.Co point-size \fIN\fR
.\"
.IP
Set point size to
.I N .
.I N
is number of points, not basic units.
.\"
.Co space-size \fIN\fR
.\"
.IP
Set space size to
.I N /36\fBm\fR.
Note that the actual instantaneous value depends of the size of an
.B em
(and thus on the current point size).
For this reason it may be best to maintain
.I N
but also recompute the actual space size in basic units whenever the point
size changes.
.\"
.Bh "\(sc 3. Page Control"
.\"
.Co begin-page [\fIN\fR]
.\"
.IP
If
.I N
is present, begin new page numbered
.I N .
Otherwise, just begin new page (presumably numbered in sequence with the
current page).
.\"
.Co offset \fIN\fR
.\"
.IP
Set page offset to
.I N .
.\"
.Co page-length \fIN\fR
.\"
.IP
Set page length to
.I N .
.\"
.Co page-number \fIN\fR
.\"
.IP
Set page number to
.I N .
(Does not begin a new page.)
.\"
.Co need \fIN\fR
.\"
.IP
.I N
units of vertical space are needed.
If less than that remains on the current page, begin a new page.
.\"
.Co mark
.\"
.IP
Remember the current vertical position on the page.
This assumes you have some notion of the current position, of course.
Not all target formats have such a notion;
.I troffcvt
itself certainly doesn't.
.\"
.Bh "\(sc 4. Text Filling, Adjusting, and Centering"
.\"
.ne 4
.Co adjust-center
.\"
.sp -.7v
.Co adjust-full
.\"
.sp -.7v
.Co adjust-left
.\"
.sp -.7v
.Co adjust-right
.\"
.IP
Fill output lines, adjusting as indicated.
.\"
.Co nofill
.\"
.IP
Do not fill output lines.
.\"
.Co center
.\"
.IP
Center output lines.
.\"
.Co break
.\"
.IP
End of input line.
Flush and terminate current output line.
.\"
.Co break-spread
.\"
.IP
Break at end of current word and spread output line to current line length.
.\"
.Bh "\(sc 5. Vertical Spacing"
.\"
.Co spacing \fIN\fR
.\"
.IP
Set vertical base-line spacing to
.I N
units, which becomes the meaning of 1
.B v .
.\"
.Co line-spacing \fIN\fR
.\"
.IP
Set line spacing to
.I N
.B v 's.
.\"
.Co space \fIN\fR
.\"
.IP
Space vertically
.I N
units (negative = upward).
.\"
.Co extra-space \fIN\fR
.\"
.IP
Indicates that the current output line should have
.I N \fBv\fP
of extra space added to it.
.\"
.Bh "\(sc 6. Line Length and Indenting"
.\"
.Co indent \fIN\fR
.\"
.IP
Set indent to
.I N
units.
.\"
.Co line-length \fIN\fR
.\"
.IP
Set line length to
.I N
units.
.\"
.Co temp-indent \fIN\fR
.\"
.IP
Temporarily set indent to
.I N
units.
This is an absolute indent, not an value by which to adjust the current
indent.
In
.I troff ,
the temporary indent value is used only for the next output line and the
prevailing indent is used again after that.
In other formats it likely corresponds to ``paragraph first line indent'' or
something similar.
.\"
.Bh "\(sc 7. Macros, Strings, Diversions, and Position Traps"
.\"
.Co diversion-begin \fIname\fR
.\"
.IP
Indicates that subsequent output, until
.Cl diversion-end
.I name
occurs, was intended in the original input to be diverted to macro
.I xx .
.\"
.Co diversion-append \fIname\fR
.\"
.IP
Indicates that subsequent output, until
.Cl diversion-end
.I name
occurs, was intended in the original input to be appended to diversion macro
.I xx .
.\"
.Co diversion-end \fIname\fR
.\"
.IP
Indicates end of preceding
.Cl diversion-begin
.I name
or
.Cl diversion-append
.I name .
.\"
.Bh "\(sc 9. Tabs, Leaders, and Fields"
.\"
.Co reset-tabs
.\"
.IP
Reset tab stops to default (every half-inch).
.\"
.Co first-tab \fIN c\fR
.\"
.IP
Clear tab stops and install first one at
.I N
units.
.I c
is
.B l
(left),
.B c
(center),
or
.B r
(right).
.\"
.Co next-tab \fIN c\fR
.\"
.IP
Add tab stop to current set.
.I N
and
.I c
are as for
.Cl first-tab .
.\"
.Co tab-char [\fIc\fR]
.\"
.IP
Set tab repetition character to
.I c .
If
.I c
is missing, tabs should be implemented as motion.
.\"
.Co leader-char [\fIc\fR]
.\"
.IP
Set leader repetition character to
.I c .
If
.I c
is missing, leaders should be implemented as motion.
.\"
.Bh "\(sc 10. Input and Output Conventions and Character Translations"
.\"
.Co underline
.\"
.IP
Turn on underlining.
.\"
.Co cunderline
.\"
.IP
Turn on continuous underlining.
.\"
.Co nounderline
.\"
.IP
Turn off underlining (both kinds).
.\"
.Co underline-font \fIF\fR
.\"
.IP
Set underline font to
.I F .
.\"
.Bh "\(sc 11. Local Horizontal and Vertical Motions, and the Width Function"
.\"
.Co motion \fIN c\fR
.\"
.IP
Move
.I N
units.
.I c
is
.B h
for horizontal motion (negative = left) or
.B v
vertical motion (negative =
upward).
.\"
.Co line \fIN c\fR
.\"
.IP
Draw line.
.I N
and
.I c
are as for
.Cl motion .
.\"
.Bh "\(sc 12. Overstrike, Bracket, Line-drawing, and Zero-width Functions"
.\"
.Co bracket-begin
.\"
.IP
Characters on following text lines
(until
.Cl bracket-end )
should be used to build a bracket.
.\"
.Co bracket-end
.\"
.IP
Terminates preceding
.Cl bracket-begin .
.\"
.Co overstrike-begin
.\"
.IP
Characters on following text lines
(until
.Cl overstrike-end )
should be overstruck.
.\"
.Co overstrike-end
.\"
.IP
Terminates preceding
.Cl overstrike-begin .
.\"
.Co zero-width \fIc\fR
.\"
.IP
Print
.I c
without changing position on page.
.\"
.Bh "\(sc 13. Hyphenation"
.\"
.Co hyphenate \fIN\fR
.\"
.IP
Set hyphenation mode.
If
.I N
is zero, turn off hyphenation.
If
.I N
is non-zero, interpret
.I N
as in the
.I troff
manual.
.\"
.Bh "\(sc 14. Three Part Titles"
.\"
.Co title-length \fIN\fR
.\"
.IP
Set title length to
.I N
units.
.\"
.Co title-begin \fIc\fR
.\"
.IP
Indicates the beginning of title part
.I c ,
where
.I c
is
.B l
(left),
.B c
(center),
or
.B r
(right).
Text up to the next
.Cl title-end
control line should be taken as the content of this title part.
.I troffcvt
indicates title content by writing the following output sequence:
.Ps
\etitle-begin l
\fItext of left title part\fP
\etitle-end
\etitle-begin m
\fItext of middle title part\fP
\etitle-end
\etitle-begin r
\fItext of right title part\fP
\etitle-end
.Pe
If no text occurs between the
.Cl title-begin
and
.Cl title-end
lines, it means the specified title part is empty.
No control words will occur between the
.Cl title-begin
and
.Cl title-end
lines.
.\"
.Co title-end
.\"
.IP
Terminates preceding
.Cl title-begin .
.\"
.Bh "\(sc 15. Tables"
.\"
.LP
The
.I troffcvt
language contains special controls to indicate table structure.
These result when
.I tblcvt
is used to preprocess
.I troffcvt
input.
The controls should be written by
.I troffcvt
in a particular order, but
.I troffcvt
itself does no checking to verify the ordering.
It relies on
.I tblcvt
to generate table-related requests that specify table elements in the
proper sequence.
For more details, see the document
.H*ahref tblcvt.html
.I "tblcvt \*- A troffcvt Postprocessor."
.H*aend
.LP
For testing
.I tblcvt ,
see the
.I tblcvt/tests
directory, which contains the tables from the Lesk
.I tbl
document, one table per file.
.\"
.Co table-begin \fIrows cols header-rows align expand box allbox doublebox\fP
.\"
.IP
Indicates the beginning of a table.
.IP
.I rows
and
.I cols
are the number of rows and columns in the table.
(A row that draws a line is considered a data row.)
.IP
For tables that are specified to have a header (using
.Rq TS
.B H
and
.Rq TH ),
.I header-rows
is non-zero.
Otherwise
.I header-rows
is 0.
.I header-rows
indicates how many of the initial data rows make up the table header.
If this is non-zero, that many rows
form a header that should be repeated if the table spans multiple
pages.
For a single-page table, header rows should be treated as just an
ordinary part of the table.
.IP
.I align
is
.B L
or
.B C
to indicate the table is left-justified or centered.
.IP
.I expand
is
.B y
if the table is expanded to the full line width,
.B n
otherwise.
.IP
The
.I box ,
.I allbox ,
and
.I doublebox
values are each
.B y
or
.B n ,
depending on whether or not
.B box ,
.B allbox ,
and
.B doublebox
were given in the table specification.
(Note that
.B allbox
and
.B doublebox
both imply
.B box .)
.\"
.Co table-end
.\"
.IP
Indicates the end of the current table.
.\"
.Co table-column-info \fIwidth sep equal\fP
.\"
.IP
Specifies values that apply to all cells in a table column.
Following the
.Cl table-begin
control, there will be one
.Cl table-column-info
line for each column of the table.
The column number is not specified; the controls for each column
are written consecutively.
.IP
.I width
is the minimum required width of the column.
The
value is non-zero if any entry in the given column specified
a
.B w
option.
If more than one entry specified
.B w ,
the last one is used.
If
.I width
is 0, no entry in the column specified
.B w
and the width is determined from the data values in the column.
.IP
.I sep
is the column separation value.
.IP
The
.I equal
value is
.B y
if any entry in the column specified the
.B e
option, and
.B n
otherwise.
All columns with an
.I equal
value of
.B y
should be made the same width.
.\"
.Co table-row-begin
.\"
.IP
Indicates the beginning of a row within a table.
.\"
.Co table-row-end
.\"
.IP
Indicates the end of the current table row.
.\"
.Co table-row-line \fIN\fP
.\"
.IP
Indicates that the table row is a single or double table-width line.
The value of
.I N
indicates the type of line:
.Ps
\etable-row-line 1	\fRTable-width single line\fP
\etable-row-line 2	\fRTable-width double line\fP
.Pe
There is no end marker for this control, as none is needed.
.\"
.Co table-cell-info \fItype vspan hspan vadjust border\fP
.\"
.IP
Indicates layout information for a a cell within a table row.
.IP
.I type
is the cell type:
.Ps
.ne 5
.ta 1i
L	\fRLeft-justified\fP
R	\fRRight-justified\fP
C	\fRCentered\fP
N	\fRNumeric (align to decimal point)\fP
A	\fRAlphanumeric\fP
.Pe
.I vspan
and
.I hspan
are the number of rows and columns spanned by the cell, including itself.
Interpret these values as follows:
.Ls B
.Li
If a cell doesn't span any other cells (the usual case), both
.I vspan
and
.I hspan
are 1.
.Li
If a cell spans other cells vertically,
.I vspan
is greater than 1.
If a cell is spanned vertically by a cell from above,
.I vspan
is zero.
.Li
If a cell spans other cells horizontally,
.I hspan
is greater than 1.
If a cell is spanned horizontally by a cell from the left,
.I hspan
is zero.
.Le
If all you want to know is whether or not a cell is spanned, the product of
.I vspan
and
.I hspan
is zero if and only if the cell is spanned.
If you need to know whether spanning is in a particular direction, you
need to examine
.I vspan
and
.I hspan
individually.
This is summarized in the following table.
.sp .5v
.TS
l lfB lfB
lw(1i)fB lw(2i) lw(2i) .
	hspan = 0	hspan > 0
vspan = 0	spanned both ways	spanned from above
vspan > 0	spanned from left	not spanned
.TE
.sp .5v
.I vadjust
is
.B T
if the cell contents should be vertically
adjusted from the top,
.B C
if the contents should be vertically centered.
.I vadjust
is meaningful only for multiple-line cells.
.IP
.I border
is the border value.
If the value is 0, there is no border.
Otherwise, the value is a bitmap with the following fields:
.Ps
.ne 9
.ta 1i 2i
\fBBits	Value	Meaning\fP
0-1	1	\fRLeft border, single line\fP
	3	\fRLeft border, double line\fP
2-3	1	\fRRight border, single line\fP
	3	\fRRight border, double line\fP
4-5	1	\fRTop border, single line\fP
	3	\fRTop border, double line\fP
6-7	1	\fRBottom border, single line\fP
	3	\fRBottom border, double line\fP
.Pe
.\"
.Co table-cell-begin
.\"
.IP
Indicates the beginning of a table cell.
.\"
.Co table-cell-end
.\"
.IP
Indicates the end of the current table cell.
.\"
.Co table-empty-cell
.\"
.IP
Indicates a table cell that is empty.
There is no end marker for this control, as none is needed.
.\"
.Co table-spanned-cell
.\"
.IP
Indicates a table cell that is spanned by an earlier cell.
There is no end marker for this control, as none is needed.
Note that that spanned cell may be spanned by a cell with data in it,
an empty cell, or a line-drawing cell.
.\"
.Co table-cell-line \fIN\fP
.\"
.IP
Indicates that the content of a table cell is a line.
The value of
.I N
indicates the type of line:
.Ps
\etable-cell-line 0	\fRColumn-data-width single line\fP
\etable-cell-line 1	\fRColumn-width single line\fP
\etable-cell-line 2	\fRColumn-width double line\fP
.Pe
There is no end marker for this control, as none is needed.
